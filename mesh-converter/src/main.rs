use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::PathBuf;

use anyhow::{anyhow, bail, Result};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    /// Input files in Wavefront OBJ format
    #[structopt(parse(from_os_str), required = true)]
    input: Vec<PathBuf>,
    /// Assets directory
    #[structopt(
        short = "o",
        long = "output",
        parse(from_os_str),
        default_value = "assets"
    )]
    output: PathBuf,
}

fn write_f32_slice(writer: &mut impl Write, data: &[f32]) -> Result<()> {
    for v in data {
        writer.write_all(&v.to_le_bytes())?;
    }
    Ok(())
}

fn write_u32_slice(writer: &mut impl Write, data: &[u32]) -> Result<()> {
    for v in data {
        writer.write_all(&v.to_le_bytes())?;
    }
    Ok(())
}

fn main() -> Result<()> {
    let mut opt = Opt::from_args();

    opt.output.push("meshes");
    std::fs::create_dir_all(&opt.output)?;

    for input in &opt.input {
        let name = input.file_name().ok_or_else(|| anyhow!("No file name"))?;

        let (models, _) = tobj::load_obj(input, true)?;
        if models.len() > 1 {
            bail!("{} contains more than one model", input.display());
        }

        let model = models
            .get(0)
            .ok_or_else(|| anyhow!("No models in {}", input.display()))?;
        let mesh = &model.mesh;

        let num_vertices = mesh.positions.len() / 3;

        if mesh.normals.len() / 3 != num_vertices {
            bail!("{} has no normals", input.display());
        }

        if mesh.texcoords.len() / 2 != num_vertices {
            bail!("{} has no texture coordinates", input.display());
        }

        opt.output.push(name);
        opt.output.set_extension("mesh");
        let mut writer = BufWriter::new(File::create(&opt.output)?);
        opt.output.pop();

        writer.write_all(b"MESH")?;
        writer.write_all(&(mesh.indices.len() as u32).to_le_bytes())?;
        write_u32_slice(&mut writer, &mesh.indices)?;
        writer.write_all(&(num_vertices as u32).to_le_bytes())?;
        write_f32_slice(&mut writer, &mesh.positions)?;
        write_f32_slice(&mut writer, &mesh.normals)?;
        write_f32_slice(&mut writer, &mesh.texcoords)?;
    }

    Ok(())
}
