//! Asset management

mod handle;
mod handle_allocator;
mod manager;
mod source;

pub use self::handle::{AsAssetId, Handle, Id, WeakHandle};
pub use self::handle_allocator::HandleAllocator;
pub use self::manager::{Assets, NewAssets};
pub use self::source::{AssetSource, DirectorySource};

use std::io::{BufRead, BufReader};
use std::path::Path;

use anyhow::Result;

/// Asset or resource used by the game engine. Can be a mesh, a texture, a material, a font, etc.
pub trait Asset: Sized + Send + Sync + 'static {}

/// Asset with the default loader
pub trait AssetDefaultLoader: Asset {
    /// Default loader type
    type Loader: AssetLoader<Self> + Default;
}

/// Asset loader
pub trait AssetLoader<A: Asset>: Sized + Send + Sync + 'static {
    /// Load an asset at the specified path
    fn load(&self, new: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<A>;
}

/// Simple asset loader for assets consisting of one file only and no dependencies
pub trait SimpleAssetLoader<A: Asset>: Sized + Send + Sync + 'static {
    /// Load an asset from the specified reader
    fn load(&self, reader: impl BufRead) -> Result<A>;
}

impl<A, L> AssetLoader<A> for L
where
    A: Asset,
    L: SimpleAssetLoader<A>,
{
    fn load(&self, _: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<A> {
        SimpleAssetLoader::load(self, BufReader::new(source.load(path)?))
    }
}
