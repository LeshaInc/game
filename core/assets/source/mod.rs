use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

use anyhow::{Context, Result};

/// Assets source. Can be a directory, a zip archive, or even a network folder if you are brave
/// enough
pub trait AssetSource: Send + Sync + 'static {
    /// Opens asset at the speficied path
    fn load(&self, path: &Path) -> Result<Box<dyn Read>>;
}

#[derive(Clone, Debug, Eq, PartialEq)]
/// Asset source where each asset is stored as file inside assets/ directory.
pub struct DirectorySource {
    /// Path to assets/ directory
    pub root: PathBuf,
}

impl DirectorySource {
    /// Create a directory source with the specified root directory path
    pub fn new(root: impl Into<PathBuf>) -> DirectorySource {
        DirectorySource { root: root.into() }
    }
}

impl AssetSource for DirectorySource {
    fn load(&self, path: &Path) -> Result<Box<dyn Read>> {
        let mut full_path = self.root.clone();
        full_path.push(path);
        let file = File::open(&full_path)
            .with_context(|| format!("Cannot open {:?}", full_path.display()))?;
        Ok(Box::new(file))
    }
}
