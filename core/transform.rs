use legion::systems::{ParallelRunnable, SystemBuilder};
use legion::{component, Entity, IntoQuery, Read, TryRead, Write};
use nalgebra::{Point3, Similarity3, Translation3, UnitQuaternion};

/// Position component
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Position(pub Point3<f32>);

/// Scale component
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Scale(pub f32);

/// Rotation component
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Rotation(pub UnitQuaternion<f32>);

/// Transform component (constructed with `Position`, `Scale` and `Rotation`)
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Transform(pub Similarity3<f32>);

/// Stores whether the `Transform` component has changed since last frame, or not
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct TransformChanged(pub bool);

impl Default for Transform {
    fn default() -> Transform {
        Transform(Similarity3::identity())
    }
}

/// Build the transformation resolving system
pub fn resolve_transform_system() -> impl ParallelRunnable {
    SystemBuilder::new("resolve-transform")
        .with_query(<(
            Read<Position>,
            TryRead<Scale>,
            TryRead<Rotation>,
            Write<Transform>,
            Write<TransformChanged>,
        )>::query())
        .with_query(<Entity>::query().filter(component::<Position>() & !component::<Transform>()))
        .with_query(
            <Entity>::query().filter(component::<Position>() & !component::<TransformChanged>()),
        )
        .build(|cbuf, world, _, queries| {
            for &entity in queries.1.iter(world) {
                cbuf.add_component(entity, Transform::default());
            }

            for &entity in queries.2.iter(world) {
                cbuf.add_component(entity, TransformChanged(true));
            }

            for (pos, scale, rot, trans, t_changed) in queries.0.iter_mut(world) {
                let mut new_trans = Similarity3::identity();

                if let Some(scale) = scale {
                    new_trans.append_scaling_mut(scale.0);
                }

                new_trans.append_translation_mut(&Translation3::from(pos.0.coords));

                if let Some(rot) = rot {
                    new_trans.append_rotation_mut(&rot.0);
                }

                *t_changed = TransformChanged(trans.0 != new_trans);
                *trans = Transform(new_trans);
            }
        })
}
