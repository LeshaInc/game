use nalgebra::{ClosedAdd, ClosedSub, Scalar, Vector2};

use crate::util::Aabr;

/// Offsets for each rectangle side (can be used to specify padding)
pub struct SideOffsets<S: Scalar> {
    /// Top offset
    pub top: S,
    /// Right offset
    pub right: S,
    /// Bottom offset
    pub bottom: S,
    /// Left offset
    pub left: S,
}

impl<S: Scalar> SideOffsets<S> {
    /// Create a new `SideOffsets`
    pub fn new(top: S, right: S, bottom: S, left: S) -> SideOffsets<S> {
        SideOffsets {
            top,
            right,
            bottom,
            left,
        }
    }

    /// Create a `SideOffsets` with equal offsets on each side
    pub fn new_equal(offset: S) -> SideOffsets<S> {
        SideOffsets::new(offset.clone(), offset.clone(), offset.clone(), offset)
    }

    /// Shrink an AABR
    pub fn shrink_aabr(&self, aabr: &Aabr<S>) -> Aabr<S>
    where
        S: ClosedAdd + ClosedSub,
    {
        let min = &aabr.min + Vector2::new(self.top.clone(), self.left.clone());
        let max = &aabr.max - Vector2::new(self.bottom.clone(), self.right.clone());
        Aabr { min, max }
    }

    /// Calculates `(left + right, top + bottom)` vector
    pub fn size_addition(&self) -> Vector2<S>
    where
        S: ClosedAdd,
    {
        Vector2::new(
            self.left.clone() + self.right.clone(),
            self.top.clone() + self.bottom.clone(),
        )
    }
}
