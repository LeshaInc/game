use std::collections::VecDeque;
use std::time::{Duration, Instant};

/// FPS Counter
#[derive(Debug)]
pub struct FpsCounter {
    frame_started: Instant,
    frames: VecDeque<Duration>,
    sum_durations: Duration,
    inertness: Duration,
}

impl FpsCounter {
    /// Create new FPS counter with specified time for which a frame is considered in calculations.
    /// The default is 2 seconds
    pub fn new(inertness: Duration) -> FpsCounter {
        FpsCounter {
            frame_started: Instant::now(),
            frames: VecDeque::with_capacity((inertness.as_secs_f32() * 60.0) as usize),
            sum_durations: Duration::default(),
            inertness,
        }
    }

    /// Begin the next frame
    pub fn next_frame(&mut self) {
        let elapsed = self.frame_started.elapsed();

        self.sum_durations += elapsed;

        while self.sum_durations >= self.inertness {
            if let Some(oldest) = self.frames.pop_front() {
                self.sum_durations = self.sum_durations.checked_sub(oldest).unwrap_or_default();
            }
        }

        self.frames.push_back(elapsed);

        self.frame_started = Instant::now();
    }

    /// Calculate average FPS
    pub fn avg_fps(&self) -> f32 {
        if self.frames.is_empty() {
            0.0
        } else {
            (self.frames.len() as f32) / self.sum_durations.as_secs_f32()
        }
    }

    /// Calculate average frame time
    pub fn avg_frametime(&self) -> Duration {
        if self.frames.is_empty() {
            Duration::default()
        } else {
            self.sum_durations.div_f32(self.frames.len() as f32)
        }
    }
}

impl Default for FpsCounter {
    fn default() -> FpsCounter {
        FpsCounter::new(Duration::from_secs(2))
    }
}
