use nalgebra::Similarity3;
use ncollide3d::bounding_volume::AABB;

/// Extended `AABB<f32>` methods
pub trait AABBOps {
    /// Transform this AABB by a similarity
    fn transform_by_similarity(&self, similarity: &Similarity3<f32>) -> AABB<f32>;
}

impl AABBOps for AABB<f32> {
    fn transform_by_similarity(&self, similarity: &Similarity3<f32>) -> AABB<f32> {
        let scaling = similarity.scaling();
        AABB::from_half_extents(self.center(), self.half_extents() * scaling)
            .transform_by(&similarity.isometry)
    }
}
