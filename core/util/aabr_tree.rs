use super::Aabr;

const NULL_NODE: u32 = u32::max_value();

#[derive(Clone, Debug)]
struct AabrNode {
    aabr: Aabr<f32>,
    parent: u32,
    children: [u32; 2],
}

impl AabrNode {
    fn is_leaf(&self) -> bool {
        self.children == [NULL_NODE; 2]
    }
}

/// AABR tree for collision checking
#[derive(Clone, Debug)]
pub struct AabrTree {
    root: u32,
    nodes: Vec<AabrNode>,
}

impl Default for AabrTree {
    fn default() -> AabrTree {
        AabrTree {
            root: NULL_NODE,
            nodes: vec![],
        }
    }
}

impl AabrTree {
    /// Create an empty AABR tree
    pub fn new() -> AabrTree {
        Default::default()
    }

    /// Clear the tree, without deallocating space
    pub fn clear(&mut self) {
        self.root = NULL_NODE;
        self.nodes.clear();
    }

    fn get(&self, idx: u32) -> &AabrNode {
        &self.nodes[idx as usize]
    }

    fn get_mut(&mut self, idx: u32) -> &mut AabrNode {
        &mut self.nodes[idx as usize]
    }

    fn alloc_node(&mut self, aabr: Aabr<f32>) -> u32 {
        let node = AabrNode {
            aabr,
            parent: NULL_NODE,
            children: [NULL_NODE; 2],
        };

        let node_idx = self.nodes.len() as u32;
        self.nodes.push(node);
        node_idx
    }

    /// Insert a node
    pub fn insert(&mut self, aabr: Aabr<f32>) {
        let node_idx = self.alloc_node(aabr);

        if self.root == NULL_NODE {
            self.root = node_idx;
            return;
        }

        let mut cur_idx = self.root;
        while !self.get(cur_idx).is_leaf() {
            let cur = self.get(cur_idx);

            let combined_aabr = aabr.union(&cur.aabr);
            let new_parent_cost = 2.0 * combined_aabr.area();
            let min_push_down_cost = 2.0 * (combined_aabr.area() - cur.aabr.area());

            let children = [self.get(cur.children[0]), self.get(cur.children[1])];
            let mut child_costs = [0.0; 2];
            for (child, child_cost) in children.iter().zip(&mut child_costs) {
                let merged_area = child.aabr.union(&aabr).area();
                *child_cost = if child.is_leaf() {
                    merged_area + min_push_down_cost
                } else {
                    merged_area - child.aabr.area() + min_push_down_cost
                }
            }

            if new_parent_cost < child_costs[0] && new_parent_cost < child_costs[1] {
                break;
            }

            cur_idx = if child_costs[0] < child_costs[1] {
                cur.children[0]
            } else {
                cur.children[1]
            };
        }

        let sibling_idx = cur_idx;
        let sibling = self.get(sibling_idx);
        let old_parent_idx = sibling.parent;
        let new_parent_aabr = aabr.union(&sibling.aabr);
        let new_parent_idx = self.alloc_node(new_parent_aabr);
        let new_parent = self.get_mut(new_parent_idx);
        new_parent.children = [sibling_idx, node_idx];
        new_parent.parent = old_parent_idx;
        self.get_mut(node_idx).parent = new_parent_idx;
        self.get_mut(sibling_idx).parent = new_parent_idx;

        if old_parent_idx == NULL_NODE {
            self.root = new_parent_idx;
        } else {
            let old_parent = self.get_mut(old_parent_idx);
            if old_parent.children[0] == sibling_idx {
                old_parent.children[0] = new_parent_idx;
            } else {
                old_parent.children[1] = new_parent_idx;
            }
        }

        let mut node_idx = new_parent_idx;
        while node_idx != NULL_NODE {
            let node = self.get(node_idx);
            let parent = node.parent;
            let [left_idx, right_idx] = node.children;
            let aabr = self.get(left_idx).aabr.union(&self.get(right_idx).aabr);
            self.get_mut(node_idx).aabr = aabr;
            node_idx = parent;
        }
    }

    /// Check whether the given AABR overlaps with any AABR contained in this tree
    pub fn any_overlap(&self, aabr: Aabr<f32>) -> bool {
        let mut stack = Vec::new();
        stack.push(self.root);
        while let Some(node_idx) = stack.pop() {
            if node_idx == NULL_NODE {
                continue;
            }

            let node = self.get(node_idx);
            if node.aabr.overlaps(&aabr) {
                if node.is_leaf() {
                    return true;
                } else {
                    stack.extend(node.children.iter().copied());
                }
            }
        }

        false
    }
}
