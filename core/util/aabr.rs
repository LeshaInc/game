use std::cmp::PartialOrd;

use nalgebra::{
    ClosedAdd, ClosedMul, ClosedSub, Matrix3, Point2, RealField, Scalar, SimdPartialOrd, Vector2,
};
use num_traits::Zero;

/// Axis aligned bounding rectangle
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Aabr<S: Scalar> {
    /// The top-left corner
    pub min: Point2<S>,
    /// The bottom-right corner
    pub max: Point2<S>,
}

impl<S: Scalar> Aabr<S> {
    /// Create a new AABR
    pub fn new(min: Point2<S>, max: Point2<S>) -> Aabr<S> {
        Aabr { min, max }
    }

    /// Get width
    pub fn width(&self) -> S
    where
        S: ClosedSub,
    {
        self.extent().x.clone()
    }

    /// Get height
    pub fn height(&self) -> S
    where
        S: ClosedSub,
    {
        self.extent().y.clone()
    }

    /// Get extent vector (`max - min`)
    pub fn extent(&self) -> Vector2<S>
    where
        S: ClosedSub,
    {
        &self.max - &self.min
    }

    /// Calculate union with other AABR
    pub fn union(&self, other: &Aabr<S>) -> Aabr<S>
    where
        S: SimdPartialOrd,
    {
        Aabr {
            min: self.min.inf(&other.min),
            max: self.max.sup(&other.max),
        }
    }

    /// Calculate intersection with other AABR
    pub fn intersection(&self, other: &Aabr<S>) -> Aabr<S>
    where
        S: SimdPartialOrd,
    {
        Aabr {
            min: self.min.sup(&other.min),
            max: self.max.inf(&other.max),
        }
    }

    /// Get top left corner
    pub fn top_left(&self) -> Point2<S> {
        self.min.clone()
    }

    /// Get top right corner
    pub fn top_right(&self) -> Point2<S>
    where
        S: ClosedAdd + ClosedSub + Zero,
    {
        &self.min + &Vector2::new(self.width(), S::zero())
    }

    /// Get bottom right corner
    pub fn bottom_right(&self) -> Point2<S> {
        self.max.clone()
    }

    /// Get bottom left corner
    pub fn bottom_left(&self) -> Point2<S>
    where
        S: ClosedAdd + ClosedSub + Zero,
    {
        &self.min + &Vector2::new(S::zero(), self.height())
    }

    /// Transform AABR by a given matrix
    pub fn transform(&self, matrix: &Matrix3<S>) -> Aabr<S>
    where
        S: RealField,
    {
        let points = [
            matrix.transform_point(&self.top_left()),
            matrix.transform_point(&self.top_right()),
            matrix.transform_point(&self.bottom_left()),
            matrix.transform_point(&self.bottom_right()),
        ];

        let (min, max) = points.iter().fold((points[0], points[3]), |(inf, sup), b| {
            (inf.inf(b), sup.sup(b))
        });

        Aabr::new(min, max)
    }

    /// Calculate area of the AABR
    pub fn area(&self) -> S
    where
        S: ClosedMul + ClosedSub,
    {
        let extent = self.extent();
        extent.x.clone() * extent.y.clone()
    }

    /// Check whether there is an overlap with other AABR
    pub fn overlaps(&self, other: &Aabr<S>) -> bool
    where
        S: PartialOrd,
    {
        self.max >= other.min && self.min <= other.max
    }

    /// Check whether the AABB contains a point
    pub fn contains_point(&self, point: &Point2<S>) -> bool
    where
        S: PartialOrd,
    {
        &self.min <= point && point <= &self.max
    }

    /// Move every edge by `value` along normal
    pub fn inflate(&self, value: S) -> Aabr<S>
    where
        S: ClosedAdd + ClosedSub,
    {
        let vector = Vector2::from_element(value);
        Aabr {
            min: &self.min - &vector,
            max: &self.max + &vector,
        }
    }
}
