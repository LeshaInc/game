use rusttype::{Font, Scale, ScaledGlyph};

/// Font utilities
pub trait FontUtil {
    /// Get reference to the font
    fn font(&self) -> &Font<'_>;

    /// Calculate width of the given text
    fn text_width(&self, size: f32, text: &str) -> f32 {
        let font = self.font();
        let scale = Scale::uniform(size);
        let glyphs = font.glyphs_for(text.chars());

        let mut last: Option<ScaledGlyph<'_>> = None;
        let mut width = 0.0;
        for glyph in glyphs {
            let glyph = glyph.scaled(scale);
            if let Some(last) = last {
                width += font.pair_kerning(scale, last.id(), glyph.id());
            }
            width += glyph.h_metrics().advance_width;
            last = Some(glyph);
        }

        width
    }
}

impl FontUtil for Font<'_> {
    fn font(&self) -> &Font<'_> {
        self
    }
}
