use nalgebra::{Scalar, Vector2};
use num_traits::{One, Zero};

/// Orientation (horizontal or vertical)
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Orientation {
    /// Vertical orientation (Y axis)
    Vertical,
    /// Horizontal orientation (X axis)
    Horizontal,
}

impl Orientation {
    /// Get orientation as a vector (left-to-right, top-to-bottom, normalized)
    pub fn as_vector<S: Zero + One + Scalar>(self) -> Vector2<S> {
        match self {
            Orientation::Vertical => Vector2::new(S::zero(), S::one()),
            Orientation::Horizontal => Vector2::new(S::one(), S::zero()),
        }
    }

    /// Get orientation orthogonal to `self`
    pub fn orthogonal(self) -> Orientation {
        match self {
            Orientation::Vertical => Orientation::Horizontal,
            Orientation::Horizontal => Orientation::Vertical,
        }
    }
}
