use nalgebra::Isometry3;

use super::drawlist3d::{DrawList3D, MeshTransform};
use super::{Mesh, Model, Texture};
use crate::assets::{AsAssetId, Assets};

/// `DrawList3D` command encoder
pub struct Encoder3D<'a> {
    /// The draw list to encode commands to
    pub list: DrawList3D,
    /// Reference to assets
    pub assets: &'a Assets,
}

impl Encoder3D<'_> {
    /// Create a new encoder
    pub fn new(assets: &Assets) -> Encoder3D<'_> {
        Encoder3D {
            list: DrawList3D::default(),
            assets,
        }
    }

    /// Add a textured mesh
    pub fn textured_mesh(
        &mut self,
        mesh: impl AsAssetId<Mesh>,
        texture: impl AsAssetId<Texture>,
        transform: Isometry3<f32>,
    ) {
        let entry = self.list.groups.entry(mesh.as_asset_id()).or_default();
        entry.transforms.push(MeshTransform::new(transform));
        entry.textures.push(Some(texture.as_asset_id()));
    }

    /// Add a plain mesh
    pub fn plain_mesh(&mut self, mesh: impl AsAssetId<Mesh>, transform: Isometry3<f32>) {
        let entry = self.list.groups.entry(mesh.as_asset_id()).or_default();
        entry.transforms.push(MeshTransform::new(transform));
        entry.textures.push(None);
    }

    /// Add a model
    pub fn model(&mut self, model: impl AsAssetId<Model>, transform: Isometry3<f32>) {
        let model = self.assets.get_by_id(model.as_asset_id()).unwrap();
        for part in &model.parts {
            // TODO: transform individual parts
            if let Some(texture) = &part.texture {
                self.textured_mesh(&part.mesh, texture, transform);
            } else {
                self.plain_mesh(&part.mesh, transform);
            }
        }
    }
}
