//! Defines 2D draw list and objects that can be rendered

use nalgebra::{Matrix3, Point2, Vector2};
use palette::Srgba;
use rusttype::Font;

use crate::assets::{Assets, Id};
use crate::util::Aabr;

/// Rendering command
#[derive(Clone, Copy, Debug)]
pub enum DrawCommand2D {
    /// Draw a rectangle specified by an index to rectangles vector inside
    /// [`DrawList2D`](struct.DrawList2D.html#structfield.rectangles)
    Rectangle(usize),
    /// Draw a text specified by an index to texts vector inside
    /// [`DrawList2D`](struct.DrawList2D.html#structfield.texts)
    Text(usize),
    /// Push current transformation matrix on stack
    PushMatrix,
    /// Transform current matrix
    Transform(Matrix3<f32>),
    /// Pop current transformation matrix from stack. Panics when there's nothing to pop
    PopMatrix,
    /// Set scissor rectangle. Pixels drawn outside it will be discarded. Beware that scissor
    /// doesn't account for transformation, its coordinates are in pixel space
    SetScissor(Aabr<f32>),
    /// Reset scissor
    ClearScissor,
}

/// List of rendering commands
#[derive(Clone, Debug, Default)]
pub struct DrawList2D {
    /// Rectangles referenced by DrawCommand::Rectangle
    pub rectangles: Vec<Rectangle>,
    /// Texts referenced by DrawCommand::Text
    pub texts: Vec<Text>,
    /// Commands to be executed in order
    pub commands: Vec<DrawCommand2D>,
}

impl DrawList2D {
    /// Move all the commands of `other` into `self`, leaving `other` empty.
    pub fn append(&mut self, other: &mut DrawList2D) {
        let rects_offset = self.rectangles.len();
        let texts_offset = self.texts.len();
        self.commands
            .extend(other.commands.drain(..).map(|cmd| match cmd {
                DrawCommand2D::Rectangle(idx) => DrawCommand2D::Rectangle(idx + rects_offset),
                DrawCommand2D::Text(idx) => DrawCommand2D::Text(idx + texts_offset),
                _ => cmd,
            }));
        self.rectangles.append(&mut other.rectangles);
        self.texts.append(&mut other.texts);
    }
}

/// 2D rectangle with rounded corners, an optional border, and a shadow.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Rectangle {
    /// Top left corner
    pub origin: Point2<f32>,
    /// Width and height
    pub size: Vector2<f32>,
    /// Background color
    pub color: Srgba,
    /// Corner radii. All corners are sharp by default
    pub corner_radii: CornerRadii,
    /// Border. If you don't want any border, specify `Border::default()`
    pub border: Border,
    /// Shadow
    pub shadow: BoxShadow,
}

impl Rectangle {
    /// Create a rectangle with specified origin, size, and color. It will have sharp corners, no
    /// border, and no shadow.
    pub fn new(
        origin: impl Into<Point2<f32>>,
        size: impl Into<Vector2<f32>>,
        color: impl Into<Srgba>,
    ) -> Rectangle {
        Rectangle {
            origin: origin.into(),
            size: size.into(),
            color: color.into(),
            corner_radii: CornerRadii::default(),
            border: Border::default(),
            shadow: BoxShadow::default(),
        }
    }

    /// Add a shadow to the rectangle
    pub fn with_shadow(self, shadow: BoxShadow) -> Rectangle {
        Rectangle { shadow, ..self }
    }

    /// Add a border to the rectangle
    pub fn with_border(self, border: Border) -> Rectangle {
        Rectangle { border, ..self }
    }

    /// Specify corner radii
    pub fn with_corner_radii(self, corner_radii: CornerRadii) -> Rectangle {
        Rectangle {
            corner_radii,
            ..self
        }
    }

    /// Get AABR of the rectangle
    pub fn aabr(&self) -> Aabr<f32> {
        let inner_aabr = Aabr {
            min: self.origin,
            max: self.origin + self.size,
        };

        let shadow_vec = Vector2::new(self.shadow.radius, self.shadow.radius);
        let shadow_aabr = Aabr {
            min: inner_aabr.min - shadow_vec + self.shadow.offset,
            max: inner_aabr.max + shadow_vec + self.shadow.offset,
        };

        inner_aabr.union(&shadow_aabr)
    }
}

/// Defines radius for each of the 4 rectangle corners
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct CornerRadii {
    /// Radius of the top left corner. Must be nonnegative
    pub top_left: f32,
    /// Radius of the top right corner
    pub top_right: f32,
    /// Radius of the bottom right corner
    pub bottom_right: f32,
    /// Radius of the bottom left corner
    pub bottom_left: f32,
}

impl CornerRadii {
    /// Define radii for each corner separately. Radii are passed in clockwise order, starting from
    /// the top left corner
    pub fn new(tl: f32, tr: f32, br: f32, bl: f32) -> CornerRadii {
        CornerRadii {
            top_left: tl,
            top_right: tr,
            bottom_right: br,
            bottom_left: bl,
        }
    }

    /// Define an equal radius for all corners
    pub fn new_equal(radius: f32) -> CornerRadii {
        CornerRadii::new(radius, radius, radius, radius)
    }

    /// Returns corner radii as an array in clockwise order, starting from the top left corner
    pub fn into_array(self) -> [f32; 4] {
        [
            self.top_left,
            self.top_right,
            self.bottom_right,
            self.bottom_left,
        ]
    }
}

impl From<f32> for CornerRadii {
    fn from(radius: f32) -> CornerRadii {
        CornerRadii::new_equal(radius)
    }
}

/// Border definition
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Border {
    /// Color
    pub color: Srgba,
    /// Nonnegative border thickness. 0 means no border
    pub thickness: f32,
}

impl Border {
    /// Create a border definition with specified thickness and color
    pub fn new(color: impl Into<Srgba>, thickness: f32) -> Border {
        Border {
            color: color.into(),
            thickness,
        }
    }
}

/// 2D box shadow
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct BoxShadow {
    /// Color
    pub color: Srgba,
    /// Blur radius
    pub radius: f32,
    /// Offset. With zero offset, the rectangle will be centered inside its shadow
    pub offset: Vector2<f32>,
}

impl BoxShadow {
    /// Create shadow with zero offset
    pub fn new(color: impl Into<Srgba>, radius: f32) -> BoxShadow {
        BoxShadow {
            color: color.into(),
            radius,
            offset: Vector2::new(0.0, 0.0),
        }
    }

    /// Specify offset
    pub fn with_offset(mut self, offset: impl Into<Vector2<f32>>) -> BoxShadow {
        self.offset = offset.into();
        self
    }
}

/// 2D text. Scaling and rotation are not recommended, stick to integer translations only if
/// possible
#[derive(Clone, Debug, PartialEq)]
pub struct Text {
    /// Position of the first character. Non LTR layouts are not supported
    pub origin: Point2<f32>,
    /// Id of the font. Must be valid, otherwise the renderer panics
    pub font_id: Id<Font<'static>>,
    /// Font size. Measured as distance between the ascent and descent lines in pixel
    pub size: f32,
    /// The text itself
    pub text: String,
    /// Text color
    pub color: Srgba,
}

impl Text {
    /// Get AABR of the text
    pub fn aabr(&self, assets: &Assets) -> Option<Aabr<f32>> {
        use rusttype::{Point, Scale};

        let font = assets.get_by_id(self.font_id).unwrap();
        let pos = Point {
            x: self.origin.x,
            y: self.origin.y,
        };
        let glyphs = font.layout(&self.text, Scale::uniform(self.size), pos);

        let mut aabr: Option<Aabr<f32>> = None;

        for glyph in glyphs {
            let bb = match glyph.pixel_bounding_box() {
                Some(v) => v,
                None => continue,
            };
            let glyph_aabr = Aabr {
                min: Point2::new(bb.min.x as f32, bb.min.y as f32),
                max: Point2::new(bb.max.x as f32, bb.max.y as f32),
            };

            aabr = Some(aabr.map(|v| v.union(&glyph_aabr)).unwrap_or(glyph_aabr));
        }

        aabr
    }
}
