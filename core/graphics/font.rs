use std::io::BufRead;

use anyhow::{anyhow, Result};
use rusttype::Font;

use crate::assets::{Asset, AssetDefaultLoader, SimpleAssetLoader};

impl Asset for Font<'static> {}

impl AssetDefaultLoader for Font<'static> {
    type Loader = FontLoader;
}

/// Font loader
#[derive(Default)]
pub struct FontLoader;

impl SimpleAssetLoader<Font<'static>> for FontLoader {
    fn load(&self, mut reader: impl BufRead) -> Result<Font<'static>> {
        let mut buf = Vec::new();
        reader.read_to_end(&mut buf)?;
        Font::try_from_vec(buf).ok_or_else(|| anyhow!("Failed to load font"))
    }
}
