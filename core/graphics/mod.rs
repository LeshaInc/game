//! Graphics API

mod encoder2d;
mod encoder3d;
mod font;
mod mesh;
mod model;
mod settings;
mod texture;

pub mod drawlist2d;
pub mod drawlist3d;

pub use self::drawlist2d::DrawList2D;
pub use self::drawlist3d::{CasterDrawList3D, DrawList3D};
pub use self::encoder2d::Encoder2D;
pub use self::encoder3d::Encoder3D;
pub use self::font::*;
pub use self::mesh::*;
pub use self::model::*;
pub use self::settings::GraphicsSettings;
pub use self::texture::*;

use std::ops::Deref;

use anyhow::Result;
use legion::systems::CommandBuffer;
use legion::{Resources, World};
use nalgebra::Vector2;
use winit::window::Window;

/// Reference to window
pub enum WindowRef<'a> {
    /// Normal backends will use this
    Pure(&'a Window),
    /// OpenGL window
    Dyn(Box<dyn Deref<Target = Window> + 'a>),
}

impl Deref for WindowRef<'_> {
    type Target = Window;

    fn deref(&self) -> &Window {
        match self {
            WindowRef::Pure(v) => v,
            WindowRef::Dyn(v) => v,
        }
    }
}

/// Graphics backend
pub trait GraphicsBackend {
    /// Get window
    fn window(&self) -> WindowRef<'_>;

    /// Update settings
    fn update_settings(&mut self, new: GraphicsSettings) -> Result<()>;

    /// Get max settings. Shouldn't change over time
    fn max_settings(&self) -> &GraphicsSettings;

    /// Render the specified world
    fn render(
        &mut self,
        world: &mut World,
        resources: &mut Resources,
        cbuf: &mut CommandBuffer,
    ) -> Result<()>;

    /// Get window resolution
    fn resolution(&self) -> Vector2<f32> {
        let window = self.window();
        let physical_resolution = window.inner_size();
        let logical_resolution = physical_resolution.to_logical(window.scale_factor());
        Vector2::new(logical_resolution.width, logical_resolution.height)
    }
}
