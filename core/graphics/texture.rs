use std::io::{BufReader, Read};
use std::path::Path;

use anyhow::Result;
use image::{DynamicImage, ImageFormat};

use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, AssetSource, NewAssets};

/// Texture asset. Contains data, unless already uploaded on GPU
#[derive(Clone)]
pub struct Texture {
    /// Texture data
    pub data: Option<DynamicImage>,
    /// If `true`, texture will be dedicated, otherwise it will be placed inside an atlas
    pub is_dedicated: bool,
}

impl Asset for Texture {}

impl AssetDefaultLoader for Texture {
    type Loader = TextureLoader;
}

/// Texture loader
#[derive(Default)]
pub struct TextureLoader;

impl AssetLoader<Texture> for TextureLoader {
    fn load(&self, _: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<Texture> {
        let format = ImageFormat::from_path(path)?;
        let mut reader = BufReader::new(source.load(path)?);
        let mut bytes = Vec::new();
        reader.read_to_end(&mut bytes)?;
        let image = image::load_from_memory_with_format(&bytes, format)?;
        Ok(Texture {
            data: Some(image),
            is_dedicated: false,
        })
    }
}

/// Dedicated texture loader. Works the same way as `TextureLoader`, but sets the `is_dedicated`
/// flag to true
#[derive(Default)]
pub struct DedicatedTextureLoader;

impl AssetLoader<Texture> for DedicatedTextureLoader {
    fn load(&self, new: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<Texture> {
        let mut texture = TextureLoader.load(new, source, path)?;
        texture.is_dedicated = true;
        Ok(texture)
    }
}
