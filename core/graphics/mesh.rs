use std::io::BufRead;

use anyhow::{bail, Result};
use nalgebra::Point3;
use ncollide3d::bounding_volume::AABB;

use crate::assets::{Asset, AssetDefaultLoader, SimpleAssetLoader};

/// Vertex type
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Vertex {
    /// Position
    pub pos: [f32; 3],
    /// Normal
    pub norm: [f32; 3],
    /// Texture coordinates
    pub tex: [f32; 2],
}

/// Mesh data
#[derive(Clone, Debug, Default)]
pub struct MeshData {
    /// Vertices
    pub vertices: Vec<Vertex>,
    /// Indices of vertices forming triangles
    pub indices: Vec<u32>,
}

/// Mesh asset. Contains data, unless already uploaded on GPU
#[derive(Clone, Debug)]
pub struct Mesh {
    /// Mesh data
    pub data: Option<MeshData>,
    /// Bounding box
    pub aabb: AABB<f32>,
}

impl Asset for Mesh {}

impl AssetDefaultLoader for Mesh {
    type Loader = MeshLoader;
}

/// Mesh loader with custom format
#[derive(Default)]
pub struct MeshLoader;

impl SimpleAssetLoader<Mesh> for MeshLoader {
    fn load(&self, mut reader: impl BufRead) -> Result<Mesh> {
        let mut buf = [0; 4];
        reader.read_exact(&mut buf)?;
        if &buf != b"MESH" {
            bail!("Invalid mesh signature");
        }

        let mut mesh_data = MeshData::default();

        reader.read_exact(&mut buf)?;
        let num_indices = u32::from_le_bytes(buf) as usize;

        mesh_data.indices.reserve(num_indices);
        for _ in 0..num_indices {
            reader.read_exact(&mut buf)?;
            mesh_data.indices.push(u32::from_le_bytes(buf));
        }

        reader.read_exact(&mut buf)?;
        let num_vertices = u32::from_le_bytes(buf) as usize;

        mesh_data.vertices.reserve(num_vertices);
        for _ in 0..num_vertices {
            reader.read_exact(&mut buf)?;
            let x = f32::from_le_bytes(buf);
            reader.read_exact(&mut buf)?;
            let y = f32::from_le_bytes(buf);
            reader.read_exact(&mut buf)?;
            let z = f32::from_le_bytes(buf);

            mesh_data.vertices.push(Vertex {
                pos: [x, y, z],
                norm: [0.0; 3],
                tex: [0.0; 2],
            });
        }

        for i in 0..num_vertices {
            reader.read_exact(&mut buf)?;
            let x = f32::from_le_bytes(buf);
            reader.read_exact(&mut buf)?;
            let y = f32::from_le_bytes(buf);
            reader.read_exact(&mut buf)?;
            let z = f32::from_le_bytes(buf);

            mesh_data.vertices[i].norm = [x, y, z];
        }

        for i in 0..num_vertices {
            reader.read_exact(&mut buf)?;
            let x = f32::from_le_bytes(buf);
            reader.read_exact(&mut buf)?;
            let y = f32::from_le_bytes(buf);

            mesh_data.vertices[i].tex = [x, y];
        }

        let first = mesh_data.vertices[0].pos;
        let init = (Point3::from(first), Point3::from(first));
        let verts = mesh_data.vertices.iter();
        let (min, max) = verts
            .map(|v| Point3::from(v.pos))
            .fold(init, |(min, max), pt| (pt.inf(&min), pt.sup(&max)));
        let aabb = AABB::new(min, max);

        Ok(Mesh {
            data: Some(mesh_data),
            aabb,
        })
    }
}
