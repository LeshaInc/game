use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use std::path::Path;

use anyhow::Result;
use serde::{Deserialize, Serialize};

/// Graphics settings
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct GraphicsSettings {
    /// Number of MSAA samples
    pub msaa: u8,
    /// Whether the shadows are enabled
    pub shadows: bool,
    /// Shadowmap resolution
    pub shadow_resolution: u32,
}

impl GraphicsSettings {
    fn load_from_file(path: &Path) -> Result<GraphicsSettings> {
        let mut contents = String::new();
        BufReader::new(File::open(path)?).read_to_string(&mut contents)?;
        Ok(toml::from_str(&contents)?)
    }

    /// Save settings
    pub fn save(&self, path: impl AsRef<Path>) -> Result<()> {
        let path = path.as_ref();
        let mut writer = BufWriter::new(File::create(path)?);
        writer.write_all(toml::to_string_pretty(self)?.as_bytes())?;
        Ok(())
    }

    /// Load settings
    pub fn load(path: impl AsRef<Path>) -> GraphicsSettings {
        let path = path.as_ref();
        match GraphicsSettings::load_from_file(&path) {
            Ok(v) => v,
            Err(_) => Default::default(),
        }
    }

    /// Clamp settings
    pub fn clamp(&self, max: &GraphicsSettings) -> GraphicsSettings {
        GraphicsSettings {
            msaa: self.msaa.max(1).min(max.msaa),
            shadows: self.shadows,
            shadow_resolution: self
                .shadow_resolution
                .next_power_of_two()
                .max(128)
                .min(max.shadow_resolution),
        }
    }
}

impl Default for GraphicsSettings {
    fn default() -> GraphicsSettings {
        GraphicsSettings {
            msaa: 1,
            shadows: true,
            shadow_resolution: 2048,
        }
    }
}
