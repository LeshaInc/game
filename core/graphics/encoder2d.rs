//! Encoding commands for the `DrawList2D`

use nalgebra::{Matrix3, Point2, UnitComplex, Vector2};
use palette::Srgba;
use rusttype::Font;

use super::drawlist2d::{
    Border, BoxShadow, CornerRadii, DrawCommand2D, DrawList2D, Rectangle, Text,
};
use crate::assets::Id;
use crate::util::Aabr;

/// `DrawList2D` command encoder
#[derive(Clone, Debug, Default)]
pub struct Encoder2D {
    /// The draw list to encode commands to
    pub list: DrawList2D,
}

impl Encoder2D {
    /// Create an encoder for the specified draw list
    pub fn new(list: DrawList2D) -> Encoder2D {
        Encoder2D { list }
    }

    /// Add a command to the inner `DrawList`
    pub fn add_command(&mut self, command: DrawCommand2D) {
        self.list.commands.push(command);
    }

    /// Add a rectangle to the inner `DrawList`. Returns the index of it.
    pub fn add_rectangle(&mut self, rectangle: Rectangle) -> usize {
        let idx = self.list.rectangles.len();
        self.list.rectangles.push(rectangle);
        idx
    }

    /// Add a text to the inner `DrawList`. Returns the index of it.
    pub fn add_text(&mut self, text: Text) -> usize {
        let idx = self.list.texts.len();
        self.list.texts.push(text);
        idx
    }

    /// Push copy of the current transformation matrix on stack
    pub fn push_matrix(&mut self) {
        self.add_command(DrawCommand2D::PushMatrix);
    }

    /// Pop a transformation matrix from the stack and set is as current
    pub fn pop_matrix(&mut self) {
        self.add_command(DrawCommand2D::PopMatrix);
    }

    /// Post-transform current view by some matrix
    pub fn transform(&mut self, matrix: Matrix3<f32>) {
        self.add_command(DrawCommand2D::Transform(matrix));
    }

    /// Translate current view by an offset
    pub fn translate(&mut self, offset: impl Into<Vector2<f32>>) {
        self.transform(Matrix3::new_translation(&offset.into()));
    }

    /// Scale current view by the values specified in vector on each axis
    pub fn scale(&mut self, axis: impl Into<Vector2<f32>>) {
        self.transform(Matrix3::new_nonuniform_scaling(&axis.into()));
    }

    /// Rotate current view by a unit complex. To create it from angle, use
    /// `UnitComplex::new(radians)`
    pub fn rotate(&mut self, rot: UnitComplex<f32>) {
        self.transform(rot.into());
    }

    /// Draw a rectangle using a builder
    ///
    /// ```no_run
    /// # use game::graphics::Encoder2D;
    /// # use game::graphics::drawlist2d::Border;
    /// # use palette::Srgba;
    /// # let mut encoder = Encoder2D::default();
    /// encoder
    ///     .rect([10.0, 10.0], [200.0, 200.0], (1.0, 0.0, 1.0, 1.0))
    ///     .with_border(Border::new((0.0, 0.0, 0.0, 1.0), 2.0));
    /// ```
    pub fn rect(
        &mut self,
        pos: impl Into<Point2<f32>>,
        size: impl Into<Vector2<f32>>,
        color: impl Into<Srgba>,
    ) -> RectangleBuilder<'_> {
        RectangleBuilder {
            encoder: self,
            rectangle: Rectangle::new(pos.into(), size.into(), color.into()),
        }
    }

    /// Draw text
    pub fn text(
        &mut self,
        font_id: impl Into<Id<Font<'static>>>,
        pos: impl Into<Point2<f32>>,
        size: f32,
        color: impl Into<Srgba>,
        text: impl Into<String>,
    ) {
        let text = Text {
            font_id: font_id.into(),
            origin: pos.into(),
            size,
            color: color.into(),
            text: text.into(),
        };
        let idx = self.add_text(text);
        self.add_command(DrawCommand2D::Text(idx));
    }

    /// Draw a ready rectangle (without builder)
    pub fn rect_ready(&mut self, rectangle: Rectangle) {
        let idx = self.add_rectangle(rectangle);
        self.add_command(DrawCommand2D::Rectangle(idx));
    }

    /// Set scissor rectangle. Pixels drawn outside it will be discarded. Beware that scissor
    /// doesn't account for transformation, its coordinates are in pixel space
    pub fn set_scissor(&mut self, aabr: Aabr<f32>) {
        self.add_command(DrawCommand2D::SetScissor(aabr));
    }

    /// Clear scissor rectangle
    pub fn clear_scissor(&mut self) {
        self.add_command(DrawCommand2D::ClearScissor);
    }

    /// Render AABR edges with 4 rectangles
    pub fn debug_aabr(&mut self, color: impl Into<Srgba>, aabr: Aabr<f32>) {
        let color = color.into();
        self.rect(aabr.min, Vector2::new(1.0, aabr.height()), color);
        self.rect(
            aabr.min + Vector2::new(1.0, 0.0),
            Vector2::new(aabr.width() - 2.0, 1.0),
            color,
        );
        self.rect(
            aabr.min + Vector2::new(aabr.width() - 1.0, 0.0),
            Vector2::new(1.0, aabr.height()),
            color,
        );
        self.rect(
            aabr.min + Vector2::new(1.0, aabr.height() - 1.0),
            Vector2::new(aabr.width() - 2.0, 1.0),
            color,
        );
    }
}

/// Rectangle configurator. The appropriate `DrawCommand2D` is added to the list on `drop()`. If
/// the value is leaked, no action is performed.
#[derive(Debug)]
pub struct RectangleBuilder<'a> {
    pub rectangle: Rectangle,
    encoder: &'a mut Encoder2D,
}

impl RectangleBuilder<'_> {
    /// Add a shadow
    pub fn with_shadow(mut self, shadow: BoxShadow) -> Self {
        self.rectangle.shadow = shadow;
        self
    }

    /// Add a border
    pub fn with_border(mut self, border: Border) -> Self {
        self.rectangle.border = border;
        self
    }

    /// Specify corner radii (0 by default)
    pub fn with_corner_radii(mut self, corner_radii: CornerRadii) -> Self {
        self.rectangle.corner_radii = corner_radii;
        self
    }
}

impl Drop for RectangleBuilder<'_> {
    fn drop(&mut self) {
        let idx = self.encoder.add_rectangle(self.rectangle);
        self.encoder.add_command(DrawCommand2D::Rectangle(idx));
    }
}
