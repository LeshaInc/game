//! List of 3D objects to be rendered

use fxhash::FxHashMap;
use nalgebra::{Isometry3, Matrix4, UnitQuaternion};

use super::{Mesh, Texture};
use crate::assets::Id;

#[derive(Debug, Clone, Copy)]
/// Mesh transform
pub struct MeshTransform {
    /// Homogeneous transformation matrix (includes rotation)
    pub matrix: Matrix4<f32>,
    /// Rotation
    pub rotation: UnitQuaternion<f32>,
}

impl MeshTransform {
    /// Create a new `MeshTransform` from an `Isometry3`
    pub fn new(isometry: Isometry3<f32>) -> MeshTransform {
        MeshTransform {
            matrix: isometry.to_homogeneous(),
            rotation: isometry.rotation,
        }
    }
}

/// Group of objects with common mesh
#[derive(Clone, Debug, Default)]
pub struct MeshGroup {
    /// Transforms
    pub transforms: Vec<MeshTransform>,
    /// Textures
    pub textures: Vec<Option<Id<Texture>>>,
}

/// List of 3D objects to render
#[derive(Clone, Debug, Default)]
pub struct DrawList3D {
    /// Mesh groups
    pub groups: FxHashMap<Id<Mesh>, MeshGroup>,
}

#[allow(clippy::len_without_is_empty)] // I don't need it
impl DrawList3D {
    /// Moves all the objects of `other` into `self`, leaving `other` empty.
    pub fn append(&mut self, other: &mut DrawList3D) {
        for (mesh, that) in other.groups.iter_mut() {
            let this = self.groups.entry(*mesh).or_default();
            this.transforms.append(&mut that.transforms);
            this.textures.append(&mut that.textures);
        }
    }

    /// Get number of objects
    pub fn len(&self) -> usize {
        self.groups.iter().map(|(_, g)| g.transforms.len()).sum()
    }

    /// Iterate over objects
    pub fn iter<'a>(
        &'a self,
    ) -> impl Iterator<Item = (Id<Mesh>, &'a MeshTransform, Option<Id<Texture>>)> + 'a {
        self.groups.iter().flat_map(|(mesh, g)| {
            let tran = g.transforms.iter();
            tran.zip(&g.textures)
                .map(move |(tr, tex)| (*mesh, tr, *tex))
        })
    }

    /// Iterate over part of the objects associated with the thread index
    pub fn split<'a>(
        &'a self,
        num_threads: usize,
        thread_idx: usize,
    ) -> impl Iterator<Item = (Id<Mesh>, &'a MeshTransform, Option<Id<Texture>>)> + 'a {
        self.groups.iter().flat_map(move |(mesh, g)| {
            let mut len = g.transforms.len() / num_threads;
            let offset = len * thread_idx;
            if thread_idx == num_threads - 1 {
                len += g.transforms.len() % num_threads;
            }

            let tran = g.transforms[offset..offset + len].iter();
            tran.zip(&g.textures)
                .map(move |(tr, tex)| (*mesh, tr, *tex))
        })
    }
}

/// Draw list for shadow casters
pub struct CasterDrawList3D(pub DrawList3D);
