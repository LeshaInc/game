use std::path::{Path, PathBuf};

use anyhow::Result;
use serde::{Deserialize, Serialize};

use super::{Mesh, Texture};
use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, AssetSource, Handle, NewAssets};

/// Part of the model
#[derive(Clone, Debug)]
pub struct ModelPart {
    /// Mesh
    pub mesh: Handle<Mesh>,
    /// Optional texture
    pub texture: Option<Handle<Texture>>,
}

/// Model part definition, which stores path to the mesh and an optional texture
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ModelPartDefinition {
    /// Path to mesh
    pub mesh: PathBuf,
    /// Path to texture
    pub texture: Option<PathBuf>,
}

/// 3D textured model definition
#[derive(Clone, Debug)]
pub struct Model {
    /// Parts of the model
    pub parts: Vec<ModelPart>,
}

impl Asset for Model {}

impl AssetDefaultLoader for Model {
    type Loader = ModelLoader;
}

/// Stores paths to all model meshes and textures
pub type ModelDefinition = Vec<ModelPartDefinition>;

/// RON model format
#[derive(Default)]
pub struct ModelLoader;

impl AssetLoader<Model> for ModelLoader {
    fn load(&self, new: &NewAssets, source: &dyn AssetSource, path: &Path) -> Result<Model> {
        let def: ModelDefinition = ron::de::from_reader(source.load(path)?)?;
        let parts = def.into_iter().map(|v| {
            let mesh = new.load(v.mesh);
            let texture = v.texture.map(|path| new.load(path));
            ModelPart { mesh, texture }
        });

        Ok(Model {
            parts: parts.collect(),
        })
    }
}
