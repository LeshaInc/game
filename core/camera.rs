use nalgebra::{
    Isometry3, Matrix4, Orthographic3, Point2, Point3, Translation3, Unit, UnitQuaternion, Vector3,
};
use ncollide3d::query::PointQuery;
use ncollide3d::shape::{Cuboid, Plane, SupportMap};

use crate::bvh::OrthoPlanes;

/// 3D orthographic camera
#[derive(Clone, Copy, Debug)]
pub struct Camera {
    /// Orthographic scale
    pub scale: f32,
    /// Screen aspect ratio (`W/H`)
    pub aspect: f32,
    /// Near clipping plane
    pub znear: f32,
    /// Far clipping plane
    pub zfar: f32,
    /// Orthographic projection matrix. Updated by calling `update_projection()`
    pub projection: Orthographic3<f32>,
    /// Target position on the `z=0` plane
    pub position: Point2<f32>,
    /// Distance from target to the eye along the direction vector
    pub distance: f32,
    /// Yaw (Z rotation in radians)
    pub yaw: f32,
    /// Pitch (Y rotation in radians)
    pub pitch: f32,
    /// Normalized direction vector. Updated by calling `update_direction()`
    pub direction: Unit<Vector3<f32>>,
    /// View matrix. Updated by calling `update_view()`
    pub view: Isometry3<f32>,
}

impl Camera {
    /// Create a new camera with default configuration
    pub fn new(position: Point2<f32>) -> Camera {
        let mut camera = Camera {
            scale: 5.0,
            aspect: 1.33,
            znear: 0.01,
            zfar: 1e3,
            projection: Orthographic3::new(-1.0, 1.0, -1.0, 1.0, 0.0, 1.0),
            position,
            distance: 100.0,
            yaw: 0.0,
            pitch: 45f32.to_radians(),
            direction: Unit::new_unchecked(Vector3::z()),
            view: Isometry3::identity(),
        };

        camera.update_view();
        camera.update_projection();
        camera
    }

    /// Update the projection matrix
    pub fn update_projection(&mut self) {
        let height = self.scale;
        let width = height * self.aspect;
        self.projection = Orthographic3::new(-width, width, -height, height, self.znear, self.zfar);
    }

    /// Update the direction
    pub fn update_direction(&mut self) {
        let rotation = UnitQuaternion::from_euler_angles(0.0, self.pitch, self.yaw);
        self.direction = Unit::new_normalize(rotation * Vector3::new(0.0, 0.0, -1.0));
    }

    /// Eye position
    pub fn eye_position(&self) -> Point3<f32> {
        let tgt = Point3::new(self.position.x, self.position.y, 0.0);
        tgt - self.distance * self.direction.into_inner()
    }

    /// Calculate the view plane and its transformation
    pub fn view_plane(&self) -> (Plane<f32>, Isometry3<f32>) {
        (
            Plane::new(self.direction),
            Isometry3::from_parts(
                Translation3::from(self.eye_position().coords),
                UnitQuaternion::identity(),
            ),
        )
    }

    /// Update the view matrix. Also updates the direction
    pub fn update_view(&mut self) {
        self.update_direction();
        let tgt = Point3::new(self.position.x, self.position.y, 0.0);
        let eye = self.eye_position();
        self.view = Isometry3::look_at_rh(&eye, &tgt, &Vector3::z());
    }

    /// Get the visible world cuboid and its transformation
    pub fn world_cuboid(&self, height: f32) -> (Cuboid<f32>, Isometry3<f32>) {
        let xsize = self.scale * self.aspect;
        let ysize = self.scale / self.pitch.sin();
        let halfsize = Vector3::new(ysize, xsize, height);
        let mut trans = Isometry3::identity();
        trans.append_rotation_mut(&UnitQuaternion::from_euler_angles(0.0, 0.0, self.yaw));
        trans.append_translation_mut(&Translation3::new(self.position.x, self.position.y, 0.0));

        (Cuboid::new(halfsize), trans)
    }

    /// Calculates optimal znear/zfar values and sets them. Doesn't update the projection
    pub fn fit_near_far(&mut self, height: f32) {
        let (cuboid, cuboid_trans) = self.world_cuboid(height);
        let near = cuboid.support_point(&cuboid_trans, &-self.direction);
        let far = cuboid.support_point(&cuboid_trans, &self.direction);

        let (plane, plane_trans) = self.view_plane();
        self.znear = plane.distance_to_point(&plane_trans, &near, true);
        self.zfar = plane.distance_to_point(&plane_trans, &far, true);
    }

    /// Calculates optimal shadow matrix for the given direction and scene height
    pub fn shadow_matrix(
        &self,
        direction: Unit<Vector3<f32>>,
        height: f32,
    ) -> (Matrix4<f32>, OrthoPlanes) {
        let (cuboid, cuboid_trans) = self.world_cuboid(height);

        let near_pt = cuboid.support_point(&cuboid_trans, &-direction.as_ref());
        let far_pt = cuboid.support_point(&cuboid_trans, &direction.as_ref());

        let offset = 0.5;
        let center = near_pt - direction.as_ref() * offset;

        let plane = Plane::new(direction);
        let plane_trans = Isometry3::from_parts(
            Translation3::from(center.coords),
            UnitQuaternion::identity(),
        );

        let margin = 0.0;

        let znear = offset;
        let zfar = plane.distance_to_point(&plane_trans, &far_pt, true);

        let right = direction.cross(&Vector3::z()).normalize();
        let top = right.cross(&direction).normalize();

        let left_pt = cuboid.support_point(&cuboid_trans, &-right);
        let left_proj = plane.project_point(&plane_trans, &left_pt, true).point;
        let left_side = (left_proj - center).dot(&right) - margin;

        let right_pt = cuboid.support_point(&cuboid_trans, &right);
        let right_proj = plane.project_point(&plane_trans, &right_pt, true).point;
        let right_side = (right_proj - center).dot(&right) + margin;

        let bottom_pt = cuboid.support_point(&cuboid_trans, &-top);
        let bottom_proj = plane.project_point(&plane_trans, &bottom_pt, true).point;
        let bottom_side = (bottom_proj - center).dot(&top) - margin;

        let top_pt = cuboid.support_point(&cuboid_trans, &top);
        let top_proj = plane.project_point(&plane_trans, &top_pt, true).point;
        let top_side = (top_proj - center).dot(&top) + margin;

        let ortho = Orthographic3::new(left_side, right_side, bottom_side, top_side, znear, zfar);
        let view = Isometry3::look_at_rh(&center, &(center + direction.as_ref()), &Vector3::z());
        let planes = OrthoPlanes::new(&center, direction.as_ref(), &ortho);

        (ortho.to_homogeneous() * view.to_homogeneous(), planes)
    }
}
