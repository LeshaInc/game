//! Bounding volume hierarchy stuff

use legion::systems::{ParallelRunnable, Resources, SystemBuilder};
use legion::{Entity, IntoQuery, Read, TryWrite};
use nalgebra::{Orthographic3, Point3, Vector3};
use ncollide3d::bounding_volume::AABB;
use ncollide3d::partitioning::{DBVTLeaf, DBVTLeafId, DBVT};

use crate::assets::{Assets, Handle};
use crate::graphics::Mesh;
use crate::util::AABBOps;
use crate::{Camera, Transform, TransformChanged};

/// Id of the entity inside the bounding volume hierarchy
pub struct BVHId(pub DBVTLeafId);

/// Bounding volume hierarchy
pub type BVH = DBVT<f32, Entity, AABB<f32>>;

/// Build the BVH updater system
pub fn update_bvh_system(res: &mut Resources) -> impl ParallelRunnable {
    res.insert(BVH::new());
    SystemBuilder::new("update-bvh")
        .with_query(<(
            Entity,
            Read<Transform>,
            Read<TransformChanged>,
            Read<Handle<Mesh>>,
            TryWrite<BVHId>,
        )>::query())
        .write_resource::<BVH>()
        .read_resource::<Assets>()
        .build(|cbuf, world, (bvh, assets), query| {
            for (&entity, trans, trans_changed, mesh, bvh_id) in query.iter_mut(world) {
                if !trans_changed.0 && bvh_id.is_some() {
                    continue;
                }

                if let Some(id) = &bvh_id {
                    bvh.remove(id.0);
                }

                let aabb = assets[mesh].aabb.transform_by_similarity(&trans.0);
                let new_id = bvh.insert(DBVTLeaf::new(aabb, entity));
                if let Some(id) = bvh_id {
                    id.0 = new_id;
                } else {
                    cbuf.add_component(entity, BVHId(new_id));
                }
            }
        })
}

fn plane_vs_aabb(aabb: &AABB<f32>, (plane_normal, plane_dist): (Vector3<f32>, f32)) -> PlaneResult {
    let center = aabb.center();
    let extents = aabb.half_extents();
    let radius = extents.dot(&plane_normal.abs());
    let dist = plane_normal.dot(&center.coords) - plane_dist;
    if dist < -radius {
        PlaneResult::InFront
    } else if dist < radius {
        PlaneResult::Intersects
    } else {
        PlaneResult::Behind
    }
}

/// Plane intersection result
#[derive(Debug, PartialEq)]
enum PlaneResult {
    InFront,
    Intersects,
    Behind,
}

/// Flags used to skip unnecessary intersection tests
#[derive(Clone, Copy, Debug)]
pub struct PlaneFlags(u8);

impl PlaneFlags {
    /// True if the AABB is in front all the planes
    pub fn in_front_all(self) -> bool {
        self.0 == 0
    }
}

impl Default for PlaneFlags {
    fn default() -> PlaneFlags {
        PlaneFlags(0b1111)
    }
}

/// Planes representing the orthographic camera
pub struct OrthoPlanes {
    planes: [(Vector3<f32>, f32); 4],
}

impl OrthoPlanes {
    /// Get planes from camera components
    pub fn new(pos: &Point3<f32>, dir: &Vector3<f32>, proj: &Orthographic3<f32>) -> OrthoPlanes {
        let right_normal = dir.cross(&Vector3::z()).normalize();
        let left_normal = -right_normal;
        let top_normal = right_normal.cross(&dir).normalize();
        let bottom_normal = -top_normal;
        let pos = pos.coords;
        let right_dist = right_normal.dot(&(pos + right_normal * proj.right().abs()));
        let left_dist = left_normal.dot(&(pos + left_normal * proj.left().abs()));
        let top_dist = top_normal.dot(&(pos + top_normal * proj.top().abs()));
        let bottom_dist = bottom_normal.dot(&(pos + bottom_normal * proj.bottom().abs()));
        OrthoPlanes {
            planes: [
                (left_normal, left_dist),
                (right_normal, right_dist),
                (top_normal, top_dist),
                (bottom_normal, bottom_dist),
            ],
        }
    }

    /// Get planes from orthographic camera
    pub fn from_camera(camera: &Camera) -> OrthoPlanes {
        OrthoPlanes::new(
            &camera.eye_position(),
            camera.direction.as_ref(),
            &camera.projection,
        )
    }

    /// Perform the intersection test with specified AABB. `None` is returned if the AABB is not
    /// visible
    pub fn intersect(&self, aabb: &AABB<f32>, parent_flags: PlaneFlags) -> Option<PlaneFlags> {
        let mut flags = parent_flags;
        for (i, plane) in self.planes.iter().enumerate() {
            let mask = 1u8 << (i as u8);
            if parent_flags.0 & mask > 0 {
                match plane_vs_aabb(aabb, *plane) {
                    PlaneResult::Behind => return None,
                    PlaneResult::InFront => flags.0 &= !mask,
                    _ => (),
                }
            };
        }
        Some(flags)
    }
}
