//! Dynamic texture atlas

use std::hash::Hash;
use std::ops::Range;

use anyhow::{Context, Result};
use fxhash::FxHashMap;
use glium::texture::{ClientFormat, RawImage2d, Texture2d};
use glium::uniforms::MagnifySamplerFilter;
use glium::{BlitTarget, Display, Rect, Surface};
use guillotiere::{AllocId, AtlasAllocator, Rectangle, Size};

#[derive(Debug)]
struct AllocatedEntry {
    id: AllocId,
    rect: Rectangle,
    is_live: bool,
}

#[derive(Debug)]
struct NewEntry<K> {
    key: K,
    size: (u32, u32),
    /// Texture data range inside atlas.new_data
    data_range: Range<usize>,
}

/// Dynamic texture atlas
pub struct TextureAtlas<K> {
    /// Texture with contents. Will grow over time
    pub texture: Texture2d,
    format: ClientFormat,

    allocator: AtlasAllocator,
    allocated: FxHashMap<K, AllocatedEntry>,
    new_entries: Vec<NewEntry<K>>,
    new_data: Vec<u8>,
}

impl<K> std::fmt::Debug for TextureAtlas<K> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TextureAtlas")
            .field("texture", &self.texture)
            .field("format", &self.format)
            .finish()
    }
}

impl<K: Hash + Eq> TextureAtlas<K> {
    /// Create a new texture atlas
    pub fn new(display: &Display, format: ClientFormat) -> Result<TextureAtlas<K>> {
        let texture = Texture2d::empty(display, 512, 512).context("Can't create atlas texture")?;
        Ok(TextureAtlas {
            texture,
            format,
            allocator: AtlasAllocator::new(Size::new(512, 512)),
            allocated: FxHashMap::default(),
            new_entries: Vec::new(),
            new_data: Vec::new(),
        })
    }

    /// Get rect of the entry, or None if it doesn't exist
    pub fn get_rect(&self, key: &K) -> Option<Rectangle> {
        self.allocated.get(key).map(|e| e.rect)
    }

    /// Get rect of the entry in normalized floating coordinates, or None if it doesn't exist
    pub fn get_rect_f32(&self, key: &K) -> Option<[f32; 4]> {
        let rect = self.get_rect(key)?;
        let size = self.texture.dimensions();
        let size = (size.0 as f32, size.1 as f32);
        Some([
            (rect.min.x as f32) / size.0,
            (rect.min.y as f32) / size.1,
            (rect.width() as f32) / size.0,
            (rect.height() as f32) / size.1,
        ])
    }

    /// Add an entry to the texture atlas. If it is already uploaded, mark it as live, otherwise
    /// upload it
    pub fn add_raw(&mut self, key: K, size: (u32, u32), data: &[u8]) {
        match self.allocated.get_mut(&key) {
            Some(e) if e.rect.size() == Size::new(size.0 as _, size.1 as _) => {
                e.is_live = true;
                return;
            }
            Some(e) => {
                self.allocator.deallocate(e.id);
                self.allocated.remove(&key);
            }
            _ => {}
        }

        let start = self.new_data.len();
        self.new_data.extend(data.iter().copied());
        let end = self.new_data.len();

        self.new_entries.push(NewEntry {
            key,
            size,
            data_range: start..end,
        });
    }

    /// Add an entry to the texture atlas. If it is already uploaded, mark it as live, otherwise call
    /// `data_fn` to get its pixel data
    pub fn add<F>(&mut self, key: K, size: (u32, u32), data_fn: F)
    where
        F: FnOnce(&mut [u8]),
    {
        match self.allocated.get_mut(&key) {
            Some(e) if e.rect.size() == Size::new(size.0 as _, size.1 as _) => {
                e.is_live = true;
                return;
            }
            Some(e) => {
                self.allocator.deallocate(e.id);
                self.allocated.remove(&key);
            }
            _ => {}
        }

        let start = self.new_data.len();
        let len = self.format.get_size() * (size.0 as usize) * (size.1 as usize);
        let end = start + len;
        self.new_data.extend((0..len).map(|_| 0));
        data_fn(&mut self.new_data[start..end]);

        self.new_entries.push(NewEntry {
            key,
            size,
            data_range: start..end,
        });
    }

    /// Deallocate one allocated entry
    pub fn deallocate(&mut self, key: &K) {
        let id = match self.allocated.get(key) {
            Some(entry) => entry.id,
            _ => return,
        };
        self.allocator.deallocate(id);
    }

    /// Deallocate dead entries
    pub fn deallocate_dead(&mut self) {
        let allocator = &mut self.allocator;
        self.allocated.retain(|_, entry| {
            if !entry.is_live {
                allocator.deallocate(entry.id);
            }
            entry.is_live
        });

        // mark all as dead
        for entry in self.allocated.values_mut() {
            entry.is_live = false;
        }
    }

    /// Pack all new entries and upload them to the texture atlas, growing it if necessary
    pub fn flush(&mut self, display: &Display) -> Result<()> {
        // allocate new entries
        while let Some(entry) = self.new_entries.last_mut() {
            let alloc = self
                .allocator
                .allocate(Size::new(entry.size.0 as _, entry.size.1 as _));

            let alloc = match alloc {
                Some(alloc) => alloc,
                None => {
                    self.grow(display)?;
                    continue;
                }
            };

            let entry = self.new_entries.pop().unwrap();
            self.allocated.insert(
                entry.key,
                AllocatedEntry {
                    id: alloc.id,
                    rect: alloc.rectangle,
                    is_live: false,
                },
            );

            // upload rect
            let rect = Rect {
                left: alloc.rectangle.min.x as _,
                bottom: alloc.rectangle.min.y as _,
                width: alloc.rectangle.width() as _,
                height: alloc.rectangle.height() as _,
            };

            let data = &self.new_data[entry.data_range];
            let raw_tex = RawImage2d {
                data: data.into(),
                width: rect.width,
                height: rect.height,
                format: self.format,
            };

            self.texture.write(rect, raw_tex);
        }

        self.new_data.clear();

        Ok(())
    }

    /// Grow atlas by a factor of 2 on both axis
    fn grow(&mut self, display: &Display) -> Result<()> {
        let old_size = self.texture.dimensions();
        let new_size = (old_size.0 * 2, old_size.1 * 2);
        let new_tex = Texture2d::empty(display, new_size.0, new_size.1)
            .with_context(|| format!("Cannot grow atlas from {:?} to {:?}", old_size, new_size))?;

        let new_surf = new_tex.as_surface();
        let old_surf = self.texture.as_surface();

        let filter = MagnifySamplerFilter::Linear;
        let src_rect = Rect {
            left: 0,
            bottom: 0,
            width: old_size.0,
            height: old_size.1,
        };
        let dst_rect = BlitTarget {
            left: 0,
            bottom: 0,
            width: old_size.0 as _,
            height: old_size.1 as _,
        };
        new_surf.blit_from_simple_framebuffer(&old_surf, &src_rect, &dst_rect, filter);

        self.texture = new_tex;

        self.allocator
            .grow(Size::new(new_size.0 as _, new_size.1 as _));

        Ok(())
    }
}
