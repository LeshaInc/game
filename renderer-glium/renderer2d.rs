//! 2D renderer

use std::ops::Range;

use anyhow::{anyhow, Context, Result};
use glium::glutin::dpi::LogicalSize;
use glium::index::{NoIndices, PrimitiveType};
use glium::texture::ClientFormat;
use glium::{Blend, DrawParameters, Program, Rect, Surface, VertexBuffer};
use nalgebra::{Matrix3, Point2, Vector2};
use palette::Srgba;
use rusttype::{Font, GlyphId, PositionedGlyph};

use super::atlas::TextureAtlas;
use super::{to_rgba, InitializationCtx, RenderingCtx};
use game_core::assets::{Assets, Id};
use game_core::graphics::drawlist2d::{DrawCommand2D, DrawList2D, Rectangle, Text};
use game_core::util::{Aabr, AabrTree, Stats};

#[derive(Debug, Clone, Copy, glium_derive::Vertex)]
struct Vertex {
    in_pos: [f32; 2],
}

impl Vertex {
    const RECT: [Vertex; 6] = [
        Vertex { in_pos: [0.0, 0.0] },
        Vertex { in_pos: [1.0, 0.0] },
        Vertex { in_pos: [0.0, 1.0] },
        Vertex { in_pos: [1.0, 0.0] },
        Vertex { in_pos: [1.0, 1.0] },
        Vertex { in_pos: [0.0, 1.0] },
    ];
}

#[derive(Debug, Clone, Copy, glium_derive::Vertex)]
struct RectInstance {
    in_transform_col0_1: [f32; 4],
    in_transform_col2_shadow_offset: [f32; 4],
    in_color: [f32; 4],
    in_shadow_color: [f32; 4],
    in_border_color: [f32; 4],
    in_corner_radii: [f32; 4],
    in_size_radius_thickness: [f32; 4],
}

impl RectInstance {
    fn new(rect: &Rectangle, dpi: f32, view_proj: &Matrix3<f32>) -> RectInstance {
        let aabr = rect.aabr();
        let margin = Vector2::from_element(1.0 / dpi);
        let min = aabr.min.coords - margin;
        let size = aabr.extent() + margin * 2.0;
        let model = Matrix3::new_nonuniform_scaling(&size).append_translation(&min);
        let mvp = view_proj * model;

        let col0 = mvp.column(0).xy();
        let col1 = mvp.column(1).xy();
        let col2 = mvp.column(2).xy();

        RectInstance {
            in_transform_col0_1: [col0.x, col0.y, col1.x, col1.y],
            in_transform_col2_shadow_offset: [
                col2.x,
                col2.y,
                rect.shadow.offset.x,
                rect.shadow.offset.y,
            ],
            in_color: to_rgba(rect.color),
            in_shadow_color: to_rgba(rect.shadow.color),
            in_border_color: to_rgba(rect.border.color),
            in_corner_radii: rect.corner_radii.into_array(),
            in_size_radius_thickness: [
                rect.size.x + margin.x * 2.0,
                rect.size.y + margin.x * 2.0,
                rect.shadow.radius,
                rect.border.thickness,
            ],
        }
    }
}

#[derive(Debug)]
struct RectangleRenderer {
    vertex_buffer: VertexBuffer<Vertex>,
    instance_buffer: Option<VertexBuffer<RectInstance>>,
    program: Program,
    rect_instances: Vec<RectInstance>,
    batch_ranges: Vec<Range<u32>>,
}

impl RectangleRenderer {
    fn new(ctx: &InitializationCtx<'_>) -> Result<RectangleRenderer> {
        Ok(RectangleRenderer {
            vertex_buffer: VertexBuffer::immutable(ctx.display, &Vertex::RECT)
                .context("Cannot create vertex buffer")?,
            program: ctx.compile_program("rects", "rects", Default::default())?,
            instance_buffer: None,
            rect_instances: Vec::new(),
            batch_ranges: Vec::new(),
        })
    }

    fn add_rect(&mut self, ctx: &RenderingCtx<'_>, rect: &Rectangle, view_proj: &Matrix3<f32>) {
        self.rect_instances
            .push(RectInstance::new(rect, ctx.dpi_factor, view_proj));
    }

    fn end_batch(&mut self) -> usize {
        let start = self.batch_ranges.last().map(|last| last.end).unwrap_or(0);
        let end = self.rect_instances.len() as u32;
        self.batch_ranges.push(start..end);
        self.batch_ranges.len() - 1
    }

    fn upload(&mut self, ctx: &mut RenderingCtx<'_>) -> Result<()> {
        ctx.stats.tris += self.rect_instances.len();
        self.instance_buffer = Some(VertexBuffer::immutable(ctx.display, &self.rect_instances)?);

        Ok(())
    }

    fn draw(
        &mut self,
        ctx: &mut RenderingCtx<'_>,
        surface: &mut impl Surface,
        batch_id: usize,
        scissor: Rect,
    ) -> Result<()> {
        let range = &self.batch_ranges[batch_id];
        if range.start == range.end {
            return Ok(());
        }

        let uniforms = glium::uniform! {
            u_dpi_factor: ctx.dpi_factor
        };

        let params = DrawParameters {
            blend: Blend::alpha_blending(),
            scissor: Some(scissor),
            ..Default::default()
        };

        let ibuf = self.instance_buffer.as_ref().unwrap();
        let slice = ibuf
            .slice(range.start as _..range.end as _)
            .ok_or_else(|| anyhow!("Draw out of range"))?;
        let slice = slice
            .per_instance()
            .map_err(|_| anyhow!("Instancing is not supported"))?;

        ctx.stats.drawcalls += 1;
        surface.draw(
            (&self.vertex_buffer, slice),
            NoIndices(PrimitiveType::TrianglesList),
            &self.program,
            &uniforms,
            &params,
        )?;

        Ok(())
    }

    fn reset(&mut self) {
        self.rect_instances.clear();
        self.batch_ranges.clear();
        self.instance_buffer = None;
    }
}

#[derive(Debug, Clone, Copy, glium_derive::Vertex)]
struct GlyphInstance {
    in_uv_rect: [f32; 4],
    in_color: [f32; 4],
    in_transform_col0_1: [f32; 4],
    in_transform_col2: [f32; 2],
}

impl GlyphInstance {
    fn new(
        bb: rusttype::Rect<i32>,
        color: Srgba,
        dpi: f32,
        view_proj: &Matrix3<f32>,
    ) -> GlyphInstance {
        let size = Vector2::new(bb.width() as f32 / dpi, bb.height() as f32 / dpi);
        let pos = Vector2::new(bb.min.x as f32 / dpi, bb.min.y as f32 / dpi);
        let model = Matrix3::new_nonuniform_scaling(&size).append_translation(&pos);
        let mvp = view_proj * model;

        let col0 = mvp.column(0).xy();
        let col1 = mvp.column(1).xy();
        let col2 = mvp.column(2).xy();

        GlyphInstance {
            in_uv_rect: [0.0; 4],
            in_color: to_rgba(color),
            in_transform_col0_1: [col0.x, col0.y, col1.x, col1.y],
            in_transform_col2: col2.into(),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct GlyphKey {
    font_id: Id<Font<'static>>,
    glyph_id: GlyphId,
    subpixel_offset: (u32, u32),
    size: u32,
}

impl GlyphKey {
    fn new(font_id: Id<Font<'static>>, glyph: &PositionedGlyph<'_>) -> GlyphKey {
        GlyphKey {
            font_id,
            glyph_id: glyph.id(),
            subpixel_offset: (
                glyph.position().x.fract().to_bits(),
                glyph.position().y.fract().to_bits(),
            ),
            size: glyph.scale().y.to_bits(),
        }
    }
}

#[derive(Debug)]
struct TextRenderer {
    vertex_buffer: VertexBuffer<Vertex>,
    instance_buffer: Option<VertexBuffer<GlyphInstance>>,
    program: Program,
    atlas: TextureAtlas<GlyphKey>,
    glyph_instances: Vec<GlyphInstance>,
    glyph_keys: Vec<GlyphKey>,
    batch_ranges: Vec<Range<u32>>,
}

impl TextRenderer {
    fn new(ctx: &InitializationCtx<'_>) -> Result<TextRenderer> {
        Ok(TextRenderer {
            vertex_buffer: VertexBuffer::immutable(ctx.display, &Vertex::RECT)
                .context("Cannot create vertex buffer")?,
            instance_buffer: None,
            program: ctx.compile_program("text", "text", Default::default())?,
            atlas: TextureAtlas::new(ctx.display, ClientFormat::U8)?,
            glyph_instances: Vec::new(),
            glyph_keys: Vec::new(),
            batch_ranges: Vec::new(),
        })
    }

    fn add_text(&mut self, ctx: &RenderingCtx<'_>, text: &Text, view_proj: &Matrix3<f32>) {
        use rusttype::{Point, Scale};

        let assets = ctx.resources.get::<Assets>().unwrap();

        let dpi = ctx.dpi_factor;
        let font = assets.get_by_id(text.font_id).unwrap();
        let pos = Point {
            x: text.origin.x * dpi,
            y: text.origin.y * dpi,
        };

        let glyphs = font.layout(&text.text, Scale::uniform(text.size * dpi), pos);
        for glyph in glyphs {
            let bb = match glyph.pixel_bounding_box() {
                Some(v) => v,
                None => continue,
            };

            let key = GlyphKey::new(text.font_id, &glyph);
            let instance = GlyphInstance::new(bb, text.color, dpi, &view_proj);
            self.glyph_instances.push(instance);
            self.glyph_keys.push(key);

            self.atlas
                .add(key, (bb.width() as u32, bb.height() as u32), |data| {
                    glyph.draw(|x, y, coverage| {
                        let value = (coverage * 255.0) as u8;
                        let idx = (y as usize) * (bb.width() as usize) + (x as usize);
                        data[idx] = value;
                    });
                });
        }
    }

    fn end_batch(&mut self) -> usize {
        let start = self.batch_ranges.last().map(|last| last.end).unwrap_or(0);
        let end = self.glyph_instances.len() as u32;
        self.batch_ranges.push(start..end);
        self.batch_ranges.len() - 1
    }

    fn upload(&mut self, ctx: &mut RenderingCtx<'_>) -> Result<()> {
        self.atlas.deallocate_dead();
        self.atlas.flush(ctx.display)?;

        if self.glyph_instances.is_empty() {
            return Ok(());
        }

        for (glyph, key) in self.glyph_instances.iter_mut().zip(&self.glyph_keys) {
            let rect = self.atlas.get_rect_f32(key).expect("no glyph in atlas");
            glyph.in_uv_rect = rect;
        }

        ctx.stats.tris += self.glyph_instances.len() * 2;
        self.instance_buffer = Some(VertexBuffer::immutable(ctx.display, &self.glyph_instances)?);

        Ok(())
    }

    fn draw(
        &mut self,
        ctx: &mut RenderingCtx<'_>,
        surface: &mut impl Surface,
        batch_id: usize,
        scissor: Rect,
    ) -> Result<()> {
        let range = &self.batch_ranges[batch_id];
        if range.start == range.end {
            return Ok(());
        }

        let uniforms = glium::uniform! {
            u_atlas: self.atlas.texture.sampled()
        };

        let params = DrawParameters {
            blend: Blend::alpha_blending(),
            scissor: Some(scissor),
            ..Default::default()
        };

        let ibuf = self.instance_buffer.as_ref().unwrap();
        let slice = ibuf
            .slice(range.start as _..range.end as _)
            .ok_or_else(|| anyhow!("Draw out of range"))?;
        let slice = slice
            .per_instance()
            .map_err(|_| anyhow!("Instancing is not supported"))?;

        ctx.stats.drawcalls += 1;
        surface.draw(
            (&self.vertex_buffer, slice),
            NoIndices(PrimitiveType::TrianglesList),
            &self.program,
            &uniforms,
            &params,
        )?;

        Ok(())
    }

    fn reset(&mut self) {
        self.glyph_instances.clear();
        self.batch_ranges.clear();
        self.glyph_keys.clear();
    }
}

fn build_proj_matrix(res: LogicalSize<f32>) -> Matrix3<f32> {
    Matrix3::new_nonuniform_scaling(&Vector2::new(2.0 / res.width, -2.0 / res.height))
        .append_translation(&Vector2::new(-1.0, 1.0))
}

fn evaluate_transforms(
    commands: &[DrawCommand2D],
    proj: Matrix3<f32>,
) -> impl Iterator<Item = (&'_ DrawCommand2D, Matrix3<f32>, Matrix3<f32>)> + Clone + '_ {
    let mut stack = Vec::new();
    let mut view = Matrix3::identity();
    let mut view_proj = proj;
    commands.iter().flat_map(move |command| match command {
        DrawCommand2D::PushMatrix => {
            stack.push(view);
            None
        }
        DrawCommand2D::Transform(mat) => {
            view = mat * view;
            view_proj = proj * view;
            None
        }
        DrawCommand2D::PopMatrix => {
            view = stack.pop().expect("nothing to pop");
            view_proj = proj * view;
            None
        }
        _ => Some((command, view_proj, view)),
    })
}

fn build_scissor(ctx: &RenderingCtx<'_>, scissor: &Aabr<f32>) -> Rect {
    let scale = ctx.dpi_factor;
    let height = ctx.logical_resolution.height;
    let size = scissor.extent();
    Rect {
        left: (scissor.min.x * scale) as u32,
        bottom: ((height - scissor.min.y - size.y) * scale) as u32,
        width: (size.x * scale) as u32,
        height: (size.y * scale) as u32,
    }
}

#[derive(Debug)]
enum DrawCall {
    Rect(usize, Rect),
    Text(usize, Rect),
}

/// 2D renderer
#[derive(Debug)]
pub struct Renderer2D {
    rectangles: RectangleRenderer,
    text: TextRenderer,
    not_flushed_text: AabrTree,
    drawcalls: Vec<DrawCall>,
}

impl Renderer2D {
    /// Create a new 2D renderer
    pub fn new(ctx: &InitializationCtx<'_>) -> Result<Renderer2D> {
        Ok(Renderer2D {
            rectangles: RectangleRenderer::new(ctx)
                .context("Cannot initialize 2D rectangle renderer")?,
            text: TextRenderer::new(ctx).context("Cannot initialize 2D text renderer")?,
            not_flushed_text: AabrTree::new(),
            drawcalls: Vec::new(),
        })
    }

    fn end_rect_batch(&mut self, scissor: Rect) {
        self.drawcalls
            .push(DrawCall::Rect(self.rectangles.end_batch(), scissor));
    }

    fn end_text_batch(&mut self, scissor: Rect) {
        self.drawcalls
            .push(DrawCall::Text(self.text.end_batch(), scissor));
        self.not_flushed_text.clear();
    }

    fn end_all_batches(&mut self, scissor: Rect) {
        self.end_rect_batch(scissor);
        self.end_text_batch(scissor);
    }

    /// Render the specified `DrawList2D`
    pub fn render(
        &mut self,
        ctx: &mut RenderingCtx<'_>,
        surface: &mut impl Surface,
        list: &DrawList2D,
    ) -> Result<()> {
        let time_guard = Stats::get().time_guard("render2d-upload");

        let assets = ctx.resources.get::<Assets>().unwrap();

        let rect = Aabr {
            min: Point2::new(0.0, 0.0),
            max: Point2::new(ctx.logical_resolution.width, ctx.logical_resolution.height),
        };
        let fullscreen_scissor = build_scissor(ctx, &rect);
        let mut scissor = fullscreen_scissor;

        let proj = build_proj_matrix(ctx.logical_resolution);
        let transformed = evaluate_transforms(&list.commands, proj);

        for (cmd, view_proj, view) in transformed {
            match *cmd {
                DrawCommand2D::Rectangle(rect_id) => {
                    let rect = &list.rectangles[rect_id];
                    let aabr = rect.aabr().transform(&view);
                    if self.not_flushed_text.any_overlap(aabr) {
                        self.end_all_batches(scissor);
                    }

                    self.rectangles.add_rect(ctx, rect, &view_proj);
                }
                DrawCommand2D::Text(text_id) => {
                    let text = &list.texts[text_id];
                    let aabr = text.aabr(&assets).map(|v| v.transform(&view));
                    if let Some(aabr) = aabr {
                        self.not_flushed_text.insert(aabr);
                        self.text.add_text(ctx, text, &view_proj);
                    }
                }
                DrawCommand2D::SetScissor(ref rect) => {
                    self.end_all_batches(scissor);
                    scissor = build_scissor(ctx, rect);
                }
                DrawCommand2D::ClearScissor => {
                    self.end_all_batches(scissor);
                    scissor = fullscreen_scissor;
                }
                _ => {}
            }
        }

        drop(assets);

        self.end_all_batches(scissor);

        self.rectangles.upload(ctx)?;
        self.text.upload(ctx)?;

        drop(time_guard);

        let time_guard = Stats::get().time_guard("render2d-draw");

        for call in self.drawcalls.drain(..) {
            match call {
                DrawCall::Rect(batch_id, s) => self.rectangles.draw(ctx, surface, batch_id, s)?,
                DrawCall::Text(batch_id, s) => self.text.draw(ctx, surface, batch_id, s)?,
            }
        }

        drop(time_guard);

        self.rectangles.reset();
        self.text.reset();

        Ok(())
    }
}
