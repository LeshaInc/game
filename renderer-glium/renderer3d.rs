use std::collections::HashMap;

use anyhow::{anyhow, Result};
use glium::framebuffer::SimpleFrameBuffer;
use glium::texture::{DepthFormat, DepthTexture2d, MipmapsOption};
use glium::uniforms::{SamplerWrapFunction, Uniforms};
use glium::{
    BackfaceCullingMode, Depth, DepthTest, Display, DrawParameters, Program, Surface, VertexBuffer,
};
use nalgebra::{Unit, Vector3};

use super::{InitializationCtx, RenderingCtx};
use game_core::graphics::drawlist3d::MeshTransform;
use game_core::graphics::{DrawList3D, GraphicsSettings};
use game_core::util::Stats;
use game_core::Camera;

/// Component for objects which cast shadow
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub struct ShadowCaster;

#[derive(Debug, Clone, Copy, glium_derive::Vertex)]
struct MeshInstance {
    in_model_row0: [f32; 4],
    in_model_row1: [f32; 4],
    in_model_row2: [f32; 4],
    in_rot: [f32; 4],
    in_uv_rect: [f32; 4],
}

impl MeshInstance {
    fn new(model: &MeshTransform) -> MeshInstance {
        MeshInstance {
            in_model_row0: model.matrix.row(0).transpose().into(),
            in_model_row1: model.matrix.row(1).transpose().into(),
            in_model_row2: model.matrix.row(2).transpose().into(),
            in_rot: model.rotation.into_inner().coords.into(),
            in_uv_rect: [0.0; 4],
        }
    }
}

#[derive(Debug)]
struct ShadowStuff {
    depthmap: DepthTexture2d,
    program: Program,
}

fn new_depthmap(display: &Display, res: u32) -> Result<DepthTexture2d> {
    let depthmap = DepthTexture2d::empty_with_format(
        display,
        DepthFormat::I24,
        MipmapsOption::NoMipmap,
        res,
        res,
    )?;

    Ok(depthmap)
}

/// 3D renderer
#[derive(Debug)]
pub struct Renderer3D {
    program: Program,
    textured_program: Program,
    shadow: Option<ShadowStuff>,
}

impl Renderer3D {
    /// Create a new 3D renderer
    pub fn new(ctx: &InitializationCtx<'_>) -> Result<Renderer3D> {
        let mut defines = HashMap::new();

        let shadow = if ctx.settings.shadows {
            let depthmap = new_depthmap(ctx.display, ctx.settings.shadow_resolution)?;

            Some(ShadowStuff {
                program: ctx.compile_program("shadows", "shadows", Default::default())?,
                depthmap,
            })
        } else {
            None
        };

        if ctx.settings.shadows {
            defines.insert("SHADOW", "");
        }

        let program = ctx.compile_program("diffuse", "diffuse", defines.clone())?;
        defines.insert("TEXTURED", "");
        let textured_program = ctx.compile_program("diffuse", "diffuse", defines)?;

        Ok(Renderer3D {
            program,
            textured_program,
            shadow,
        })
    }

    /// Update graphics settings
    pub fn update_settings(
        &mut self,
        ctx: &InitializationCtx<'_>,
        old: &GraphicsSettings,
        new: &GraphicsSettings,
    ) -> Result<()> {
        if new.shadows {
            if !old.shadows || old.shadow_resolution != new.shadow_resolution {
                let program = self.shadow.take().map(|v| v.program);
                let program = match program {
                    Some(v) => v,
                    None => ctx.compile_program("shadows", "shadows", Default::default())?,
                };
                let depthmap = new_depthmap(ctx.display, new.shadow_resolution)?;
                self.shadow = Some(ShadowStuff { program, depthmap });
            }

            if !old.shadows {
                let mut defines = HashMap::new();
                defines.insert("SHADOW", "");
                self.program = ctx.compile_program("diffuse", "diffuse", defines.clone())?;
                defines.insert("TEXTURED", "");
                self.textured_program = ctx.compile_program("diffuse", "diffuse", defines)?;
            }
        } else if old.shadows {
            self.shadow = None;
            let mut defines = HashMap::new();
            self.program = ctx.compile_program("diffuse", "diffuse", defines.clone())?;
            defines.insert("TEXTURED", "");
            self.textured_program = ctx.compile_program("diffuse", "diffuse", defines)?;
        }

        Ok(())
    }

    fn upload_list(
        ctx: &mut RenderingCtx<'_>,
        list: &DrawList3D,
    ) -> Result<VertexBuffer<MeshInstance>> {
        let mut instances = Vec::with_capacity(list.len());

        instances.extend(list.iter().map(|(_, trans, tex)| {
            let mut instance = MeshInstance::new(trans);
            if let Some(rect) = tex.and_then(|t| ctx.storage.atlas.get_rect_f32(&t)) {
                instance.in_uv_rect = rect;
            }
            instance
        }));

        Ok(VertexBuffer::immutable(ctx.display, &instances)?)
    }

    fn draw(
        ctx: &mut RenderingCtx<'_>,
        textured_program: &Program,
        plain_program: &Program,
        surface: &mut impl Surface,
        list: &DrawList3D,
        uniforms: impl Uniforms,
        params: &DrawParameters<'_>,
    ) -> Result<()> {
        let buf = Self::upload_list(ctx, list)?;

        let mut objects = list.iter().peekable();

        let mut offset = 0;
        let mut batch_len = 0;
        while let Some((mesh_id, _, texture_id)) = objects.next() {
            batch_len += 1;

            let next = objects.peek().copied();
            if let Some((next_mesh, _, next_tex)) = next {
                if mesh_id == next_mesh && texture_id.is_some() == next_tex.is_some() {
                    continue;
                }
            }

            let (vbuf, ibuf) = &ctx.storage.meshes[&mesh_id];
            ctx.stats.tris += ibuf.len() / 3 * batch_len;

            let program = if texture_id.is_some() {
                &textured_program
            } else {
                &plain_program
            };

            let slice = buf.slice(offset..offset + batch_len).unwrap();
            offset += batch_len;
            batch_len = 0;

            let instances = slice
                .per_instance()
                .map_err(|_| anyhow!("Instancing is not supported"))?;

            ctx.stats.drawcalls += 1;

            surface.draw((vbuf, instances), ibuf, program, &uniforms, &params)?;
        }

        Ok(())
    }

    /// Render the specified world
    pub fn render<S: Surface>(
        &mut self,
        ctx: &mut RenderingCtx<'_>,
        surface: &mut S,
        objects: DrawList3D,
        casters: DrawList3D,
    ) -> Result<()> {
        let _time_guard = Stats::get().time_guard("render3d");

        let mut camera = ctx.resources.get_mut::<Camera>().unwrap();
        let light_dir = Unit::new_normalize(Vector3::new(1.0, 1.0, -2.0));
        let (shadow_matrix, _planes) = camera.shadow_matrix(light_dir, 4.0);
        camera.fit_near_far(4.0);
        camera.aspect = ctx.logical_resolution.width / ctx.logical_resolution.height;
        camera.update_projection();

        let proj = camera.projection.to_homogeneous();
        let view = camera.view.to_homogeneous();

        drop(camera);

        if let Some(shadow) = &self.shadow {
            let uniforms = glium::uniform! {
                u_view_projection: *shadow_matrix.as_ref()
            };

            let params = DrawParameters {
                depth: Depth {
                    test: DepthTest::IfLess,
                    write: true,
                    ..Default::default()
                },
                ..Default::default()
            };

            let mut shadow_surface = SimpleFrameBuffer::depth_only(ctx.display, &shadow.depthmap)?;

            shadow_surface.clear_depth(1.0);

            Self::draw(
                ctx,
                &shadow.program,
                &shadow.program,
                &mut shadow_surface,
                &casters,
                uniforms,
                &params,
            )?;
        }

        let params = DrawParameters {
            depth: Depth {
                test: DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            backface_culling: BackfaceCullingMode::CullClockwise,
            ..Default::default()
        };

        let uniforms = glium::uniform! {
            u_view_projection: *(proj * view).as_ref(),
            u_texture: ctx.storage.atlas.texture.sampled()
        };

        if let Some(shadow) = &self.shadow {
            let wrap = SamplerWrapFunction::Clamp;
            let shadowmap_size = [
                shadow.depthmap.dimensions().0 as f32,
                shadow.depthmap.dimensions().1 as f32,
            ];
            let uniforms = uniforms
                .add("u_shadow_matrix", *shadow_matrix.as_ref())
                .add("u_shadowmap", shadow.depthmap.sampled().wrap_function(wrap))
                .add("u_shadowmap_size", shadowmap_size);

            Self::draw(
                ctx,
                &self.textured_program,
                &self.program,
                surface,
                &objects,
                uniforms,
                &params,
            )?;
        } else {
            Self::draw(
                ctx,
                &self.textured_program,
                &self.program,
                surface,
                &objects,
                uniforms,
                &params,
            )?;
        }

        Ok(())
    }
}
