//! Utilities for working with shaders

use std::collections::HashMap;
use std::fs;
use std::path::Path;

use anyhow::{Context, Result};
use glium::{Display, Program, Version};

fn prepend_version(source: &mut String, ver: Version) {
    source.insert_str(0, &format!("#version {}{}0\n", ver.1, ver.2));
}

fn prepend_defines(source: &mut String, defines: &HashMap<&str, &str>) {
    for (k, v) in defines.iter() {
        source.insert_str(0, &format!("#define {} {}\n", k, v));
    }
}

/// Compile shader program located in assets/shaders/{vert, frag}
pub fn compile_program(
    display: &Display,
    assets_path: &Path,
    vert: &str,
    frag: &str,
    mut defines: HashMap<&str, &str>,
) -> Result<Program> {
    let version = display.get_supported_glsl_version();

    if version.1 == 1 && version.2 < 3 {
        defines.insert("LEGACY_GLSL", "");
        defines.insert("ATTR", "attribute");
        defines.insert("IN", "varying");
        defines.insert("OUT", "varying");
    } else {
        defines.insert("ATTR", "in");
        defines.insert("IN", "in");
        defines.insert("OUT", "out");
    }

    let mut path = assets_path.to_path_buf();
    path.push("shaders");

    path.push(vert);
    if path.extension().is_none() {
        path.set_extension("vert");
    }

    let mut vert_source = fs::read_to_string(&path)
        .with_context(|| format!("Cannot read shader {}", path.display()))?;
    prepend_defines(&mut vert_source, &defines);
    prepend_version(&mut vert_source, version);

    path.pop();
    path.push(frag);
    if path.extension().is_none() {
        path.set_extension("frag");
    }

    let mut frag_source = fs::read_to_string(&path)
        .with_context(|| format!("Cannot read shader {}", path.display()))?;
    prepend_defines(&mut frag_source, &defines);
    prepend_version(&mut frag_source, version);

    let program = Program::from_source(display, &vert_source, &frag_source, None)
        .with_context(|| format!("Error while compiling shader program ({}, {})", vert, frag))?;

    Ok(program)
}
