//! 2D and 3D rendering

mod renderer2d;
mod renderer3d;

pub mod atlas;
pub mod resources;
pub mod shaders;

pub use self::renderer2d::Renderer2D;
pub use self::renderer3d::{Renderer3D, ShadowCaster};

use std::cell::Ref;
use std::collections::HashMap;
use std::ops::Deref;
use std::path::{Path, PathBuf};

use anyhow::{bail, Context, Result};
use glium::framebuffer::SimpleFrameBuffer;
use glium::glutin::dpi::{LogicalSize, PhysicalSize};
use glium::glutin::event_loop::EventLoop;
use glium::glutin::window::{Window, WindowBuilder};
use glium::glutin::{ContextBuilder, GlProfile, PossiblyCurrent, WindowedContext};
use glium::texture::{
    DepthFormat, DepthTexture2dMultisample, MipmapsOption, SrgbFormat, SrgbTexture2dMultisample,
};
use glium::uniforms::MagnifySamplerFilter;
use glium::{Api, Display, Program, Surface, Version};
use legion::systems::CommandBuffer;
use legion::{Entity, IntoQuery, Resources, World, Write};
use palette::{Pixel as _, Srgb, Srgba};
use takeable_option::Takeable;

use self::resources::GlStorage;
use game_core::assets::Assets;
use game_core::graphics::{
    CasterDrawList3D, DrawList2D, DrawList3D, GraphicsBackend, GraphicsSettings, WindowRef,
};
use game_core::util::Stats;

/// Convert sRGBA to linear RGBA as an array suitable for shaders
pub fn to_rgba(srgba: Srgba) -> [f32; 4] {
    srgba.into_linear().into_format().into_raw()
}

/// Convert sRGB to linear RGB as an array suitable for shaders
pub fn to_rgb(srgba: Srgb) -> [f32; 3] {
    srgba.into_linear().into_format().into_raw()
}

/// Context passed to renderers during initialization
#[derive(Clone, Copy)]
pub struct InitializationCtx<'a> {
    /// OpenGL display
    pub display: &'a Display,
    /// Path to `assets/` directory
    pub assets_path: &'a Path,
    /// Version of shaders to load
    pub glsl_version: Version,
    /// Graphics settings
    pub settings: &'a GraphicsSettings,
}

impl InitializationCtx<'_> {
    /// Compile a shader program using appropriate GLSL version
    pub fn compile_program(
        &self,
        frag: &str,
        vert: &str,
        defines: HashMap<&str, &str>,
    ) -> Result<Program> {
        self::shaders::compile_program(self.display, self.assets_path, frag, vert, defines)
    }
}

/// Rendering stats
#[derive(Default)]
pub struct RenderingStats {
    /// Number of triangles
    pub tris: usize,
    /// Number of drawcalls
    pub drawcalls: u32,
}

/// Context passed to renderers during frame rendering
pub struct RenderingCtx<'a> {
    /// OpenGL display
    pub display: &'a Display,
    /// Physical resolution of the window
    pub physical_resolution: PhysicalSize<u32>,
    /// Logical resolution of the window with applied DPI factor
    pub logical_resolution: LogicalSize<f32>,
    /// DPI scaling factor (`logical * dpi = physical`)
    pub dpi_factor: f32,
    /// Resources
    pub resources: &'a mut Resources,
    /// Storage
    pub storage: &'a GlStorage,
    /// Statistics
    pub stats: &'a mut RenderingStats,
}

/// The rendering engine
pub struct Graphics {
    display: Display,
    storage: GlStorage,
    settings: GraphicsSettings,
    max_settings: GraphicsSettings,

    glsl_version: Version,
    assets_path: PathBuf,

    renderer2d: Renderer2D,
    renderer3d: Renderer3D,

    msaa_tex: Option<(SrgbTexture2dMultisample, DepthTexture2dMultisample)>,
}

fn create_msaa_tex(
    display: &Display,
    dims: (u32, u32),
    samples: u32,
) -> Result<(SrgbTexture2dMultisample, DepthTexture2dMultisample)> {
    let color = SrgbTexture2dMultisample::empty_with_format(
        display,
        SrgbFormat::U8U8U8U8,
        MipmapsOption::NoMipmap,
        dims.0,
        dims.1,
        samples,
    )?;

    let depth = DepthTexture2dMultisample::empty_with_format(
        display,
        DepthFormat::I24,
        MipmapsOption::NoMipmap,
        dims.0,
        dims.1,
        samples,
    )?;

    Ok((color, depth))
}

impl Graphics {
    /// Initialize the rendering engine
    pub fn new(
        event_loop: &EventLoop<()>,
        assets_path: &Path,
        settings: GraphicsSettings,
    ) -> Result<Graphics> {
        let window_builder = WindowBuilder::new();
        let context_builder = ContextBuilder::new()
            .with_vsync(true)
            .with_gl_profile(GlProfile::Core);
        let display = Display::new(window_builder, context_builder, &event_loop)
            .context("Unable to initialize display")?;

        let gl_version = display.get_opengl_version();
        let glsl_version = display.get_supported_glsl_version();
        if glsl_version < Version(Api::Gl, 1, 2) {
            bail!("OpenGL {}.{} is not supported", gl_version.1, gl_version.2);
        }

        let ctx = InitializationCtx {
            display: &display,
            assets_path: &assets_path,
            glsl_version,
            settings: &settings,
        };

        let renderer2d = Renderer2D::new(&ctx).context("Cannot initialize 2D renderer")?;
        let renderer3d = Renderer3D::new(&ctx).context("Cannot initialize 3D renderer")?;

        Ok(Graphics {
            storage: GlStorage::new(&display)?,
            settings,
            max_settings: query_max_settings(&display),
            display,
            glsl_version,
            assets_path: assets_path.into(),
            renderer2d,
            renderer3d,
            msaa_tex: None,
        })
    }
}

impl GraphicsBackend for Graphics {
    fn window(&self) -> WindowRef<'_> {
        WindowRef::Dyn(Box::new(WindowRefInner {
            inner: self.display.gl_window(),
        }))
    }

    fn update_settings(&mut self, new: GraphicsSettings) -> Result<()> {
        let ctx = InitializationCtx {
            display: &self.display,
            assets_path: &self.assets_path,
            glsl_version: self.glsl_version,
            settings: &new,
        };

        self.renderer3d
            .update_settings(&ctx, &self.settings, &new)?;
        self.settings = new;

        Ok(())
    }

    fn max_settings(&self) -> &GraphicsSettings {
        &self.max_settings
    }

    fn render(
        &mut self,
        world: &mut World,
        resources: &mut Resources,
        cbuf: &mut CommandBuffer,
    ) -> Result<()> {
        let time_guard = Stats::get().time_guard("render");

        let mut assets = resources.get_mut::<Assets>().unwrap();
        self.storage.maintain(&self.display, &mut assets)?;
        drop(assets);

        let window = self.display.gl_window();
        let window = window.window();
        let physical_resolution = window.inner_size();
        let logical_resolution = physical_resolution.to_logical(window.scale_factor());

        let mut frame = self.display.draw();

        if self.settings.msaa > 1
            && (self.msaa_tex.is_none()
                || self.msaa_tex.as_ref().unwrap().0.dimensions() != frame.get_dimensions()
                || self.msaa_tex.as_ref().unwrap().0.samples() != u32::from(self.settings.msaa))
        {
            self.msaa_tex = Some(create_msaa_tex(
                &self.display,
                frame.get_dimensions(),
                self.settings.msaa as _,
            )?);
        } else if self.settings.msaa == 1 {
            self.msaa_tex = None;
        };

        let mut ctx = RenderingCtx {
            display: &self.display,
            physical_resolution,
            logical_resolution,
            dpi_factor: window.scale_factor() as f32,
            resources,
            storage: &self.storage,
            stats: &mut RenderingStats::default(),
        };

        let mut drawlist3d = DrawList3D::default();
        let mut query = <(Entity, Write<DrawList3D>)>::query();
        for (&entity, drawlist) in query.iter_mut(world) {
            drawlist3d.append(drawlist);
            cbuf.remove(entity);
        }

        let mut casters_drawlist3d = DrawList3D::default();
        let mut query = <(Entity, Write<CasterDrawList3D>)>::query();
        for (&entity, drawlist) in query.iter_mut(world) {
            casters_drawlist3d.append(&mut drawlist.0);
            cbuf.remove(entity);
        }

        let mut drawlist2d = DrawList2D::default();
        let mut query = <(Entity, Write<DrawList2D>)>::query();
        for (&entity, drawlist) in query.iter_mut(world) {
            drawlist2d.append(drawlist);
            cbuf.remove(entity);
        }

        if let Some((col, depth)) = self.msaa_tex.as_ref() {
            let mut fbuf = SimpleFrameBuffer::with_depth_buffer(ctx.display, col, depth)?;
            fbuf.clear_color_and_depth((1.0, 1.0, 1.0, 1.0), 1.0);
            self.renderer3d
                .render(&mut ctx, &mut fbuf, drawlist3d, casters_drawlist3d)
                .context("3D rendering error")?;
            fbuf.fill(&frame, MagnifySamplerFilter::Nearest);
        } else {
            frame.clear_color_and_depth((1.0, 1.0, 1.0, 1.0), 1.0);
            self.renderer3d
                .render(&mut ctx, &mut frame, drawlist3d, casters_drawlist3d)
                .context("3D rendering error")?;
        }

        self.renderer2d
            .render(&mut ctx, &mut frame, &drawlist2d)
            .context("2D rendering error")?;

        Stats::get().set_count("drawcalls", ctx.stats.drawcalls);
        Stats::get().set_count("triangles", ctx.stats.tris as _);

        drop(time_guard);

        frame.finish()?;

        Ok(())
    }
}

/// Window reference
pub struct WindowRefInner<'a> {
    inner: Ref<'a, Takeable<WindowedContext<PossiblyCurrent>>>,
}

impl Deref for WindowRefInner<'_> {
    type Target = Window;

    fn deref(&self) -> &Window {
        &self.inner.window()
    }
}

// FIXME: this function is cursed
fn max_msaa_samples(display: &Display) -> u8 {
    use glium::texture::{Texture2dMultisample, UncompressedFloatFormat};
    use std::panic::{catch_unwind, set_hook, take_hook};
    use std::sync::Mutex;

    let mtx = Mutex::new(display);
    let mut max_samples = 1u8;

    set_hook(Box::new(|_| {}));

    loop {
        let samples = max_samples * 2;
        let res = catch_unwind(|| {
            let display = mtx.lock().unwrap();
            let facade = &**display;
            let samples = samples as u32;

            let depth = DepthTexture2dMultisample::empty_with_format(
                facade,
                DepthFormat::I24,
                MipmapsOption::NoMipmap,
                16,
                16,
                samples,
            )
            .unwrap();

            let color = Texture2dMultisample::empty_with_format(
                facade,
                UncompressedFloatFormat::U8U8U8U8,
                MipmapsOption::NoMipmap,
                16,
                16,
                samples,
            )
            .unwrap();

            assert!(color.samples() == samples);
            assert!(depth.samples() == samples);

            let mut fbuf = SimpleFrameBuffer::with_depth_buffer(facade, &color, &depth).unwrap();
            fbuf.clear_color(0.0, 0.0, 0.0, 0.0);
        });

        match res.ok().and_then(|_| max_samples.checked_mul(2)) {
            Some(v) => max_samples = v,
            _ => break,
        }
    }

    let _ = take_hook();

    max_samples
}

fn query_max_settings(display: &Display) -> GraphicsSettings {
    GraphicsSettings {
        msaa: max_msaa_samples(display).min(16),
        shadows: true,
        shadow_resolution: 2048, // TODO
    }
}
