//! Graphics resources

use anyhow::{Context, Result};
use fxhash::FxHashMap;
use glium::index::PrimitiveType;
use glium::texture::ClientFormat;
use glium::{Display, IndexBuffer, VertexBuffer};

use super::atlas::TextureAtlas;
use game_core::assets::{Assets, Id};
use game_core::graphics::{Mesh, Texture};

#[derive(Clone, Copy, Debug, glium_derive::Vertex)]
#[repr(C)]
pub struct MeshVertex {
    #[glium(attr = "in_pos")]
    pos: [f32; 3],
    #[glium(attr = "in_normal")]
    norm: [f32; 3],
    #[glium(attr = "in_tex")]
    tex: [f32; 2],
}

/// Resource storage
#[derive(Debug)]
pub struct GlStorage {
    /// Meshes
    pub meshes: FxHashMap<Id<Mesh>, (VertexBuffer<MeshVertex>, IndexBuffer<u32>)>,
    /// Texture atlas
    pub atlas: TextureAtlas<Id<Texture>>,
}

impl GlStorage {
    /// Create an empty `GlStorage`
    pub fn new(display: &Display) -> Result<GlStorage> {
        Ok(GlStorage {
            meshes: FxHashMap::default(),
            atlas: TextureAtlas::new(display, ClientFormat::U8U8U8U8)?,
        })
    }

    /// Upload data on GPU. Perform cleanup
    pub fn maintain(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        self.maintain_meshes(display, assets)?;
        self.maintain_textures(display, assets)?;
        Ok(())
    }

    fn maintain_meshes(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        self.cleanup_meshes(display, assets)?;
        self.upload_meshes(display, assets)
    }

    fn upload_meshes(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        for (handle, mesh) in assets.iter_mut::<Mesh>() {
            let data = match mesh.data.take() {
                Some(v) => v,
                None => continue,
            };

            let vertices: &[MeshVertex] = unsafe {
                std::slice::from_raw_parts(data.vertices.as_ptr() as *const _, data.vertices.len())
            };

            let vbuf = VertexBuffer::immutable(display, vertices)
                .context("Cannot create vertex buffer")?;
            let ibuf = IndexBuffer::immutable(display, PrimitiveType::TrianglesList, &data.indices)
                .context("Cannot create index buffer")?;
            self.meshes.insert(handle.id(), (vbuf, ibuf));
        }

        Ok(())
    }

    fn cleanup_meshes(&mut self, _: &Display, assets: &mut Assets) -> Result<()> {
        assets.cleanup_with(|id, _, _| {
            self.meshes.remove(&id);
        });

        Ok(())
    }

    fn maintain_textures(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        self.cleanup_textures(display, assets)?;
        self.upload_textures(display, assets)
    }

    fn upload_textures(&mut self, display: &Display, assets: &mut Assets) -> Result<()> {
        for (handle, texture) in assets.iter_mut::<Texture>() {
            let data = match texture.data.take() {
                Some(v) => v,
                None => continue,
            };

            let rgba = data.flipv().into_rgba();
            let (w, h) = rgba.dimensions();
            let samples = rgba.as_flat_samples();
            let bytes = samples.as_slice();
            self.atlas.add_raw(handle.id(), (w, h), &bytes);
        }

        self.atlas.flush(display)?;

        Ok(())
    }

    fn cleanup_textures(&mut self, _: &Display, assets: &mut Assets) -> Result<()> {
        assets.cleanup_with(|id, _, _| {
            self.atlas.deallocate(&id);
        });

        Ok(())
    }
}
