#!/usr/bin/env bash

NAME=game

cmd() {
  echo "$" $@ 1>&2
  $@
}

cmd mkdir -p package

if [ -e target/release/$NAME ]; then
  cmd cp target/release/$NAME package/
elif [ -e target/debug/$NAME ]; then
  cmd cp target/debug/$NAME package/
elif [ -e target/debug/$NAME.exe ]; then
  cmd cp target/debug/$NAME.exe package/
elif [ -e target/release/$NAME.exe ]; then
  cmd cp target/release/$NAME.exe package/
elif [ -e target/x86_64-pc-windows-gnu/debug/$NAME.exe ]; then
  cmd cp target/x86_64-pc-windows-gnu/debug/$NAME.exe package/
elif [ -e target/x86_64-pc-windows-gnu/release/$NAME.exe ]; then
  cmd cp target/x86_64-pc-windows-gnu/release/$NAME.exe package/
elif [ -e target/i686-pc-windows-gnu/debug/$NAME.exe ]; then
  cmd cp target/i686-pc-windows-gnu/debug/$NAME.exe package/
elif [ -e target/i686-pc-windows-gnu/release/$NAME.exe ]; then
  cmd cp target/i686-pc-windows-gnu/release/$NAME.exe package/
else
  echo "ERROR: build artifacts not found" 1>&2
  exit 1
fi

cmd find assets -type d -exec mkdir -p package/{} \;

extensions=(vert frag glsl ttf mesh png ron)
for extension in "${extensions[@]}"; do
  cmd find assets -type f -name "*.$extension" -exec cp -f {} package/{} \;
done

cmd cp LICENSE package/
