use std::path::PathBuf;
use std::time::{Duration, Instant};

use anyhow::Result;
use legion::systems::CommandBuffer;
use legion::{Resources, Schedule, World};
use nalgebra::{Point2, Vector2};
use winit::dpi::LogicalPosition;
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};

use crate::graphics::{GraphicsBackend, GraphicsSettings};
use crate::input::Input;
use crate::util::FpsCounter;

/// Logical resolution of the window
pub struct WindowResolution(pub Vector2<f32>);

/// Maximum settings
pub struct MaxSettings(pub GraphicsSettings);

/// Path to assets
pub struct AssetsPath(pub PathBuf);

/// Time since last frame
pub struct DeltaTime(pub f32);

/// Cursor manipulation
#[derive(Clone, Copy, Debug)]
pub struct Cursor {
    /// Is cursor visible
    pub visible: bool,
    /// Is cursor grab enabled
    pub grab: bool,
    /// Set to `Some(pos)` if the position of cursor should be changed
    pub set_position: Option<Point2<f32>>,
}

impl Default for Cursor {
    fn default() -> Cursor {
        Cursor {
            visible: true,
            grab: false,
            set_position: None,
        }
    }
}

#[cfg(feature = "oldgl")]
type Graphics = game_renderer_glium::Graphics;
#[cfg(feature = "vulkan")]
type Graphics = game_renderer_gfx::Graphics<gfx_backend_vulkan::Backend>;
#[cfg(feature = "dx12")]
type Graphics = game_renderer_gfx::Graphics<gfx_backend_dx12::Backend>;
#[cfg(feature = "dx11")]
type Graphics = game_renderer_gfx::Graphics<gfx_backend_dx11::Backend>;
#[cfg(feature = "gl")]
type Graphics = game_renderer_gfx::Graphics<gfx_backend_gl::Backend>;

/// Start the main loop, running the `schedule` every frame before rendering
pub fn main_loop(mut world: World, mut resources: Resources, mut schedule: Schedule) -> Result<()> {
    let event_loop = EventLoop::new();

    let assets_path = resources.get::<AssetsPath>().unwrap();
    let settings = resources
        .get::<GraphicsSettings>()
        .as_deref()
        .cloned()
        .unwrap_or_default();

    let mut graphics = Graphics::new(&event_loop, &assets_path.0, settings)?;

    drop(assets_path);

    let mut cbuf = CommandBuffer::new(&world);
    let mut last_frame = Instant::now();

    resources.insert(FpsCounter::new(Duration::from_secs(2)));
    resources.insert(Cursor::default());
    resources.insert(MaxSettings(graphics.max_settings().clone()));

    let mut input = resources.get_mut_or_default::<Input>();
    input.set_dpi_factor(graphics.window().scale_factor());
    drop(input);

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } => {
            *control_flow = ControlFlow::Exit;
        }
        Event::WindowEvent { event, .. } => {
            let mut input = resources.get_mut_or_default::<Input>();
            input.handle_event(&event);
        }
        Event::MainEventsCleared => {
            let resolution = graphics.resolution();
            resources.insert(WindowResolution(resolution));
            resources.insert(DeltaTime(last_frame.elapsed().as_secs_f32()));

            schedule.execute(&mut world, &mut resources);

            let cursor_pos = if let Some(cursor) = resources.get::<Cursor>() {
                let window = graphics.window();
                window.set_cursor_visible(cursor.visible);
                let _ = window.set_cursor_grab(cursor.grab);
                cursor.set_position
            } else {
                None
            };

            if let Some(pos) = cursor_pos {
                let window = graphics.window();
                let _ = window.set_cursor_position(LogicalPosition::new(pos.x, pos.y));
                let mut input = resources.get_mut_or_default::<Input>();
                input.set_mouse_pos(pos);
            }

            if let Some(settings) = resources.get::<GraphicsSettings>() {
                if let Err(e) = graphics.update_settings(settings.clone()) {
                    eprintln!("Cannot change settings: {}", e);
                    *control_flow = ControlFlow::Exit;
                    return;
                }

                let assets_path = resources.get::<AssetsPath>().unwrap();
                if let Err(e) = settings.save(assets_path.0.join("../settings.toml")) {
                    eprintln!("Cannot save settings: {}", e);
                }
            }

            last_frame = Instant::now();

            if let Err(e) = graphics.render(&mut world, &mut resources, &mut cbuf) {
                eprintln!("Rendering error: {}", e);
                *control_flow = ControlFlow::Exit;
                return;
            }

            cbuf.flush(&mut world);

            let mut input = resources.get_mut_or_default::<Input>();
            input.next_tick();
            drop(input);

            let mut fps_counter = resources.get_mut::<FpsCounter>().unwrap();
            fps_counter.next_frame();
        }
        _ => {}
    })
}
