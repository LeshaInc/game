use std::ops::{Deref, DerefMut};

use bumpalo::Bump;
use rusttype::Font;

use super::storage::WidgetStorage;
use super::{StyleChain, UiStorage, WidgetId};
use crate::assets::{Assets, Id};
use crate::graphics::Encoder2D;
use crate::input::Input;

/// UI context
pub struct UiContext<'a> {
    /// Bump allocator for short-lived data
    pub bump: &'a Bump,
    /// Encoder to write draw commands to
    pub encoder: &'a mut Encoder2D,
    /// Assets
    pub assets: &'a Assets,
    /// Default font (to be replaced with `Style`)
    pub font: Id<Font<'static>>,
    /// Id of the current widget
    pub id: WidgetId,
    /// Storage
    pub storage: &'a mut UiStorage,
    /// Input
    pub input: &'a Input,
    /// Time since last frame
    pub dt: f32,
    /// Style chain
    pub style: StyleChain<'a>,
}

impl UiContext<'_> {
    /// Get storage of the current widget
    pub fn storage(&mut self) -> WidgetStorage<'_> {
        self.storage.widget(&self.id)
    }
}

impl Deref for UiContext<'_> {
    type Target = Encoder2D;

    fn deref(&self) -> &Encoder2D {
        &self.encoder
    }
}

impl DerefMut for UiContext<'_> {
    fn deref_mut(&mut self) -> &mut Encoder2D {
        &mut self.encoder
    }
}
