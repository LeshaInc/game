use nalgebra::Vector2;

use super::Widget;
use crate::ui::UiContext;
use crate::util::Orientation;

/// Filler widget (aka spring)
pub struct Filler {
    stretch: f32,
    orientation: Orientation,
}

impl Filler {
    /// Create a new filler widget with specified stretch factor
    pub fn new(stretch: f32, orientation: Orientation) -> Filler {
        Filler {
            stretch,
            orientation,
        }
    }
}

impl Widget<'_> for Filler {
    fn stretch_factor(&self) -> f32 {
        self.stretch
    }

    fn size(&mut self, _: &UiContext<'_>, hint: Vector2<f32>) -> Vector2<f32> {
        match self.orientation {
            Orientation::Vertical => Vector2::new(1.0, hint.y),
            Orientation::Horizontal => Vector2::new(hint.x, 1.0),
        }
    }
}
