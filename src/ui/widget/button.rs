use nalgebra::Vector2;
use once_cell::sync::Lazy;
use palette::Hsva;
use rusttype::Scale;

use super::{Geometry, Widget};
use crate::graphics::drawlist2d::{Border, BoxShadow, CornerRadii};
use crate::input::MouseButton;
use crate::ui::{Style, UiContext};
use crate::util::{Animation, FontUtil};

static DEFAULT_STYLE: Lazy<Style> = lazy_style! {
    button {
        rounding: 4.0,
        background: Hsva::new(0.0, 0.0, 0.9, 1.0),
        foreground: Hsva::new(0.0, 0.0, 0.2, 1.0),
        border_color: Hsva::new(0.0, 0.0, 0.1, 1.0),
        shadow_color: Hsva::new(0.0, 0.0, 0.0, 0.5),
        border_thickness: 1.0,
        font_size: 20.0,
        shadow_radius: 5.0,
    };

    button_hover {
        background: Hsva::new(0.0, 0.0, 1.0, 1.0),
        shadow_color: Hsva::new(0.0, 0.0, 0.0, 0.7),
        border_thickness: 1.5,
        shadow_radius: 8.0,
    };
};

/// Button
pub struct Button<'a> {
    label: &'a str,
    id: &'a str,
    callback: Box<dyn FnMut() + 'a>, // TODO: use bump
    size: Vector2<f32>,
    offset: Vector2<f32>,
}

impl Button<'_> {
    /// Create a new button with the given label. By default, the entire label will be used as an
    /// ID, but you can override it by setting it to `some text##id`. In this case, only the part
    /// before `##` will be visible.
    pub fn new<'a, F>(uctx: &mut UiContext<'a>, label: &str, callback: F) -> Button<'a>
    where
        F: FnMut() + 'a,
    {
        let label = uctx.bump.alloc_str(label);
        let mut split = label.rsplitn(2, "##");
        let id = split.next().unwrap_or(label);
        let label = split.next().unwrap_or(label);

        uctx.style.deque.push_back(&DEFAULT_STYLE);

        let font_size = uctx.style.get("button", "font_size");

        uctx.style.deque.pop_back();

        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let v_metrics = font.v_metrics(Scale::uniform(font_size));

        let label_width = font.text_width(font_size, label);
        let label_height = v_metrics.ascent - v_metrics.descent;
        let button_width = label_width + font_size + 8.0;
        let button_height = label_height * 1.5 + 8.0;

        let callback = Box::new(callback);

        Button {
            label,
            id,
            callback,
            size: Vector2::new(button_width, button_height),
            offset: Vector2::new(
                (button_width - label_width) / 2.0,
                button_height / 2.0 - v_metrics.descent,
            ),
        }
    }
}

impl<'a> Widget<'a> for Button<'a> {
    fn id(&self) -> Option<&'a str> {
        Some(self.id)
    }

    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let rect_bounds = geometry.clamped_bounds.inflate(-4.0);
        let has_entered = uctx.input.has_mouse_entered(&rect_bounds);
        let has_left = uctx.input.has_mouse_left(&rect_bounds);

        let mut storage = uctx.storage.widget(&uctx.id);
        let animation = storage
            .entry::<Animation<f32>>()
            .or_insert_with(|| Animation::new(0.0, 8.0));

        if has_entered {
            animation.speed = 8.0;
            animation.go_to(1.0);
        }

        if has_left {
            animation.speed = 3.0;
            animation.go_to(0.0);
        }

        animation.update(uctx.dt);

        if rect_bounds.contains_point(&uctx.input.mouse_position())
            && uctx.input.has_mouse_released(MouseButton::Left)
        {
            (self.callback)();
        }
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let storage = uctx.storage.widget(&uctx.id);
        let time = storage
            .get::<Animation<f32>>()
            .map(|animation| animation.value)
            .unwrap_or(0.0);

        uctx.style.deque.push_back(&DEFAULT_STYLE);

        let style = &uctx.style;

        let foreground = style.get::<Hsva>("button", "foreground");
        let border_color = style.get::<Hsva>("button", "border_color");
        let rounding = style.get("button", "rounding");
        let font_size = style.get("button", "font_size");

        let (from, to) = ("button", "button_hover");
        let background = style.interpolate::<Hsva>(from, to, "background", time);
        let shadow_color = style.interpolate::<Hsva>(from, to, "shadow_color", time);
        let shadow_radius = style.interpolate::<f32>(from, to, "shadow_radius", time);
        let border_thickness = style.interpolate::<f32>(from, to, "border_thickness", time);

        let border = Border::new(border_color, border_thickness);
        let shadow = BoxShadow::new(shadow_color, shadow_radius).with_offset([0.0, 1.0]);

        uctx.style.deque.pop_back();

        let font = uctx.font;
        let pos = geometry.bounds.min;

        let rpos = pos + Vector2::new(4.0, 4.0);
        let size = self.size - Vector2::new(8.0, 8.0);
        uctx.rect(rpos, size, background)
            .with_corner_radii(CornerRadii::new_equal(rounding))
            .with_border(border)
            .with_shadow(shadow);

        let tpos = pos + self.offset;
        uctx.text(font, tpos, font_size, foreground, self.label);
    }

    fn size(&mut self, _: &UiContext<'a>, _: Vector2<f32>) -> Vector2<f32> {
        self.size
    }
}
