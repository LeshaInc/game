use nalgebra::Vector2;
use once_cell::sync::Lazy;
use palette::Hsva;

use super::{Geometry, Widget};
use crate::ui::{Style, UiContext};
use crate::util::FontUtil;

static DEFAULT_STYLE: Lazy<Style> = lazy_style! {
    text {
        foreground: Hsva::new(0.0, 0.0, 0.1, 1.0),
        font_size: 20.0,
        line_height: 1.1,
    };
};

/// Multiline text
pub struct Text {
    text: String,
    breaks: Vec<u32>,
    font_size: f32,
    line_height: f32,
}

impl Text {
    /// Create a new text
    pub fn new(uctx: &mut UiContext<'_>, text: impl Into<String>) -> Text {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let font_size = uctx.style.get("text", "font_size");
        let line_height = uctx.style.get("text", "line_height");
        uctx.style.deque.pop_back();

        Text {
            text: text.into(),
            breaks: Vec::new(),
            font_size,
            line_height,
        }
    }
}

impl<'a> Widget<'a> for Text {
    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let foreground = uctx.style.get::<Hsva>("text", "foreground");
        uctx.style.deque.pop_back();

        let font_id = uctx.font;
        let font = uctx.assets.get_by_id(font_id).unwrap();
        let v_metrics = font.v_metrics(rusttype::Scale::uniform(self.font_size));

        let line_height = self.font_size * self.line_height;

        let x = geometry.bounds.min.x;
        let mut y = geometry.bounds.min.y + line_height / 2.0 - v_metrics.descent;
        let mut last_break = 0;
        for &breeak in &self.breaks {
            let line = self.text[last_break as usize..breeak as usize].trim();
            uctx.text(font_id, [x, y], self.font_size, foreground, line);
            last_break = breeak;
            y += line_height;
        }
    }

    fn size(&mut self, uctx: &UiContext<'a>, hint: Vector2<f32>) -> Vector2<f32> {
        let scale = rusttype::Scale::uniform(self.font_size);
        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let space_width = font.glyph(' ').scaled(scale).h_metrics().advance_width;

        let mut size = Vector2::new(hint.x, 0.0);
        let mut x = 0.0;
        let mut word_start = 0;

        self.breaks.clear();

        let trimmed_len = self.text.trim_end_matches('\n').len();
        for (i, c) in self.text.char_indices() {
            let is_last = i + c.len_utf8() == trimmed_len;
            match (c, is_last) {
                ('\n', _) => {
                    self.breaks.push((i + 1) as u32);
                    size.x = size.x.max(x);
                    size.y += self.line_height * self.font_size;
                    x = 0.0;
                    word_start = i + 1;
                }
                (' ', false) | (_, true) => {
                    let end_idx = if c == ' ' { i } else { i + c.len_utf8() };
                    let word = &self.text[word_start..end_idx];
                    let word_width = font.text_width(self.font_size, word);
                    size.x = size.x.max(word_width);

                    let space = space_width * x.signum();
                    let addition = word_width + space;
                    x += addition;
                    if x > size.x {
                        self.breaks.push(word_start as u32);
                        size.y += self.line_height * self.font_size;
                        x = addition
                    }

                    word_start = i + 1;
                }
                _ => {}
            }

            if is_last {
                self.breaks.push((i + c.len_utf8()) as u32);
                size.y += self.line_height * self.font_size;
            }
        }

        size.x = size.x.max(x);

        size
    }

    fn stretch_factor(&self) -> f32 {
        1.0
    }
}
