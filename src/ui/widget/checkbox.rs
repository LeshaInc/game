use nalgebra::{UnitComplex, Vector2};
use once_cell::sync::Lazy;
use palette::Hsva;
use rusttype::Scale;

use super::{Geometry, Widget};
use crate::graphics::drawlist2d::Border;
use crate::input::MouseButton;
use crate::ui::{Style, UiContext};
use crate::util::{Animation, FontUtil};

static DEFAULT_STYLE: Lazy<Style> = lazy_style! {
    checkbox {
        size: 16.0,
        foreground: Hsva::new(0.0, 0.0, 0.1, 1.0),
        border_color: Hsva::new(0.0, 0.0, 0.3, 1.0),
        check_color: Hsva::new(0.0, 0.0, 0.15, 1.0),
        font_size: 20.0,
        line_height: 1.2,
    };
};

/// Checkbox
pub struct Checkbox<'a> {
    label: &'a str,
    id: &'a str,
    value: &'a mut bool,
    width: f32,
    height: f32,
}

impl Checkbox<'_> {
    /// Create a new checkbox
    pub fn new<'a>(uctx: &mut UiContext<'a>, label: &str, value: &'a mut bool) -> Checkbox<'a> {
        let label = uctx.bump.alloc_str(label);
        let mut split = label.rsplitn(2, "##");
        let id = split.next().unwrap_or(label);
        let label = split.next().unwrap_or(label);

        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let font_size = uctx.style.get("checkbox", "font_size");
        let line_height = uctx.style.get::<f32>("checkbox", "line_height");
        let size = uctx.style.get::<f32>("checkbox", "size");
        uctx.style.deque.pop_back();

        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let width = font.text_width(font_size, label);

        Checkbox {
            label,
            id,
            value,
            width: size * 1.5 + width,
            height: size.max(font_size * line_height),
        }
    }
}

impl<'a> Widget<'a> for Checkbox<'a> {
    fn id(&self) -> Option<&'a str> {
        Some(self.id)
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let size = uctx.style.get::<f32>("checkbox", "size");
        let font_size = uctx.style.get("checkbox", "font_size");
        let foreground = uctx.style.get::<Hsva>("checkbox", "foreground");
        let border_color = uctx.style.get::<Hsva>("checkbox", "border_color");
        let mut check_color = uctx.style.get::<Hsva>("checkbox", "check_color");
        uctx.style.deque.pop_back();

        let storage = uctx.storage.widget(&uctx.id);
        let time = storage
            .get::<Animation<f32>>()
            .map(|animation| animation.value)
            .unwrap_or(0.0);

        let pos = geometry.bounds.min + Vector2::new(0.0, (self.height - size) / 2.0);
        uctx.rect(pos, [size, size], (0.0, 0.0, 0.0, 0.0))
            .with_border(Border::new(border_color, 1.0));

        if time > f32::EPSILON {
            let angle = (1.0 - time) * 10f32.to_radians();
            check_color.alpha *= time;

            uctx.push_matrix();
            uctx.rotate(UnitComplex::from_angle(215f32.to_radians() + angle));
            uctx.translate(pos.coords + Vector2::new(size * 0.4, size * 0.75));
            uctx.rect([-1.0, -1.0], [2.0, size * 0.72], check_color);
            uctx.pop_matrix();

            uctx.push_matrix();
            uctx.rotate(UnitComplex::from_angle(125f32.to_radians() + angle));
            uctx.translate(pos.coords + Vector2::new(size * 0.4, size * 0.75));
            uctx.rect([-1.0, -1.0], [2.0, size * 0.32], check_color);
            uctx.pop_matrix();
        }

        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let v_metrics = font.v_metrics(Scale::uniform(font_size));

        let pos =
            geometry.bounds.min + Vector2::new(size * 1.5, self.height / 2.0 - v_metrics.descent);
        let font = uctx.font;
        uctx.text(font, pos, font_size, foreground, self.label);
    }

    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let mut storage = uctx.storage.widget(&uctx.id);
        let init = *self.value as i32 as f32;
        let value = storage.get::<bool>().copied().unwrap_or(false);
        let animation = storage
            .entry::<Animation<f32>>()
            .or_insert_with(|| Animation::new(init, 8.0));

        let update_value = *self.value != value;
        if update_value {
            animation.go_to(*self.value as i32 as f32);
        }

        animation.update(uctx.dt);

        let bounds = &geometry.bounds;

        if bounds.contains_point(&uctx.input.mouse_position())
            && uctx.input.has_mouse_released(MouseButton::Left)
        {
            *self.value = !*self.value;
            animation.go_to(*self.value as i32 as f32);
            storage.insert(*self.value);
        }

        if update_value {
            storage.insert(*self.value);
        }
    }

    fn size(&mut self, _: &UiContext<'a>, _: Vector2<f32>) -> Vector2<f32> {
        Vector2::new(self.width, self.height)
    }
}
