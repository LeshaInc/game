use nalgebra::Vector2;

use super::{Geometry, Widget};
use crate::ui::widget::{Stack, StackContainer, StackSettings};
use crate::ui::UiContext;
use crate::util::SideOffsets;

/// Padding box
pub struct PaddingBox<'a> {
    padding: SideOffsets<f32>,
    inner: Stack<'a>,
}

impl<'a> PaddingBox<'a> {
    /// Create a new padding box
    pub fn new<F>(uctx: &mut UiContext<'a>, padding: SideOffsets<f32>, factory: F) -> PaddingBox<'a>
    where
        F: FnOnce(&mut StackContainer<'a, '_>),
    {
        let inner = Stack::new(uctx, StackSettings::default(), factory);
        PaddingBox { padding, inner }
    }

    fn inner_geometry(&self, geometry: &Geometry) -> Geometry {
        let bounds = self.padding.shrink_aabr(&geometry.bounds);
        Geometry {
            bounds,
            clamped_bounds: bounds.intersection(&geometry.clamped_bounds),
            parent_clamped_bounds: geometry.clamped_bounds,
        }
    }
}

impl<'a> Widget<'a> for PaddingBox<'a> {
    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let geometry = self.inner_geometry(geometry);
        self.inner.update(uctx, &geometry);
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let geometry = self.inner_geometry(geometry);
        self.inner.draw(uctx, &geometry);
    }

    fn size(&mut self, uctx: &UiContext<'a>, hint: Vector2<f32>) -> Vector2<f32> {
        self.inner.size(uctx, hint - self.padding.size_addition()) + self.padding.size_addition()
    }

    fn stretch_factor(&self) -> f32 {
        self.inner.stretch_factor()
    }
}
