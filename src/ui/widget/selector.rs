use nalgebra::{UnitComplex, Vector2};
use once_cell::sync::Lazy;
use palette::Hsva;
use rusttype::Scale;

use super::{Geometry, Widget};
use crate::input::MouseButton;
use crate::ui::{Style, UiContext};
use crate::util::{Aabr, Animation, FontUtil};

static DEFAULT_STYLE: Lazy<Style> = lazy_style! {
    selector {
        foreground: Hsva::new(0.0, 0.0, 0.1, 1.0),
        arrow_foreground: Hsva::new(0.0, 0.0, 0.2, 1.0),
        width: 160.0,
        height: 24.0,
    };
};

/// Settings of the selector widget
pub struct SelectorSettings<'a, T> {
    id: &'a str,
    value: &'a mut T,
    options: Vec<(&'a str, T)>,
}

impl<'a, T: PartialEq + Clone> SelectorSettings<'a, T> {
    /// Create new selector settings
    pub fn new(id: &'a str, value: &'a mut T) -> SelectorSettings<'a, T> {
        SelectorSettings {
            id,
            value,
            options: Vec::new(),
        }
    }

    /// Add an option
    pub fn with_option(mut self, name: &'a str, value: T) -> Self {
        self.options.push((name, value));
        self
    }
}

/// Selector
pub struct Selector<'a, T> {
    settings: SelectorSettings<'a, T>,
    selected: usize,
    width: f32,
    height: f32,
}

impl<T: PartialEq + Clone> Selector<'_, T> {
    /// Create a new selector
    pub fn new<'a>(uctx: &mut UiContext<'a>, settings: SelectorSettings<'a, T>) -> Selector<'a, T> {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let width = uctx.style.get("selector", "width");
        let height = uctx.style.get("selector", "height");
        uctx.style.deque.pop_back();

        let mut options = settings.options.iter();
        let selected = options.position(|v| v.1 == *settings.value).unwrap();

        Selector {
            settings,
            selected,
            width,
            height,
        }
    }

    fn left_button_bounds(&self, bounds: &Aabr<f32>) -> Aabr<f32> {
        let aabr = Aabr::new(
            bounds.min,
            bounds.min + Vector2::new(self.height, self.height),
        );
        bounds.intersection(&aabr)
    }

    fn right_button_bounds(&self, bounds: &Aabr<f32>) -> Aabr<f32> {
        let aabr = Aabr::new(
            bounds.min + Vector2::new(bounds.width() - self.height, 0.0),
            bounds.min + Vector2::new(bounds.width(), self.height),
        );
        bounds.intersection(&aabr)
    }
}

impl<'a, T: PartialEq + Clone> Widget<'a> for Selector<'a, T> {
    fn id(&self) -> Option<&'a str> {
        Some(self.settings.id)
    }

    fn update(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        let left = self.left_button_bounds(&geometry.clamped_bounds);
        let l_entered = uctx.input.has_mouse_entered(&left);
        let l_left = uctx.input.has_mouse_left(&left);
        let l_clicked = left.contains_point(&uctx.input.mouse_position())
            && uctx.input.has_mouse_released(MouseButton::Left);

        let right = self.right_button_bounds(&geometry.clamped_bounds);
        let r_entered = uctx.input.has_mouse_entered(&right);
        let r_left = uctx.input.has_mouse_left(&right);
        let r_clicked = right.contains_point(&uctx.input.mouse_position())
            && uctx.input.has_mouse_released(MouseButton::Left);

        let mut storage = uctx.storage.widget(&uctx.id);
        let (l_anim, r_anim) = storage
            .entry::<(Animation<f32>, Animation<f32>)>()
            .or_insert_with(|| (Animation::new(0.0, 6.0), Animation::new(0.0, 6.0)));

        let offset = 3.0;

        if self.selected > 0 {
            if l_clicked {
                self.selected -= 1;
                *self.settings.value = self.settings.options[self.selected].1.clone();
            } else if l_entered {
                l_anim.go_to(-offset);
            }
        }
        if l_left {
            l_anim.go_to(0.0);
        }

        if self.selected < self.settings.options.len() - 1 {
            if r_clicked {
                self.selected += 1;
                *self.settings.value = self.settings.options[self.selected].1.clone();
            } else if r_entered {
                r_anim.go_to(offset);
            }
        }
        if r_left {
            r_anim.go_to(0.0);
        }

        l_anim.update(uctx.dt);
        r_anim.update(uctx.dt);
    }

    fn draw(&mut self, uctx: &mut UiContext<'a>, geometry: &Geometry) {
        uctx.style.deque.push_back(&DEFAULT_STYLE);
        let foreground = uctx.style.get::<Hsva>("selector", "foreground");
        let arrow_foreground = uctx.style.get::<Hsva>("selector", "arrow_foreground");
        uctx.style.deque.pop_back();

        let text = self.settings.options[self.selected].0;

        let font = uctx.assets.get_by_id(uctx.font).unwrap();
        let font_size = self.height * 0.75;
        let v_metrics = font.v_metrics(Scale::uniform(font_size));
        let text_width = font.text_width(font_size, text);

        let pos = geometry.bounds.min
            + Vector2::new(
                (self.width - text_width) / 2.0,
                self.height / 2.0 - v_metrics.descent,
            );
        let font = uctx.font;
        uctx.text(font, pos, font_size, foreground, text);

        let storage = uctx.storage.widget(&uctx.id);
        let (l_offset, r_offset) = storage
            .get::<(Animation<f32>, Animation<f32>)>()
            .map(|(l, r)| (l.value, r.value))
            .unwrap_or((0.0, 0.0));

        if self.selected > 0 {
            let origin =
                geometry.bounds.min + Vector2::new(self.height * 0.3 + l_offset, self.height / 2.0);
            uctx.push_matrix();
            uctx.rotate(UnitComplex::from_angle(45f32.to_radians()));
            uctx.translate(origin.coords);
            uctx.rect([-0.75, -0.75], [self.height * 0.42, 1.5], arrow_foreground);
            uctx.pop_matrix();
            uctx.push_matrix();
            uctx.rotate(UnitComplex::from_angle(-45f32.to_radians()));
            uctx.translate(origin.coords);
            uctx.rect([-0.75, -0.75], [self.height * 0.42, 1.5], arrow_foreground);
            uctx.pop_matrix();
        }

        if self.selected < self.settings.options.len() - 1 {
            let origin = geometry.bounds.min
                + Vector2::new(self.width - self.height * 0.3 + r_offset, self.height / 2.0);
            uctx.push_matrix();
            uctx.rotate(UnitComplex::from_angle(135f32.to_radians()));
            uctx.translate(origin.coords);
            uctx.rect([-0.75, -0.75], [self.height * 0.42, 1.5], arrow_foreground);
            uctx.pop_matrix();
            uctx.push_matrix();
            uctx.rotate(UnitComplex::from_angle(-135f32.to_radians()));
            uctx.translate(origin.coords);
            uctx.rect([-0.75, -0.75], [self.height * 0.42, 1.5], arrow_foreground);
            uctx.pop_matrix();
        }
    }

    fn size(&mut self, _: &UiContext<'a>, _: Vector2<f32>) -> Vector2<f32> {
        Vector2::new(self.width, self.height)
    }
}
