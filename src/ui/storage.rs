//! UI storage

use std::any::Any;
use std::collections::{hash_map, HashMap};
use std::hash::{BuildHasherDefault, Hasher};

use super::WidgetId;
use crate::util::AnyMap;

struct U64Hasher {
    value: u64,
}

impl Default for U64Hasher {
    fn default() -> U64Hasher {
        U64Hasher { value: 0 }
    }
}

impl Hasher for U64Hasher {
    fn write(&mut self, _: &[u8]) {
        unimplemented!("u64 hasher supports only write_u64");
    }

    fn write_u64(&mut self, value: u64) {
        self.value = value;
    }

    fn finish(&self) -> u64 {
        self.value
    }
}

#[derive(Debug)]
struct TypeStorage<T> {
    map: HashMap<u64, T, BuildHasherDefault<U64Hasher>>,
}

impl<T> Default for TypeStorage<T> {
    fn default() -> TypeStorage<T> {
        TypeStorage {
            map: HashMap::default(),
        }
    }
}

/// UI storage which contains widget specific data
#[derive(Default)]
pub struct UiStorage {
    per_type: AnyMap<dyn Any + Send + Sync>,
}

impl UiStorage {
    /// Get the storage of widget with given ID
    pub fn widget(&mut self, id: &WidgetId) -> WidgetStorage<'_> {
        WidgetStorage {
            storage: self,
            key: id.get_hash(),
        }
    }
}

/// Widget data storage (can hold any `Send + Sync + 'static` type)
pub struct WidgetStorage<'a> {
    storage: &'a mut UiStorage,
    key: u64,
}

impl WidgetStorage<'_> {
    fn for_type<T: Send + Sync + 'static>(&self) -> Option<&TypeStorage<T>> {
        self.storage.per_type.get()
    }

    fn for_type_mut<T: Send + Sync + 'static>(&mut self) -> &mut TypeStorage<T> {
        self.storage.per_type.entry().or_default()
    }

    /// Insert a new value, returning the old one if it was there.
    pub fn insert<T: Send + Sync + 'static>(&mut self, value: T) -> Option<T> {
        let key = self.key;
        self.for_type_mut().map.insert(key, value)
    }

    /// Get a reference to the value, if it exists
    pub fn get<T: Send + Sync + 'static>(&self) -> Option<&T> {
        self.for_type().and_then(|v| v.map.get(&self.key))
    }

    /// Get a mutable reference to the value, if it exists
    pub fn get_mut<T: Send + Sync + 'static>(&mut self) -> Option<&mut T> {
        let key = self.key;
        self.for_type_mut().map.get_mut(&key)
    }

    /// Check whether the storage has the specified value
    pub fn contains<T: Send + Sync + 'static>(&self) -> bool {
        self.for_type::<T>()
            .filter(|v| v.map.contains_key(&self.key))
            .is_some()
    }

    /// Provides in-place mutable access to an occupied entry before any potential inserts into the map
    pub fn entry<T: Send + Sync + 'static>(&mut self) -> hash_map::Entry<'_, u64, T> {
        let key = self.key;
        self.for_type_mut::<T>().map.entry(key)
    }
}
