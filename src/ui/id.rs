use std::convert::TryInto;
use std::hash::{Hash, Hasher};

use fxhash::FxHasher;

/// A `Path` like widget ID. Stores the widget's ID, as well as its all ancestors and component
/// hashes
#[derive(Clone, Default)]
pub struct WidgetId {
    data: Vec<u8>,
}

fn hash_slice(data: &[u8]) -> u64 {
    let mut hasher = FxHasher::default();
    data.hash(&mut hasher);
    hasher.finish()
}

fn hash_2_u64(a: u64, b: u64) -> u64 {
    let mut hasher = FxHasher::default();
    hasher.write_u64(a);
    hasher.write_u64(b);
    hasher.finish()
}

impl WidgetId {
    /// ID of the root
    pub fn new() -> WidgetId {
        WidgetId::default()
    }

    #[inline(always)]
    fn get_hash_fallible(&self) -> Option<u64> {
        let idx_start = self.data.len().checked_sub(9)?;
        let idx_end = self.data.len().checked_sub(1)?;
        let slice = self.data.get(idx_start..idx_end)?;
        let bytes = slice.try_into().ok()?;
        Some(u64::from_ne_bytes(bytes))
    }

    /// Get hash of the ID
    ///
    /// This function is fast and can be relied upon.
    pub fn get_hash(&self) -> u64 {
        self.get_hash_fallible().unwrap_or_else(|| hash_slice(&[]))
    }

    /// Push a component. Think of it as `self/id` as it was a filesystem path
    pub fn push(&mut self, id: &str) {
        assert!(!id.contains('\0'));
        let parent_hash = self.get_hash();
        let child_hash = hash_slice(id.as_bytes());
        self.data.reserve(id.len() + 9);
        self.data.extend_from_slice(id.as_bytes());
        let new_hash = hash_2_u64(parent_hash, child_hash);
        self.data.extend_from_slice(&new_hash.to_ne_bytes());
        self.data.push(0);
    }

    /// Pop the last component. If there's nothing to pop, does nothing
    pub fn pop(&mut self) {
        if self.data.len() < 9 {
            return;
        }

        self.data.drain(self.data.len() - 9..);
        for (i, &byte) in self.data.iter().enumerate().rev() {
            if byte == 0 {
                if i + 1 < self.data.len() {
                    self.data.drain(i + 1..);
                }
                return;
            }
        }

        self.data.clear();
    }
}

impl Hash for WidgetId {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.get_hash().hash(hasher);
    }
}
