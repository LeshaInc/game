//! User interface

#[macro_use]
mod style;

mod container;
mod context;
mod id;
mod storage;

pub mod widget;

pub use self::container::Container;
pub use self::context::UiContext;
pub use self::id::WidgetId;
pub use self::storage::*;
pub use self::style::*;
pub use self::widget::Widget;

use rusttype::Font;

use crate::assets::Handle;

/// Handle to the default font
pub struct DefaultFont(pub Handle<Font<'static>>);
