//! An unnamed game

#![warn(missing_docs)]
#![warn(rust_2018_idioms)]

#[macro_use]
pub mod ui;

pub mod input;
pub mod world;

mod main_loop;

pub use self::main_loop::*;
pub use game_core::*;
