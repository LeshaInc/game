//! World

mod block;
mod draw;

pub use self::block::*;
pub use self::draw::*;

use fxhash::FxHashMap;
use nalgebra::{Point2, Point3};

use crate::bvh::OrthoPlanes;

/// Chunk size along X and Y axis. Should be a power of two
pub const CHUNK_SIZE: usize = 16;
/// Chunk height
pub const MAX_HEIGHT: usize = 6;

/// World chunk (16x16x6)
#[derive(Clone, Debug)]
pub struct Chunk {
    /// Chunk position
    pub pos: Point2<i16>,
    /// Blocks (unit cubes)
    pub blocks: ChunkBlocks,
}

impl Chunk {
    /// Create an empty chunk
    pub fn new(pos: Point2<i16>) -> Chunk {
        Chunk {
            pos,
            blocks: ChunkBlocks::default(),
        }
    }

    /// Get chunk position in world space `(x * CHUNK_SIZE, y * CHUNK_SIZE, 0)`
    pub fn world_pos(&self) -> Point3<f32> {
        Point3::new(
            f32::from(self.pos.x) * (CHUNK_SIZE as f32),
            f32::from(self.pos.y) * (CHUNK_SIZE as f32),
            0.0,
        )
    }

    /// Run the closure for each visible block
    pub fn foreach_visible_block<F>(&self, planes: &OrthoPlanes, f: F)
    where
        F: FnMut(Point3<u8>, BlockId),
    {
        let pos = self.world_pos();
        self.blocks.foreach_visible(pos, planes, f);
    }
}

/// All loaded chunks
#[derive(Clone, Debug, Default)]
pub struct WorldMap {
    /// Chunks
    pub chunks: FxHashMap<Point2<i16>, Chunk>,
}
