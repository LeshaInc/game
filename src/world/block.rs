use std::convert::TryFrom;

use nalgebra::{Point2, Point3, Vector2, Vector3};
use ncollide3d::bounding_volume::AABB;

use super::{CHUNK_SIZE, MAX_HEIGHT};
use crate::assets::Handle;
use crate::bvh::{OrthoPlanes, PlaneFlags};
use crate::graphics::Model;

/// Id of a block
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct BlockId(pub u16);

impl BlockId {
    /// Empty block
    pub const AIR: BlockId = BlockId(0);
}

/// A single layer of blocks
#[derive(Clone, Copy, Debug)]
pub struct BlockLayer {
    /// 2D array of blocks (`blocks[x][y]`)
    pub blocks: [[BlockId; CHUNK_SIZE]; CHUNK_SIZE],
}

/// Stores information about blocks inside a chunk
#[derive(Clone, Debug)]
pub struct ChunkBlocks {
    /// Layers stacked on top of each other
    pub layers: [BlockLayer; MAX_HEIGHT],
}

impl Default for ChunkBlocks {
    fn default() -> ChunkBlocks {
        ChunkBlocks {
            layers: [BlockLayer {
                blocks: [[BlockId::AIR; CHUNK_SIZE]; CHUNK_SIZE],
            }; MAX_HEIGHT],
        }
    }
}

impl ChunkBlocks {
    /// Set block
    pub fn set(&mut self, pos: impl Into<Point3<u8>>, id: BlockId) {
        let pos: Point3<usize> = nalgebra::convert(pos.into());
        self.layers[pos.z].blocks[pos.x][pos.y] = id;
    }

    /// Get block
    pub fn get(&self, pos: impl Into<Point3<u8>>) -> BlockId {
        let pos: Point3<usize> = nalgebra::convert(pos.into());
        self.layers[pos.z].blocks[pos.x][pos.y]
    }

    fn indices() -> impl Iterator<Item = (usize, usize, usize)> {
        (0..MAX_HEIGHT).flat_map(|z| {
            (0..CHUNK_SIZE)
                .flat_map(|x| (0..CHUNK_SIZE).map(move |y| (x, y)))
                .map(move |(x, y)| (x, y, z))
        })
    }

    /// Iterate over block IDs. Empty blocks are skipped
    pub fn iter(&self) -> impl Iterator<Item = BlockId> + '_ {
        Self::indices()
            .map(move |(x, y, z)| self.layers[z].blocks[x][y])
            .filter(|b| *b != BlockId::AIR)
    }

    /// Iterate over block IDs with position. Empty blocks are skipped
    pub fn iter_pos(&self) -> impl Iterator<Item = (Point3<u8>, BlockId)> + '_ {
        Self::indices()
            .map(move |(x, y, z)| {
                (
                    Point3::new(x as u8, y as u8, z as u8),
                    self.layers[z].blocks[x][y],
                )
            })
            .filter(|(_, b)| *b != BlockId::AIR)
    }

    /// Run the closure for each visible block
    pub fn foreach_visible<F>(&self, pos: Point3<f32>, planes: &OrthoPlanes, mut f: F)
    where
        F: FnMut(Point3<u8>, BlockId),
    {
        let min = Point2::new(0, 0);
        let max = Point2::new(CHUNK_SIZE as u8, CHUNK_SIZE as u8);
        self._foreach_visible(min, max, PlaneFlags::default(), pos, planes, &mut f);
    }

    fn _foreach_visible<F>(
        &self,
        min: Point2<u8>,
        max: Point2<u8>,
        flags: PlaneFlags,
        pos: Point3<f32>,
        planes: &OrthoPlanes,
        f: &mut F,
    ) where
        F: FnMut(Point3<u8>, BlockId),
    {
        let size = max - min;
        let minf = Vector3::new(min.x as f32, min.y as f32, 0.0);
        let maxf = Vector3::new(max.x as f32, max.y as f32, MAX_HEIGHT as f32);
        let aabb = AABB::new(pos + minf, pos + maxf);

        let flags = match planes.intersect(&aabb, flags) {
            Some(v) => v,
            None => return,
        };

        if flags.in_front_all() || size == Vector2::new(1, 1) {
            for z in 0..MAX_HEIGHT {
                for x in min.x..max.x {
                    for y in min.y..max.y {
                        let pos = Point3::new(x, y, z as u8);
                        let idx: Point3<usize> = nalgebra::convert(pos);
                        let block = self.layers[idx.z].blocks[idx.x][idx.y];
                        if block != BlockId::AIR {
                            f(pos, block);
                        }
                    }
                }
            }
        } else {
            let size = if size.x > size.y {
                Vector2::new(size.x / 2, size.y)
            } else {
                Vector2::new(size.x, size.y / 2)
            };

            self._foreach_visible(min, min + size, flags, pos, planes, f);
            self._foreach_visible(max - size, max, flags, pos, planes, f);
        }
    }
}

/// Contains information about all block types
#[derive(Clone, Debug, Default)]
pub struct BlockRegistry {
    blocks: Vec<BlockType>,
}

impl BlockRegistry {
    /// Register a block type
    pub fn register(&mut self, block: BlockType) -> BlockId {
        let id = BlockId(
            self.blocks
                .len()
                .checked_add(1)
                .and_then(|val| u16::try_from(val).ok())
                .expect("too many blocks"),
        );

        self.blocks.push(block);

        id
    }

    /// Get a block type by ID
    pub fn get(&self, id: BlockId) -> &BlockType {
        &self.blocks[id.0 as usize - 1]
    }
}

/// Block type
#[derive(Clone, Debug)]
pub struct BlockType {
    /// Model
    pub model: Handle<Model>,
}
