use legion::systems::{ParallelRunnable, SystemBuilder};
use nalgebra::{Isometry3, Point3, Translation3, Unit, Vector3};

use super::{BlockRegistry, WorldMap};
use crate::assets::Assets;
use crate::bvh::OrthoPlanes;
use crate::graphics::{CasterDrawList3D, Encoder3D};
use crate::Camera;

/// Build the world drawing system
pub fn draw_world_system() -> impl ParallelRunnable {
    SystemBuilder::new("draw-world")
        .read_resource::<Camera>()
        .read_resource::<Assets>()
        .read_resource::<WorldMap>()
        .read_resource::<BlockRegistry>()
        .build(move |cbuf, _, (camera, assets, world, registry), _| {
            let mut encoder = Encoder3D::new(&assets);
            let mut caster_encoder = Encoder3D::new(&assets);

            let planes = OrthoPlanes::from_camera(camera);

            let light_dir = Unit::new_normalize(Vector3::new(1.0, 1.0, -2.0));
            let (_, caster_planes) = camera.shadow_matrix(light_dir, 4.0);

            for chunk in world.chunks.values() {
                let chunk_pos = chunk.world_pos();

                let add_block = |encoder: &mut Encoder3D<'_>, block_pos, block_id| {
                    let block = registry.get(block_id);
                    let block_pos: Point3<f32> = nalgebra::convert(block_pos);
                    let world_pos = chunk_pos + block_pos.coords;

                    let mut trans = Isometry3::identity();
                    trans.append_translation_mut(&Translation3::from(world_pos.coords));
                    encoder.model(&block.model, trans);
                };

                chunk.foreach_visible_block(&planes, |block_pos, block_id| {
                    add_block(&mut encoder, block_pos, block_id)
                });

                chunk.foreach_visible_block(&caster_planes, |block_pos, block_id| {
                    add_block(&mut caster_encoder, block_pos, block_id)
                });
            }

            cbuf.push((CasterDrawList3D(caster_encoder.list),));
            cbuf.push((encoder.list,));
        })
}
