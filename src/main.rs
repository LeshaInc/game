use anyhow::Result;
use bumpalo::Bump;
use legion::systems::{ParallelRunnable, SystemBuilder};
use legion::{Resources, Schedule, World};
use nalgebra::{Isometry3, Point2, Translation3, UnitComplex, Vector2};
use rusttype::Font;

use game::assets::{Assets, DirectorySource, Handle};
use game::graphics::drawlist2d::{BoxShadow, CornerRadii};
use game::graphics::{Encoder2D, Encoder3D, GraphicsSettings, Mesh, Model, Texture};
use game::input::{Input, MouseButton, VirtualKeyCode};
use game::ui::widget::{Geometry, PaddingBox, SelectorSettings, Widget};
use game::ui::{Container, DefaultFont, StyleChain, UiContext, UiStorage, WidgetId};
use game::util::{Aabr, FpsCounter, SideOffsets, Stats};
use game::world::{draw_world_system, BlockRegistry, BlockType, Chunk, WorldMap};
use game::{
    resolve_transform_system, style, AssetsPath, Camera, Cursor, DeltaTime, MaxSettings,
    WindowResolution,
};

fn draw_ui_system() -> impl ParallelRunnable {
    let mut visible = false;

    SystemBuilder::new("draw-ui")
        .read_resource::<Input>()
        .read_resource::<WindowResolution>()
        .read_resource::<DeltaTime>()
        .read_resource::<Assets>()
        .read_resource::<FpsCounter>()
        .read_resource::<DefaultFont>()
        .write_resource::<UiStorage>()
        .build(move |cbuf, _, res, _| {
            let _time_guard = Stats::get().time_guard("build-ui");

            let (input, res, dt, assets, fps_counter, font, storage) = res;

            if input.has_pressed(VirtualKeyCode::F1) {
                visible = !visible;
            }

            if !visible {
                return;
            }

            let mut encoder = Encoder2D::default();

            let bump = Bump::new();
            let mut uctx = UiContext {
                bump: &bump,
                encoder: &mut encoder,
                assets: &assets,
                font: font.0.id(),
                id: WidgetId::new(),
                storage,
                input: &input,
                dt: dt.0,
                style: StyleChain::default(),
            };

            let mut stack = PaddingBox::new(&mut uctx, SideOffsets::new_equal(4.0), |stack| {
                stack.label(&format!("FPS: {}", fps_counter.avg_fps()));
                stack.label(&format!("SPF: {:?}", fps_counter.avg_frametime()));

                let s = style! {
                    text {
                        font_size: 16.0,
                    };
                };

                stack.style_scope(s, |scope| {
                    scope.text(&Stats::get().to_string());
                });
            });

            let size = stack.size(&uctx, Vector2::new(200.0, res.0.y));
            let min = Point2::new(0.0, 0.0);

            let bounds = Aabr::new(min, min + size);
            let geometry = Geometry {
                bounds,
                clamped_bounds: bounds,
                parent_clamped_bounds: bounds,
            };

            uctx.rect(min, size, (1.0, 1.0, 1.0, 0.5));

            stack.update(&mut uctx, &geometry);
            stack.draw(&mut uctx, &geometry);
            drop(stack);

            cbuf.push((encoder.list,));
        })
}

fn settings_ui_system() -> impl ParallelRunnable {
    let mut visible = false;

    SystemBuilder::new("draw-settings-ui")
        .read_resource::<Input>()
        .read_resource::<WindowResolution>()
        .read_resource::<DeltaTime>()
        .read_resource::<Assets>()
        .read_resource::<DefaultFont>()
        .read_resource::<MaxSettings>()
        .write_resource::<GraphicsSettings>()
        .write_resource::<UiStorage>()
        .build(move |cbuf, _, res, _| {
            let _time_guard = Stats::get().time_guard("build-settings-ui");

            let (input, res, dt, assets, font, max_settings, settings, storage) = res;

            if input.has_pressed(VirtualKeyCode::F3) {
                visible = !visible;
            }

            if !visible {
                return;
            }

            let mut encoder = Encoder2D::default();

            let size = Vector2::new(350.0, 272.0);
            let pos = Point2::from((res.0 - size) / 2.0);

            encoder
                .rect(pos, size, (1.0, 1.0, 1.0, 0.7))
                .with_corner_radii(CornerRadii::new_equal(3.0))
                .with_shadow(BoxShadow::new((0.0, 0.0, 0.0, 0.5), 10.0).with_offset([0.0, 4.0]));

            let bump = Bump::new();
            let mut uctx = UiContext {
                bump: &bump,
                encoder: &mut encoder,
                assets: &assets,
                font: font.0.id(),
                id: WidgetId::new(),
                storage,
                input: &input,
                dt: dt.0,
                style: StyleChain::default(),
            };

            let GraphicsSettings {
                ref mut msaa,
                ref mut shadows,
                ref mut shadow_resolution,
            } = **settings;

            let mut stack = PaddingBox::new(&mut uctx, SideOffsets::new_equal(16.0), |stack| {
                stack.hstack(|row| {
                    row.label("MSAA");
                    row.filler(1.0);
                    let options = [
                        ("Off", 1),
                        ("2× (Low)", 2),
                        ("4× (Medium)", 4),
                        ("8× (High)", 8),
                        ("16× (Very High)", 16),
                        ("32× (Insane)", 32),
                    ];
                    let mut settings = SelectorSettings::new("msaa", msaa);
                    for opt in &options[..max_settings.0.msaa.trailing_zeros() as usize + 1] {
                        settings = settings.with_option(opt.0, opt.1);
                    }
                    row.selector(settings);
                });

                let shadows_enabled = *shadows;
                stack.checkbox("Shadows", shadows);
                if shadows_enabled {
                    stack.hstack(|row| {
                        row.label("Shadow resolution");
                        row.filler(1.0);
                        let settings = SelectorSettings::new("shadow-res", shadow_resolution)
                            .with_option("512² (Low)", 512)
                            .with_option("1024² (Medium)", 1024)
                            .with_option("2048² (High)", 2048)
                            .with_option("4096² (Very High)", 4096);
                        row.selector(settings);
                    });
                }
            });

            let bounds = Aabr::new(pos, pos + stack.size(&uctx, size));
            let geometry = Geometry {
                bounds,
                clamped_bounds: bounds,
                parent_clamped_bounds: bounds,
            };

            stack.update(&mut uctx, &geometry);
            stack.draw(&mut uctx, &geometry);
            drop(stack);

            cbuf.push((encoder.list,));
        })
}

fn camera_controls_system() -> impl ParallelRunnable {
    let mut scale_target = 5.0;
    let mut grab_position = None;
    SystemBuilder::new("camera-controls")
        .read_resource::<Input>()
        .read_resource::<WindowResolution>()
        .read_resource::<DeltaTime>()
        .write_resource::<Camera>()
        .write_resource::<Cursor>()
        .build(move |_, _, (input, res, dt, camera, cursor), _| {
            let mut offset = Vector2::new(0.0, 0.0);

            // horizontal movement

            if input.is_key_down(VirtualKeyCode::W) {
                offset.x -= 1.0;
            }
            if input.is_key_down(VirtualKeyCode::S) {
                offset.x += 1.0;
            }
            if input.is_key_down(VirtualKeyCode::A) {
                offset.y -= 1.0;
            }
            if input.is_key_down(VirtualKeyCode::D) {
                offset.y += 1.0;
            }

            // scaling

            let mag: f32 = offset.magnitude_squared();
            if mag > 0.5 {
                let yaw = UnitComplex::new(camera.yaw);
                offset *= camera.scale * dt.0 * 3.0 / mag.sqrt();
                camera.position += yaw * offset;
            }

            if input.wheel_delta().abs() > 0.1 {
                let val = (camera.scale.powf(0.33) - 0.2 * input.wheel_delta()).powf(3.0);
                scale_target = val.max(2.0).min(50.0);
            }

            camera.scale += (scale_target - camera.scale) * dt.0 * 10.0;

            // rotation

            if input.is_key_down(VirtualKeyCode::Q) {
                camera.yaw -= dt.0 * 2.0;
            }
            if input.is_key_down(VirtualKeyCode::E) {
                camera.yaw += dt.0 * 2.0;
            }

            if input.is_mouse_button_down(MouseButton::Middle) {
                cursor.visible = false;
                cursor.grab = true;

                if grab_position.is_none() {
                    grab_position = Some(input.mouse_position());
                }

                let center = Point2::new((res.0.x / 2.0).floor(), res.0.y / 2.0);
                cursor.set_position = Some(center);

                let rotation_delta = input.mouse_delta().x * 0.005;
                camera.yaw -= rotation_delta;
            } else {
                cursor.set_position = grab_position.take();
                cursor.visible = true;
                cursor.grab = false;
            }

            camera.update_view();
            camera.update_projection();
        })
}

fn draw_floor_system(assets: &Assets) -> impl ParallelRunnable {
    let floor: Handle<Mesh> = assets.load("meshes/floor.mesh");

    SystemBuilder::new("draw-floor")
        .read_resource::<Assets>()
        .build(move |cbuf, _, assets, _| {
            let mut encoder = Encoder3D::new(assets);

            for x in -10..=10 {
                for y in -10..=10 {
                    let mut trans = Isometry3::identity();
                    trans.append_translation_mut(&Translation3::new(
                        (x as f32) * 16.0,
                        (y as f32) * 16.0,
                        0.0,
                    ));
                    encoder.plain_mesh(&floor, trans);
                }
            }

            cbuf.push((encoder.list,));
        })
}

fn main() -> Result<()> {
    env_logger::init();

    let mut root_path = std::env::current_exe()?;
    root_path.pop(); // remove executable name
    if root_path.ends_with("target/release/") || root_path.ends_with("target/debug") {
        root_path.pop();
        root_path.pop();
    }

    log::info!("Starting Game");

    let world = World::default();
    let mut resources = Resources::default();

    log::info!("Loading settings");

    root_path.push("settings.toml");
    let settings = GraphicsSettings::load(&root_path);
    root_path.pop();

    log::debug!("Settings: {:?}", settings);
    resources.insert(settings);

    let mut assets_path = root_path;
    assets_path.push("assets");

    let mut assets = Assets::new(DirectorySource::new(&assets_path));
    assets.register::<Font<'static>>();
    assets.register::<Mesh>();
    assets.register::<Texture>();
    assets.register::<Model>();

    let font_handle = assets.load("fonts/OpenSans-Regular.ttf");
    resources.insert(DefaultFont(font_handle));

    resources.insert(AssetsPath(assets_path));
    resources.insert::<UiStorage>(UiStorage::default());

    let mut camera = Camera::new(Point2::new(0.0, 0.0));
    camera.yaw = -135f32.to_radians();
    camera.update_view();
    camera.update_projection();
    resources.insert(camera);

    let mut registry = BlockRegistry::default();
    let conveyor = registry.register(BlockType {
        model: assets.load("models/conveyor.ron"),
    });
    let conveyor_p = registry.register(BlockType {
        model: assets.load("models/conveyor_p.ron"),
    });
    resources.insert(registry);

    let mut map = WorldMap::default();
    for cx in -10..10 {
        for cy in -10..10 {
            let mut chunk = Chunk::new(Point2::new(cx, cy));
            for x in 0..16 {
                for y in 0..16 {
                    if y % 2 == 0 {
                        chunk.blocks.set([x, y, 0], conveyor);
                    } else {
                        chunk.blocks.set([x, y, 0], conveyor_p);
                    }
                }
            }
            map.chunks.insert(chunk.pos, chunk);
        }
    }
    resources.insert(map);

    let schedule = Schedule::builder()
        .add_system(resolve_transform_system())
        .add_system(draw_ui_system())
        .add_system(settings_ui_system())
        .add_system(camera_controls_system())
        .add_system(draw_floor_system(&assets))
        .add_system(draw_world_system())
        .build();

    log::info!("Loading assets");

    let waiter = assets.spawn_workers();
    waiter.wait();
    assets.flush();

    resources.insert(assets);

    log::info!("Starting main loop");

    game::main_loop(world, resources, schedule)
}
