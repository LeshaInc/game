//! Input manager

pub use winit::event::{
    ElementState, KeyboardInput, ModifiersState, MouseButton, MouseScrollDelta, ScanCode,
    VirtualKeyCode, WindowEvent,
};

use std::{collections::HashSet, ops::Deref};

use either::Either;
use nalgebra::{Point2, Vector2};
use winit::dpi::LogicalPosition;

use crate::util::Aabr;

#[derive(Debug, Clone)]
/// Keyboard state containing all keys pressed at the moment
pub struct KeyboardState {
    pressed_scancodes: HashSet<ScanCode>,
    pressed_keycodes: HashSet<VirtualKeyCode>,
    modifiers: ModifiersState,
}

/// Trait implemented for types which can be viewed as a key identifier (virtual key code or scancode)
pub trait AsKey {
    /// Perform the conversion
    fn as_key(&self) -> Either<ScanCode, VirtualKeyCode>;
}

impl AsKey for ScanCode {
    fn as_key(&self) -> Either<ScanCode, VirtualKeyCode> {
        Either::Left(*self)
    }
}

impl AsKey for VirtualKeyCode {
    fn as_key(&self) -> Either<ScanCode, VirtualKeyCode> {
        Either::Right(*self)
    }
}

impl AsKey for Either<ScanCode, VirtualKeyCode> {
    fn as_key(&self) -> Either<ScanCode, VirtualKeyCode> {
        *self
    }
}

impl<T: AsKey> AsKey for &T {
    fn as_key(&self) -> Either<ScanCode, VirtualKeyCode> {
        (*self).as_key()
    }
}

impl KeyboardState {
    fn update_modifiers(&mut self, modifiers: ModifiersState) {
        self.modifiers = modifiers;
    }

    fn handle_event(&mut self, event: KeyboardInput) {
        match event.state {
            ElementState::Pressed => {
                if let Some(code) = event.virtual_keycode {
                    self.pressed_keycodes.insert(code);
                }

                self.pressed_scancodes.insert(event.scancode);
            }

            ElementState::Released => {
                if let Some(code) = event.virtual_keycode {
                    self.pressed_keycodes.remove(&code);
                }

                self.pressed_scancodes.remove(&event.scancode);
            }
        }
    }

    /// Is the specified key down?
    pub fn is_key_down(&self, key: impl AsKey) -> bool {
        match key.as_key() {
            Either::Left(scancode) => self.pressed_scancodes.contains(&scancode),
            Either::Right(keycode) => self.pressed_keycodes.contains(&keycode),
        }
    }

    /// Is the specified key up?
    pub fn is_key_up(&self, key: impl AsKey) -> bool {
        !self.is_key_down(key.as_key())
    }
}

impl Default for KeyboardState {
    fn default() -> KeyboardState {
        KeyboardState {
            pressed_keycodes: HashSet::with_capacity(128),
            pressed_scancodes: HashSet::with_capacity(128),
            modifiers: ModifiersState::default(),
        }
    }
}

#[derive(Debug, Clone)]
/// Current mouse state containing pressed buttons and cursor position
pub struct MouseState {
    pressed_buttons: HashSet<MouseButton>,
    position: Point2<f32>,
}

impl MouseState {
    fn handle_move_event(&mut self, position: LogicalPosition<f64>) {
        self.position = Point2::new(position.x as _, position.y as _);
    }

    fn handle_button_event(&mut self, state: ElementState, button: MouseButton) {
        match state {
            ElementState::Pressed => {
                self.pressed_buttons.insert(button);
            }

            ElementState::Released => {
                self.pressed_buttons.remove(&button);
            }
        }
    }

    /// Is the specified button down?
    pub fn is_mouse_button_down(&self, button: MouseButton) -> bool {
        self.pressed_buttons.contains(&button)
    }

    /// Current mouse cursor position (in logical coordinates)
    pub fn mouse_position(&self) -> Point2<f32> {
        self.position
    }
}

impl Default for MouseState {
    fn default() -> MouseState {
        MouseState {
            pressed_buttons: HashSet::with_capacity(4),
            position: Point2::new(0.0, 0.0),
        }
    }
}

#[derive(Debug, Clone)]
/// Current input state consisting of both mouse and keyboard states
pub struct InputState {
    keyboard: KeyboardState,
    mouse: MouseState,
    is_focused: bool,
    dpi_factor: f64,
}

impl InputState {
    fn handle_event(&mut self, event: &WindowEvent<'_>) {
        match *event {
            WindowEvent::Focused(focused) => self.is_focused = focused,
            WindowEvent::ModifiersChanged(mods) => self.keyboard.update_modifiers(mods),
            WindowEvent::KeyboardInput { input, .. } => self.keyboard.handle_event(input),

            WindowEvent::CursorMoved { position, .. } => {
                self.mouse
                    .handle_move_event(position.to_logical(self.dpi_factor));
            }

            WindowEvent::MouseInput { state, button, .. } => {
                self.mouse.handle_button_event(state, button)
            }

            WindowEvent::ScaleFactorChanged { scale_factor, .. } => self.dpi_factor = scale_factor,

            _ => {}
        }
    }

    /// Is the window focused?
    pub fn is_focused(&self) -> bool {
        self.is_focused
    }

    /// Current keyboard state
    pub fn keyboard_state(&self) -> &KeyboardState {
        &self.keyboard
    }

    /// Current mouse state
    pub fn mouse_state(&self) -> &MouseState {
        &self.mouse
    }
}

impl InputState {
    /// Is the specified key down?
    pub fn is_key_down(&self, key: impl AsKey) -> bool {
        self.keyboard.is_key_down(key.as_key())
    }

    /// Is the specified key up?
    pub fn is_key_up(&self, key: impl AsKey) -> bool {
        self.keyboard.is_key_up(key.as_key())
    }
}

impl InputState {
    /// Current mouse cursor position
    pub fn mouse_position(&self) -> Point2<f32> {
        self.mouse.mouse_position()
    }

    /// Is the specified mouse button down?
    pub fn is_mouse_button_down(&self, button: MouseButton) -> bool {
        self.mouse.is_mouse_button_down(button)
    }

    /// Is the specified mouse button up?
    pub fn is_mouse_button_up(&self, button: MouseButton) -> bool {
        !self.mouse.is_mouse_button_down(button)
    }
}

impl Default for InputState {
    fn default() -> InputState {
        InputState {
            keyboard: KeyboardState::default(),
            mouse: MouseState::default(),
            is_focused: true,
            dpi_factor: 1.0,
        }
    }
}

#[derive(Clone, Debug, Default)]
/// Input handler storing input events and current state
pub struct Input {
    state: InputState,
    prev_state: InputState,
    mouse_events: Vec<(MouseButton, ElementState)>,
    keyboard_events: Vec<KeyboardInput>,
    wheel_delta: f32,
}

impl Input {
    /// Change DPI factor. Default factor is `1.0`
    pub fn set_dpi_factor(&mut self, factor: f64) {
        self.state.dpi_factor = factor;
    }

    /// Current input state
    pub fn state(&self) -> &InputState {
        &self.state
    }

    /// Current input state
    pub fn prev_state(&self) -> &InputState {
        &self.prev_state
    }

    /// Clear the events
    pub fn next_tick(&mut self) {
        self.prev_state = self.state.clone();
        self.mouse_events.clear();
        self.keyboard_events.clear();
        self.wheel_delta = 0.0;
    }

    /// Change mouse position
    pub fn set_mouse_pos(&mut self, pos: Point2<f32>) {
        self.state.mouse.position = pos;
    }

    /// Handle a window event
    pub fn handle_event(&mut self, event: &WindowEvent<'_>) {
        self.state.handle_event(event);

        match event {
            WindowEvent::MouseInput { state, button, .. } => {
                self.mouse_events.push((*button, *state))
            }

            WindowEvent::MouseWheel { delta, .. } => match delta {
                MouseScrollDelta::LineDelta(_, y) => self.wheel_delta += y,
                MouseScrollDelta::PixelDelta(v) => self.wheel_delta += v.y as f32,
            },

            WindowEvent::KeyboardInput { input, .. } => self.keyboard_events.push(*input),

            _ => (),
        }
    }

    /// Are there any mouse release events?
    pub fn has_mouse_released(&self, button: MouseButton) -> bool {
        self.mouse_events
            .iter()
            .any(|(btn, state)| btn == &button && state == &ElementState::Released)
    }

    /// Are there any mouse press events?
    pub fn has_mouse_pressed(&self, button: MouseButton) -> bool {
        self.mouse_events
            .iter()
            .any(|(btn, state)| btn == &button && state == &ElementState::Pressed)
    }

    fn had_key_event(&self, key: impl AsKey, state: ElementState) -> bool {
        self.keyboard_events.iter().any(|input| {
            input.state == state
                && match (key.as_key(), input.virtual_keycode) {
                    (Either::Left(scancode), _) => input.scancode == scancode,
                    (Either::Right(keycode), Some(k)) => keycode == k,
                    _ => false,
                }
        })
    }

    /// Are there any key press events?
    pub fn has_pressed(&self, key: impl AsKey) -> bool {
        self.had_key_event(key, ElementState::Pressed)
    }

    /// Are there any key release events?
    pub fn has_released(&self, key: impl AsKey) -> bool {
        self.had_key_event(key, ElementState::Released)
    }

    /// Has mouse entered the given AABR?
    pub fn has_mouse_entered(&self, aabr: &Aabr<f32>) -> bool {
        !aabr.contains_point(&self.prev_state.mouse_position())
            && aabr.contains_point(&self.mouse_position())
    }

    /// Has mouse left the given AABR?
    pub fn has_mouse_left(&self, aabr: &Aabr<f32>) -> bool {
        aabr.contains_point(&self.prev_state.mouse_position())
            && !aabr.contains_point(&self.mouse_position())
    }

    /// Mouse cursor position change
    pub fn mouse_delta(&self) -> Vector2<f32> {
        self.state.mouse_position() - self.prev_state.mouse_position()
    }

    /// Mouse wheel delta
    pub fn wheel_delta(&self) -> f32 {
        self.wheel_delta
    }
}

impl Deref for Input {
    type Target = InputState;

    fn deref(&self) -> &InputState {
        &self.state
    }
}
