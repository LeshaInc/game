ATTR vec2 in_pos;

ATTR vec4 in_transform_col0_1;
ATTR vec2 in_transform_col2;
ATTR vec4 in_color;
ATTR vec4 in_uv_rect;

OUT vec4 v_color;
OUT vec2 v_uv;

void main() {
    v_color = in_color;
    v_uv = in_pos * in_uv_rect.zw + in_uv_rect.xy;

    mat3 transform = mat3(mat3x2(
        in_transform_col0_1.xy,
        in_transform_col0_1.zw,
        in_transform_col2
    ));

    gl_Position = vec4(transform * vec3(in_pos, 1.0), 1.0);
}

