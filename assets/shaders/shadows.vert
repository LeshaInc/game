ATTR vec3 in_pos;

ATTR vec4 in_model_row0;
ATTR vec4 in_model_row1;
ATTR vec4 in_model_row2;

uniform mat4 u_view_projection;

void main() {
    mat4 model = transpose(mat4(
        in_model_row0,
        in_model_row1,
        in_model_row2,
        vec4(0, 0, 0, 1)
    ));

    gl_Position = u_view_projection * model * vec4(in_pos, 1);
}
