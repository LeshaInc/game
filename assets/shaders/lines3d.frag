IN vec4 v_color;

#ifdef LEGACY_GLSL
#define OUTPUT gl_FragColor
#else
out vec4 out_color;
#define OUTPUT out_color
#endif

void main() {
    OUTPUT = v_color;
}
