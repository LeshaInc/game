IN vec4 v_color;
IN vec2 v_uv;

uniform sampler2D u_atlas;

#ifdef LEGACY_GLSL
#define OUTPUT gl_FragColor
#else
out vec4 out_color;
#define OUTPUT out_color
#endif

void main() {
    float alpha = texture2D(u_atlas, v_uv).r;
    OUTPUT = vec4(v_color.rgb, v_color.a * alpha);
}
