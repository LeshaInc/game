ATTR vec2 in_pos;

ATTR vec4 in_transform_col0_1;
ATTR vec4 in_transform_col2_shadow_offset;
ATTR vec4 in_color;
ATTR vec4 in_shadow_color;
ATTR vec4 in_border_color;
ATTR vec4 in_corner_radii;
ATTR vec4 in_size_radius_thickness;

OUT vec2 v_pos;
OUT vec2 v_size;
OUT vec4 v_color;
OUT vec4 v_shadow_color;
OUT vec2 v_shadow_offset;
OUT float v_shadow_radius;
OUT vec4 v_border_color;
OUT vec4 v_corner_radii;
OUT float v_border_thickness;

void main() {
    v_pos = in_pos;
    v_size = in_size_radius_thickness.xy;
    v_color = in_color;
    v_shadow_color = in_shadow_color;
    v_shadow_offset = in_transform_col2_shadow_offset.zw;
    v_shadow_radius = in_size_radius_thickness.z;
    v_border_color = in_border_color;
    v_corner_radii = in_corner_radii;
    v_border_thickness = in_size_radius_thickness.w;

    mat3 transform = mat3(mat3x2(
        in_transform_col0_1.xy,
        in_transform_col0_1.zw,
        in_transform_col2_shadow_offset.xy
    ));

    gl_Position = vec4(transform * vec3(in_pos, 1.0), 1.0);
}

