IN vec3 v_normal;
#ifdef SHADOW
IN vec3 v_shadow_space_pos;
#endif
IN vec2 v_tex;

#ifdef LEGACY_GLSL
#define OUTPUT gl_FragColor
#define texture texture2D
#else
out vec4 out_color;
#define OUTPUT out_color
#endif

uniform sampler2D u_shadowmap;
uniform vec2 u_shadowmap_size;

#ifdef TEXTURED
uniform sampler2D u_texture;
#endif

void main() {
    vec3 light_dir = normalize(vec3(-1.0, -1.0, 2.0));

    vec3 ambient = vec3(0.1);
    float diffuseCoeff = max(dot(v_normal, light_dir), 0.0);
    vec3 diffuse = diffuseCoeff * vec3(0.8);

#ifdef TEXTURED
    vec3 obj_color = pow(texture(u_texture, v_tex).rgb, vec3(2.4));
#else
    vec3 obj_color = vec3(1.0);
#endif

#ifdef SHADOW
    vec3 shadow_pos = v_shadow_space_pos;
    float current_depth = shadow_pos.z;

    float shadow = 0.0;

#ifdef LEGACY_GLSL
    float depth = texture(u_shadowmap, shadow_pos.xy).r; 
    shadow = float(current_depth - 0.003 > depth);
#else
    vec2 texel_size = 1.0 / u_shadowmap_size;
    for(int x = -1; x <= 1; ++x) {
        for(int y = -1; y <= 1; ++y) {
            float depth = texture(u_shadowmap, shadow_pos.xy + vec2(x, y) * texel_size).r; 
            shadow += float(current_depth - 0.003 > depth);
        }    
    }
    shadow /= 9.0;
#endif

    vec3 color = (ambient + diffuse * (1.0 - shadow * 0.6)) * obj_color;
#else
    vec3 color = (ambient + diffuse) * obj_color;
#endif

    OUTPUT = vec4(color, 1.0);
}
