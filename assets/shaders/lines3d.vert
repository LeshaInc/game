ATTR vec3 in_pos;
ATTR vec3 in_color;

OUT vec4 v_color;

void main() {
    v_color = vec4(in_color, 1.0);
    gl_Position = vec4(in_pos, 1.0);
}
