ATTR vec3 in_pos;
ATTR vec3 in_normal;
ATTR vec2 in_tex;

ATTR vec4 in_model_row0;
ATTR vec4 in_model_row1;
ATTR vec4 in_model_row2;
ATTR vec4 in_rot;
ATTR vec4 in_uv_rect;

OUT vec3 v_normal;
#ifdef SHADOW
OUT vec3 v_shadow_space_pos;
#endif
OUT vec2 v_tex;

uniform mat4 u_view_projection;
uniform mat4 u_shadow_matrix;

vec3 rotate_vec(vec4 q, vec3 v) {
    vec3 u = q.xyz;
    float s = q.w;
    return 2.0 * dot(u, v) * u + (s * s - dot(u, u)) * v + 2.0 * s * cross(u, v);
}

void main() {
    v_normal = rotate_vec(in_rot, in_normal);

    mat4 model = transpose(mat4(
        in_model_row0,
        in_model_row1,
        in_model_row2,
        vec4(0, 0, 0, 1)
    ));

    vec4 world_pos = model * vec4(in_pos, 1);

#ifdef SHADOW
    vec4 shadow_pos = u_shadow_matrix * world_pos;
    shadow_pos = shadow_pos / shadow_pos.w * 0.5 + 0.5;
    shadow_pos.z = clamp(shadow_pos.z, 0.0, 1.0);
    v_shadow_space_pos = shadow_pos.xyz;
#endif

    v_tex = in_tex * in_uv_rect.zw + in_uv_rect.xy;

    gl_Position = u_view_projection * world_pos;
}
