#version 450

layout(location = 0) in vec2 v_pos;
layout(location = 1) in vec2 v_size;
layout(location = 2) in vec4 v_color;
layout(location = 3) in vec4 v_shadow_color;
layout(location = 4) in vec2 v_shadow_offset;
layout(location = 5) in float v_shadow_radius;
layout(location = 6) in vec4 v_border_color;
layout(location = 7) in vec4 v_corner_radii;
layout(location = 8) in float v_border_thickness;

layout(location = 0) out vec4 out_color;

layout(push_constant) uniform PushConstant {
    float dpi_factor;
} pc;

float rounded_rect(vec2 p, vec2 b, vec4 r) {
    r.xy = (p.x > 0.0) ? r.xy : r.zw;
    r.x = (p.y > 0.0) ? r.x : r.y;
    vec2 q = abs(p) - b + r.x;
    return min(max(q.x, q.y), 0.0) + length(max(q, 0.0)) - r.x;
}

void main() {
    vec2 half_size = v_size / 2 - 1.0 / pc.dpi_factor;
    vec2 pos = min(vec2(v_shadow_radius), v_shadow_offset) + (v_pos - vec2(0.5)) * (v_size + vec2(2 * v_shadow_radius));
    
    float dist = rounded_rect(pos, half_size, v_corner_radii);
    float border_dist = dist + v_border_thickness;

    float mask = clamp(0.5 - dist * pc.dpi_factor, 0, 1);
    float borderMask = clamp(0.5 + border_dist * pc.dpi_factor, 0, 1) * sign(v_border_thickness);

    vec4 color = mix(v_color, v_border_color, borderMask);
    color = vec4(color.rgb, color.a * mask);

    if (v_shadow_radius > 1.0) {
        float shadow_dist = rounded_rect(pos - v_shadow_offset, half_size - v_shadow_radius, v_corner_radii);
        float shadow = 1 - smoothstep(0, 2 * v_shadow_radius, shadow_dist);
        out_color = vec4(mix(v_shadow_color.rgb, color.rgb, color.a), mix(v_shadow_color.a * shadow, 1, color.a));
    } else {
        out_color = color;
    }
}

