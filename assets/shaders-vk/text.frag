#version 450

layout(location = 0) in vec4 v_color;
layout(location = 1) in vec2 v_uv;

layout(location = 0) out vec4 out_color;

layout(set = 0, binding = 0) uniform sampler2D u_atlas;

void main() {
    float alpha = texture(u_atlas, v_uv).r;
    out_color = vec4(v_color.rgb, v_color.a * alpha);
}
