#version 450

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex;

layout(location = 3) in vec4 in_model_row0;
layout(location = 4) in vec4 in_model_row1;
layout(location = 5) in vec4 in_model_row2;
layout(location = 6) in vec4 in_rot;
layout(location = 7) in vec4 in_uv_rect;

layout(location = 0) out vec3 v_normal;
layout(location = 1) out vec3 v_shadow_space_pos;
layout(location = 2) out vec2 v_tex;

layout(std140, set = 0, binding = 0) uniform Uniforms {
    mat4 view_projection;
    mat4 shadow_matrix;
    vec2 u_shadowmap_size;
} ubo;

vec3 rotate_vec(vec4 q, vec3 v) {
    vec3 u = q.xyz;
    float s = q.w;
    return 2.0 * dot(u, v) * u + (s * s - dot(u, u)) * v + 2.0 * s * cross(u, v);
}

void main() {
    v_normal = rotate_vec(in_rot, in_normal);

    mat4 model = transpose(mat4(
        in_model_row0,
        in_model_row1,
        in_model_row2,
        vec4(0, 0, 0, 1)
    ));

    vec4 world_pos = model * vec4(in_pos, 1);
    vec4 shadow_pos = ubo.shadow_matrix * world_pos;
    shadow_pos = shadow_pos / shadow_pos.w * 0.5 + 0.5;
    shadow_pos.z = clamp(shadow_pos.z, 0.0, 1.0);
    v_shadow_space_pos = shadow_pos.xyz;
    v_tex = in_tex * in_uv_rect.zw + in_uv_rect.xy;

    gl_Position = ubo.view_projection * world_pos;
}
