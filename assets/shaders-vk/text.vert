#version 450

layout(location = 0) in vec2 in_pos;

layout(location = 1) in vec4 in_transform_col0_1;
layout(location = 2) in vec2 in_transform_col2;
layout(location = 3) in vec4 in_color;
layout(location = 4) in vec4 in_uv_rect;

layout(location = 0) out vec4 v_color;
layout(location = 1) out vec2 v_uv;

void main() {
    v_color = in_color;
    v_uv = in_pos * in_uv_rect.zw + in_uv_rect.xy;

    mat3 transform = mat3(mat3x2(
        in_transform_col0_1.xy,
        in_transform_col0_1.zw,
        in_transform_col2
    ));

    gl_Position = vec4(transform * vec3(in_pos, 1.0), 1.0);
}

