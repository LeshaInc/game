#version 450

layout(location = 0) in vec3 in_pos;

layout(location = 3) in vec4 in_model_row0;
layout(location = 4) in vec4 in_model_row1;
layout(location = 5) in vec4 in_model_row2;

layout(push_constant) uniform PC {
    layout(offset = 0) mat4 view_projection;
} pc;

void main() {
    mat4 model = transpose(mat4(
        in_model_row0,
        in_model_row1,
        in_model_row2,
        vec4(0, 0, 0, 1)
    ));

    gl_Position = pc.view_projection * model * vec4(in_pos, 1);
}
