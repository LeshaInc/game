#version 450

layout(location = 0) in vec3 v_normal;
layout(location = 1) in vec3 v_shadow_space_pos;
layout(location = 2) in vec2 v_tex;

layout(location = 0) out vec4 out_color;

layout(std140, set = 0, binding = 0) uniform Uniforms {
    mat4 view_projection;
    mat4 shadow_matrix;
    vec2 shadowmap_size;
} ubo;

layout(push_constant) uniform PushConst {
    float textured;
} pc;

layout(set = 0, binding = 1) uniform sampler2D u_shadowmap;
layout(set = 0, binding = 2) uniform sampler2D u_texture;

void main() {
    vec3 light_dir = normalize(vec3(-1.0, -1.0, 2.0));

    vec3 ambient = vec3(0.1);
    float diffuseCoeff = max(dot(v_normal, light_dir), 0.0);
    vec3 diffuse = diffuseCoeff * vec3(0.8);

    vec3 obj_color = mix(vec3(1), texture(u_texture, v_tex).rgb, pc.textured);

    vec3 shadow_pos = v_shadow_space_pos;
    float current_depth = shadow_pos.z;

    float shadow = 0.0;
    vec2 texel_size = 1.0 / ubo.shadowmap_size;
    for(int x = -1; x <= 1; ++x) {
        for(int y = -1; y <= 1; ++y) {
            float depth = texture(u_shadowmap, shadow_pos.xy + vec2(x, y) * texel_size).r; 
            shadow += float(current_depth > depth);
        }    
    }
    shadow /= 9.0;

    vec3 color = (ambient + diffuse * (1.0 - shadow * 0.6)) * obj_color;
    out_color = vec4(color, 1.0);
}
