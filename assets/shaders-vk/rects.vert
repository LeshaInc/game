#version 450

layout(location = 0) in vec2 in_pos;

layout(location = 1) in vec4 in_transform_col0_1;
layout(location = 2) in vec4 in_transform_col2_shadow_offset;
layout(location = 3) in vec4 in_color;
layout(location = 4) in vec4 in_shadow_color;
layout(location = 5) in vec4 in_border_color;
layout(location = 6) in vec4 in_corner_radii;
layout(location = 7) in vec4 in_size_radius_thickness;

layout(location = 0) out vec2 v_pos;
layout(location = 1) out vec2 v_size;
layout(location = 2) out vec4 v_color;
layout(location = 3) out vec4 v_shadow_color;
layout(location = 4) out vec2 v_shadow_offset;
layout(location = 5) out float v_shadow_radius;
layout(location = 6) out vec4 v_border_color;
layout(location = 7) out vec4 v_corner_radii;
layout(location = 8) out float v_border_thickness;

void main() {
    v_pos = in_pos;
    v_size = in_size_radius_thickness.xy;
    v_color = in_color;
    v_shadow_color = in_shadow_color;
    v_shadow_offset = in_transform_col2_shadow_offset.zw;
    v_shadow_radius = in_size_radius_thickness.z;
    v_border_color = in_border_color;
    v_corner_radii = in_corner_radii;
    v_border_thickness = in_size_radius_thickness.w;

    mat3 transform = mat3(mat3x2(
        in_transform_col0_1.xy,
        in_transform_col0_1.zw,
        in_transform_col2_shadow_offset.xy
    ));

    gl_Position = vec4(transform * vec3(in_pos, 1.0), 1.0);
}

