//! Sampler stuff

use anyhow::{Context, Result};
use fxhash::FxHashMap;
use gfx_hal::device::Device;
use gfx_hal::image::SamplerDesc;
use gfx_hal::Backend;

use crate::util::Relevant;
use crate::Gpu;

/// Sampler wrapper
pub struct Sampler<B: Backend> {
    raw: B::Sampler,
    _relevant: Relevant,
}

impl<B: Backend> Sampler<B> {
    /// Create a new sampler
    pub fn new(gpu: &Gpu<B>, desc: &SamplerDesc) -> Result<Sampler<B>> {
        let raw = unsafe { gpu.device().create_sampler(desc) }.context("Cannot create sampler")?;
        Ok(Sampler {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Get raw sampler object
    pub fn raw(&self) -> &B::Sampler {
        &self.raw
    }

    /// Destroy the sampler
    ///
    /// # Safety
    /// All commands referring to the sampler must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_sampler(self.raw);
        self._relevant.destroy();
    }
}

/// Sampler cache
///
/// Because samplers are immutable, it is possible to reuse them
pub struct SamplerCache<B: Backend> {
    samplers: FxHashMap<SamplerDesc, Sampler<B>>,
    _relevant: Relevant,
}

impl<B: Backend> Default for SamplerCache<B> {
    fn default() -> SamplerCache<B> {
        SamplerCache::new()
    }
}

impl<B: Backend> SamplerCache<B> {
    /// Create an empty sampler cache
    pub fn new() -> SamplerCache<B> {
        SamplerCache {
            samplers: FxHashMap::default(),
            _relevant: Relevant::new::<Self>(),
        }
    }

    /// Retrieve a sampler with given description, or create a new one if it doesn't exist
    pub fn get(&mut self, gpu: &Gpu<B>, desc: &SamplerDesc) -> Result<&Sampler<B>> {
        if !self.samplers.contains_key(desc) {
            let sampler = Sampler::new(gpu, desc)?;
            self.samplers.insert(desc.clone(), sampler);
        }
        Ok(&self.samplers[desc])
    }

    /// Destroy the sampler cache
    ///
    /// # Safety
    /// All commands referring to any of cached samplers must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        for (_, sampler) in self.samplers {
            sampler.destroy(gpu);
        }
        self._relevant.destroy();
    }
}
