//! Descriptor wrappers

use std::borrow::Borrow;
use std::iter;

use anyhow::{Context, Result};
use gfx_hal::device::Device;
use gfx_hal::pso::{
    DescriptorPool as _, DescriptorPoolCreateFlags, DescriptorRangeDesc, DescriptorSetLayoutBinding,
};
use gfx_hal::Backend;

use crate::util::Relevant;
use crate::Gpu;

/// Decriptor pool wrapper
pub struct DescriptorPool<B: Backend> {
    raw: B::DescriptorPool,
    free_individual: bool,
    _relevant: Relevant,
}

impl<B: Backend> DescriptorPool<B> {
    /// Create a new descriptor pool
    pub fn new<I>(
        gpu: &Gpu<B>,
        max_sets: usize,
        ranges: I,
        free_individual: bool,
    ) -> Result<DescriptorPool<B>>
    where
        I: IntoIterator,
        I::Item: Borrow<DescriptorRangeDesc>,
    {
        let flags = if free_individual {
            DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET
        } else {
            DescriptorPoolCreateFlags::empty()
        };

        let raw = unsafe { gpu.device().create_descriptor_pool(max_sets, ranges, flags) }
            .context("Cannot create descriptor pool")?;

        Ok(DescriptorPool {
            raw,
            free_individual,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Get raw descriptor pool
    pub fn raw(&self) -> &B::DescriptorPool {
        &self.raw
    }

    /// Allocate a single descriptor set with the specified layout
    pub fn allocate(&mut self, layout: &DescriptorSetLayout<B>) -> Result<DescriptorSet<B>> {
        let raw = unsafe { self.raw.allocate_set(layout.raw()) }?;
        Ok(DescriptorSet { raw })
    }

    /// Free a single descriptor.
    ///
    /// # Panics
    /// If the pool was created without the `free_individual` flag
    ///
    /// # Safety
    /// All commands referring to the set must have completed execution
    pub unsafe fn free(&mut self, set: DescriptorSet<B>) {
        assert!(self.free_individual);
        self.raw.free_sets(iter::once(set.raw));
    }

    /// Destroy the desrciptor pool. All allocated sets are implicitly freed and invalidated.
    ///
    /// # Safety
    /// Allocated descriptor sets should not be in use.
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_descriptor_pool(self.raw);
        self._relevant.destroy();
    }
}

/// Descriptor set layout wrapper
pub struct DescriptorSetLayout<B: Backend> {
    raw: B::DescriptorSetLayout,
    _relevant: Relevant,
}

impl<B: Backend> DescriptorSetLayout<B> {
    /// Create a new descriptor set layout
    pub fn new<I>(gpu: &Gpu<B>, bindings: I) -> Result<DescriptorSetLayout<B>>
    where
        I: IntoIterator,
        I::Item: Borrow<DescriptorSetLayoutBinding>,
    {
        let samplers = iter::empty::<B::Sampler>();
        let raw = unsafe {
            gpu.device()
                .create_descriptor_set_layout(bindings, samplers)
        }
        .context("Cannot create descriptor set layout")?;

        Ok(DescriptorSetLayout {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Get raw descriptor set layout
    pub fn raw(&self) -> &B::DescriptorSetLayout {
        &self.raw
    }

    /// Set descriptor set layout name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        unsafe {
            gpu.device()
                .set_descriptor_set_layout_name(&mut self.raw, name)
        }
    }

    /// Destroy the desrciptor set layout
    pub fn destroy(self, gpu: &Gpu<B>) {
        unsafe { gpu.device().destroy_descriptor_set_layout(self.raw) };
        self._relevant.destroy();
    }
}

/// Descriptor set wrapper
///
/// Creation and destruction is handled by `DescriptorPool`
pub struct DescriptorSet<B: Backend> {
    raw: B::DescriptorSet,
}

impl<B: Backend> DescriptorSet<B> {
    /// Get raw descriptor set
    pub fn raw(&self) -> &B::DescriptorSet {
        &self.raw
    }

    /// Set descriptor set name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        unsafe { gpu.device().set_descriptor_set_name(&mut self.raw, name) }
    }
}
