//! Renderer

#![warn(missing_docs)]

pub mod buffer;
pub mod descriptor;
pub mod frame;
pub mod image;
pub mod pass;
pub mod pipeline;
pub mod sampler;
pub mod util;

mod command;
mod gpu;
mod shader;
mod storage;

pub use self::command::CommandBufferCache;
pub use self::gpu::Gpu;
pub use self::shader::ShaderModule;
pub use self::storage::Storage;

use std::borrow::Borrow;
use std::iter;
use std::mem::ManuallyDrop;
use std::ops::Drop;
use std::path::Path;

use anyhow::{anyhow, Context, Result};
use gfx_hal::adapter::PhysicalDevice;
use gfx_hal::command::{CommandBuffer as _, CommandBufferFlags, CommandBufferInheritanceInfo};
use gfx_hal::device::Device;
use gfx_hal::format::{Aspects, Format};
use gfx_hal::image::{Extent, SubresourceRange, Usage as ImageUsage, ViewKind};
use gfx_hal::pso::{
    BufferDescriptorFormat, BufferDescriptorType, DescriptorRangeDesc, DescriptorType,
    ImageDescriptorType,
};
use gfx_hal::pso::{Rect, Viewport};
use gfx_hal::queue::{CommandQueue, Submission};
use gfx_hal::window::{
    AcquireError, Extent2D, PresentError, PresentationSurface, Surface, SwapchainConfig,
};
use gfx_hal::{Backend, Instance, Limits};
use legion::systems::CommandBuffer;
use legion::{Entity, IntoQuery, Resources, World, Write};
use winit::dpi::{LogicalSize, PhysicalSize};
use winit::event_loop::EventLoop;
use winit::window::{Window, WindowBuilder};

use crate::descriptor::DescriptorPool;
use crate::frame::{FrameCleaner, FrameIndex, FrameRingbuffer};
use crate::image::{Image, ImageInfo, ImageView};
use crate::pass::{Render2DPass, Render3DPass};
use crate::sampler::SamplerCache;
use game_core::assets::Assets;
use game_core::graphics::{
    CasterDrawList3D, DrawList2D, DrawList3D, GraphicsBackend, GraphicsSettings, WindowRef,
};

struct PerFrame<B: Backend> {
    command_buffers: CommandBufferCache<B>,
    cleaner: FrameCleaner<B>,
    image_ready_semaphore: B::Semaphore,
    frame_completed_fence: B::Fence,
    depth_image: Image<B>,
    depth_image_view: ImageView<B>,
}

/// Renderer
pub struct Graphics<B: Backend> {
    gpu: Gpu<B>,
    desc_pool: ManuallyDrop<DescriptorPool<B>>,
    sampler_cache: ManuallyDrop<SamplerCache<B>>,
    storage: ManuallyDrop<Storage<B>>,

    frame_data: ManuallyDrop<FrameRingbuffer<PerFrame<B>>>,
    cur_frame: FrameIndex,
    frames_in_flight: usize,

    window: Window,
    surface: ManuallyDrop<B::Surface>,
    swapchain_config: SwapchainConfig,
    resolution: PhysicalSize<u32>,
    settings: GraphicsSettings,
    max_settings: GraphicsSettings,
    new_settings: Option<GraphicsSettings>,

    render3d_pass: ManuallyDrop<Render3DPass<B>>,
    render2d_pass: ManuallyDrop<Render2DPass<B>>,
}

/// Context passed to renderers during initialization
pub struct InitializationCtx<'a, B: Backend> {
    /// GPU
    pub gpu: &'a Gpu<B>,
    /// Path to assets
    pub assets_path: &'a Path,
    /// Graphics settings
    pub settings: &'a GraphicsSettings,
    /// Descriptor pool
    pub desc_pool: &'a mut DescriptorPool<B>,
    /// Sampler cache
    pub sampler_cache: &'a mut SamplerCache<B>,
    /// Command buffer for initialization commands
    pub cbuf: &'a mut B::CommandBuffer,
    /// Cleaner
    pub cleaner: &'a mut FrameCleaner<B>,
    /// Number of frames in flight
    pub frames_in_flight: usize,
}

/// Context passed to renderers during rendering
pub struct RenderingCtx<'a, B: Backend> {
    /// GPU
    pub gpu: &'a Gpu<B>,
    /// Frame cleaner
    pub frame_cleaner: &'a mut FrameCleaner<B>,
    /// Sampler cache
    pub sampler_cache: &'a mut SamplerCache<B>,
    /// Physical resolution
    pub resolution: PhysicalSize<u32>,
    /// Logical resolution
    pub logical_resolution: LogicalSize<f32>,
    /// DPI factor
    pub dpi_factor: f32,
    /// Command buffer
    pub cbuf: &'a mut B::CommandBuffer,
    /// Resource storage
    pub storage: &'a mut Storage<B>,
    /// ECS resources
    pub resources: &'a mut Resources,
    /// Current frame index
    pub frame: FrameIndex,
}

impl<B: Backend> RenderingCtx<'_, B> {
    /// Get resolution as an `Extent`
    pub fn extent(&self) -> Extent {
        Extent {
            width: self.resolution.width,
            height: self.resolution.height,
            depth: 1,
        }
    }

    /// Get resolution as a `Viewport` (default depth range)
    pub fn viewport(&self) -> Viewport {
        Viewport {
            rect: self.rect(),
            depth: 0.0..1.0,
        }
    }

    /// Get resolution as a `Rect`
    pub fn rect(&self) -> Rect {
        Rect {
            x: 0,
            y: 0,
            w: self.resolution.width as _,
            h: self.resolution.height as _,
        }
    }
}

/// Context passed to renderers during teardown
pub struct TeardownCtx<'a, B: Backend> {
    /// GPU
    pub gpu: &'a Gpu<B>,
    /// Descriptor pool
    pub desc_pool: &'a mut DescriptorPool<B>,
}

fn create_depth_image<B: Backend>(
    gpu: &Gpu<B>,
    res: PhysicalSize<u32>,
) -> Result<(Image<B>, ImageView<B>)> {
    let mut image = Image::new(
        gpu,
        &ImageInfo::new_2d(res.width, res.height)
            .with_format(Format::D16Unorm)
            .with_usage(ImageUsage::DEPTH_STENCIL_ATTACHMENT),
    )?;

    image.set_name(gpu, "depth");

    let image_view = image.create_view(
        gpu,
        ViewKind::D2,
        Format::D16Unorm,
        SubresourceRange {
            aspects: Aspects::DEPTH,
            levels: 0..1,
            layers: 0..1,
        },
    )?;

    Ok((image, image_view))
}

fn get_max_settings(limits: Limits) -> GraphicsSettings {
    GraphicsSettings {
        msaa: 1,
        shadows: true,
        shadow_resolution: limits.max_image_2d_size,
    }
}

impl<B: Backend> Graphics<B> {
    /// Initialize the renderer
    pub fn new(
        event_loop: &EventLoop<()>,
        assets_path: &Path,
        settings: GraphicsSettings,
    ) -> Result<Graphics<B>> {
        let instance =
            B::Instance::create("game", 1).map_err(|_| anyhow!("Backend is not supported"))?;
        let window = WindowBuilder::new()
            .build(event_loop)
            .context("Cannot build window")?;
        let mut surface =
            unsafe { instance.create_surface(&window) }.context("Cannot create surface")?;
        let gpu = Gpu::new(instance, &surface)?;

        let resolution = window.inner_size();

        let surface_caps = surface.capabilities(gpu.physical_device());
        let swapchain_config = SwapchainConfig::from_caps(
            &surface_caps,
            Format::Bgra8Srgb,
            Extent2D {
                width: resolution.width,
                height: resolution.height,
            },
        );

        unsafe {
            surface
                .configure_swapchain(gpu.device(), swapchain_config.clone())
                .context("Cannot configure swapchain")?;
        }

        let frames_in_flight = swapchain_config.image_count as usize;
        let ranges = vec![
            DescriptorRangeDesc {
                ty: DescriptorType::Buffer {
                    ty: BufferDescriptorType::Uniform,
                    format: BufferDescriptorFormat::Structured {
                        dynamic_offset: false,
                    },
                },
                count: frames_in_flight,
            },
            DescriptorRangeDesc {
                ty: DescriptorType::Image {
                    ty: ImageDescriptorType::Sampled { with_sampler: true },
                },
                count: 3 * frames_in_flight,
            },
        ];

        let mut desc_pool = DescriptorPool::new(&gpu, 2 * frames_in_flight, ranges, false)?;
        let mut sampler_cache = SamplerCache::new();
        let storage = Storage::new(&gpu, frames_in_flight)?;

        let frame_data = FrameRingbuffer::new(frames_in_flight, |_| {
            let device: &B::Device = gpu.device();
            let command_buffers = CommandBufferCache::new(&gpu)?;
            let image_ready_semaphore = device.create_semaphore()?;
            let frame_completed_fence = device.create_fence(true)?;
            let (depth_image, depth_image_view) = create_depth_image(&gpu, resolution)?;

            Ok(PerFrame {
                command_buffers,
                cleaner: FrameCleaner::new(),
                image_ready_semaphore,
                frame_completed_fence,
                depth_image,
                depth_image_view,
            })
        })?;

        let mut cleaner = FrameCleaner::new();
        let mut command_buffers = CommandBufferCache::new(&gpu)?;
        let mut cbuf: B::CommandBuffer = command_buffers.get_primary();

        unsafe {
            cbuf.begin(
                CommandBufferFlags::ONE_TIME_SUBMIT,
                CommandBufferInheritanceInfo::default(),
            );
        }

        let mut ctx = InitializationCtx {
            gpu: &gpu,
            assets_path,
            settings: &settings,
            desc_pool: &mut desc_pool,
            sampler_cache: &mut sampler_cache,
            cbuf: &mut cbuf,
            cleaner: &mut cleaner,
            frames_in_flight,
        };

        let render3d_pass = Render3DPass::new(&mut ctx)?;
        let render2d_pass = Render2DPass::new(&mut ctx)?;

        drop(ctx);

        unsafe { cbuf.finish() };
        unsafe {
            let mut queue = gpu.queue();
            let queue: &mut B::CommandQueue = &mut queue;
            queue.submit(
                Submission {
                    command_buffers: iter::once(&cbuf),
                    wait_semaphores: None,
                    signal_semaphores: iter::empty::<&B::Semaphore>(),
                },
                None,
            );
        }

        let device: &B::Device = gpu.device();
        device.wait_idle()?;

        command_buffers.return_primary(cbuf);
        unsafe { command_buffers.destroy(&gpu) };
        unsafe { cleaner.cleanup(&gpu) };

        let physical_device: &B::PhysicalDevice = gpu.physical_device();

        Ok(Graphics {
            max_settings: get_max_settings(physical_device.limits()),
            gpu,
            desc_pool: ManuallyDrop::new(desc_pool),
            sampler_cache: ManuallyDrop::new(sampler_cache),
            storage: ManuallyDrop::new(storage),

            frame_data: ManuallyDrop::new(frame_data),
            cur_frame: FrameIndex::default(),
            frames_in_flight,

            window,
            surface: ManuallyDrop::new(surface),
            swapchain_config,
            resolution,
            settings,
            new_settings: None,

            render3d_pass: ManuallyDrop::new(render3d_pass),
            render2d_pass: ManuallyDrop::new(render2d_pass),
        })
    }

    fn resize(&mut self, mut new_resolution: PhysicalSize<u32>) -> Result<()> {
        let caps = self.surface.capabilities(self.gpu.physical_device());
        new_resolution.width = new_resolution
            .width
            .max(caps.extents.start().width)
            .min(caps.extents.end().width);
        new_resolution.height = new_resolution
            .height
            .max(caps.extents.start().height)
            .min(caps.extents.end().height);

        self.swapchain_config.extent = Extent2D {
            width: new_resolution.width,
            height: new_resolution.height,
        };

        unsafe {
            self.surface
                .configure_swapchain(self.gpu.device(), self.swapchain_config.clone())?;
        }

        for frame in self.frame_data.iter_mut() {
            let (depth_image, depth_image_view) = create_depth_image(&self.gpu, new_resolution)?;
            let cleaner = &mut frame.cleaner;
            cleaner.add_image(std::mem::replace(&mut frame.depth_image, depth_image));
            cleaner.add_image_view(std::mem::replace(
                &mut frame.depth_image_view,
                depth_image_view,
            ));
        }

        self.resolution = new_resolution;
        Ok(())
    }

    fn _update_settings(&mut self, new: GraphicsSettings) -> Result<()> {
        self.render3d_pass.update_settings(
            &self.settings,
            &new,
            &self.gpu,
            &mut self.frame_data[self.cur_frame].cleaner,
        )?;

        self.settings = new;

        Ok(())
    }
}

impl<B: Backend> GraphicsBackend for Graphics<B> {
    fn window(&self) -> WindowRef<'_> {
        WindowRef::Pure(&self.window)
    }

    fn max_settings(&self) -> &GraphicsSettings {
        &self.max_settings
    }

    fn render(
        &mut self,
        world: &mut World,
        resources: &mut Resources,
        ecs_cbuf: &mut CommandBuffer,
    ) -> Result<()> {
        let new_resolution = self.window.inner_size();

        if new_resolution != self.resolution {
            self.resize(new_resolution)?;
        }

        let frame_data = &mut self.frame_data[self.cur_frame];

        unsafe {
            self.gpu
                .device()
                .wait_for_fence(&frame_data.frame_completed_fence, !0)
                .context("Error while waiting for fence")?;
            self.gpu
                .device()
                .reset_fence(&frame_data.frame_completed_fence)
                .context("Cannot reset fence")?;
            frame_data.cleaner.cleanup(&self.gpu);
        }

        if let Some(settings) = self.new_settings.take() {
            self._update_settings(settings)?;
        }

        let frame_data = &mut self.frame_data[self.cur_frame];

        let res = unsafe { self.surface.acquire_image(!0) };

        let color_view = match res {
            Ok((v, _)) => v,
            Err(AcquireError::OutOfDate) => {
                self.resize(self.window.inner_size())?;
                return self.render(world, resources, ecs_cbuf);
            }
            Err(e) => return Err(e.into()),
        };

        let depth_view = &frame_data.depth_image_view;

        unsafe { frame_data.command_buffers.reset() };
        let mut cbuf = frame_data.command_buffers.get_primary();

        unsafe {
            cbuf.begin(
                CommandBufferFlags::ONE_TIME_SUBMIT,
                CommandBufferInheritanceInfo::default(),
            );
        }

        let dpi_factor = self.window.scale_factor();
        let mut ctx = RenderingCtx {
            gpu: &self.gpu,
            frame_cleaner: &mut frame_data.cleaner,
            sampler_cache: &mut self.sampler_cache,
            resolution: self.resolution,
            logical_resolution: self.resolution.to_logical(dpi_factor),
            dpi_factor: dpi_factor as f32,
            cbuf: &mut cbuf,
            storage: &mut self.storage,
            resources,
            frame: self.cur_frame,
        };

        let mut assets = ctx.resources.get_mut::<Assets>().unwrap();
        ctx.storage
            .maintain(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf, &mut assets)?;
        drop(assets);

        let mut drawlist3d = DrawList3D::default();
        let mut query = <(Entity, Write<DrawList3D>)>::query();
        for (&entity, drawlist) in query.iter_mut(world) {
            drawlist3d.append(drawlist);
            ecs_cbuf.remove(entity);
        }

        let mut casters_drawlist3d = DrawList3D::default();
        let mut query = <(Entity, Write<CasterDrawList3D>)>::query();
        for (&entity, drawlist) in query.iter_mut(world) {
            casters_drawlist3d.append(&mut drawlist.0);
            ecs_cbuf.remove(entity);
        }

        let mut drawlist2d = DrawList2D::default();
        let mut query = <(Entity, Write<DrawList2D>)>::query();
        for (&entity, drawlist) in query.iter_mut(world) {
            drawlist2d.append(drawlist);
            ecs_cbuf.remove(entity);
        }

        self.render3d_pass.render(
            &mut ctx,
            color_view.borrow(),
            depth_view,
            drawlist3d,
            casters_drawlist3d,
        )?;

        self.render2d_pass
            .render(&mut ctx, color_view.borrow(), &drawlist2d)?;

        drop(ctx);

        let semaphore = &frame_data.image_ready_semaphore;
        let mut queue = self.gpu.queue();

        let res = unsafe {
            cbuf.finish();
            queue.submit(
                Submission {
                    command_buffers: iter::once(&cbuf),
                    wait_semaphores: None,
                    signal_semaphores: iter::once(semaphore),
                },
                Some(&frame_data.frame_completed_fence),
            );
            queue.present_surface(&mut self.surface, color_view, Some(semaphore))
        };

        frame_data.command_buffers.return_primary(cbuf);
        drop(queue);

        match res {
            Err(PresentError::OutOfDate) => self.resize(self.window.inner_size())?, // TODO: don't drop the frame
            Err(e) => return Err(e.into()),
            _ => {}
        }

        self.cur_frame = self.cur_frame.next(self.frames_in_flight);

        Ok(())
    }

    fn update_settings(&mut self, new: GraphicsSettings) -> Result<()> {
        self.new_settings = Some(new);
        Ok(())
    }
}

impl<B: Backend> Drop for Graphics<B> {
    fn drop(&mut self) {
        let device = self.gpu.device();
        let instance = self.gpu.instance();
        device.wait_idle().unwrap();

        let mut ctx = TeardownCtx {
            gpu: &self.gpu,
            desc_pool: &mut self.desc_pool,
        };

        unsafe {
            ManuallyDrop::take(&mut self.render3d_pass).destroy(&mut ctx);
            ManuallyDrop::take(&mut self.render2d_pass).destroy(&mut ctx);
            self.surface.unconfigure_swapchain(device);
            instance.destroy_surface(ManuallyDrop::take(&mut self.surface));
            ManuallyDrop::take(&mut self.sampler_cache).destroy(&self.gpu);
            ManuallyDrop::take(&mut self.storage).destroy(&self.gpu);

            ManuallyDrop::take(&mut self.frame_data).destroy(|mut per_frame| {
                per_frame.command_buffers.destroy(&self.gpu);
                per_frame.cleaner.cleanup(&self.gpu);
                device.destroy_fence(per_frame.frame_completed_fence);
                device.destroy_semaphore(per_frame.image_ready_semaphore);
                per_frame.depth_image.destroy(&self.gpu);
                per_frame.depth_image_view.destroy(&self.gpu);
            });
        }
    }
}
