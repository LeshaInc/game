use std::cell::Cell;

use anyhow::Result;
use fxhash::FxHashMap;
use gfx_hal::buffer::Usage as BufferUsage;
use gfx_hal::format::Format;
use gfx_hal::image::Usage as ImageUsage;
use gfx_hal::Backend;

use crate::buffer::{Buffer, BufferUploader};
use crate::frame::{FrameCleaner, FrameIndex};
use crate::gpu::Gpu;
use crate::image::ImageAtlas;
use crate::util::inspect_slice_as_bytes;
use game_core::assets::{Assets, Id};
use game_core::graphics::{Mesh, Texture};

/// Mesh entry
pub struct MeshEntry<B: Backend> {
    /// Vertex buffer
    pub vbuf: Buffer<B>,
    /// Index buffer (32 bit)
    pub ibuf: Buffer<B>,
    /// If false, a pipeline barrier is needed to make both buffers visible
    /// - Src state: `PipelineStage::TRANSFER, Access::TRANSFER_WRITE`
    pub is_visible: Cell<bool>, // -_-
}

/// Resource storage
pub struct Storage<B: Backend> {
    /// Texture atlas
    pub atlas: ImageAtlas<B, Id<Texture>>,
    /// Meshes
    pub meshes: FxHashMap<Id<Mesh>, MeshEntry<B>>,
    buffer_uploader: BufferUploader<B>,
}

impl<B: Backend> Storage<B> {
    /// Create a new resource storage
    pub fn new(gpu: &Gpu<B>, frames_in_flight: usize) -> Result<Storage<B>> {
        let mut atlas = ImageAtlas::new(
            gpu,
            frames_in_flight,
            Format::Rgba8Srgb,
            ImageUsage::SAMPLED,
        )?;
        atlas.set_name(gpu, "tex-atlas");
        Ok(Storage {
            atlas,
            meshes: FxHashMap::default(),
            buffer_uploader: BufferUploader::new(gpu)?,
        })
    }

    /// Upload data on GPU and perform cleanup
    ///
    /// Note: you need to call `flush` on the atlas manually
    pub fn maintain(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        cbuf: &mut B::CommandBuffer,
        assets: &mut Assets,
    ) -> Result<()> {
        self.maintain_meshes(gpu, cleaner, cbuf, assets)?;
        self.maintain_textures(gpu, cleaner, frame, assets)?;
        Ok(())
    }

    fn maintain_meshes(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        cbuf: &mut B::CommandBuffer,
        assets: &mut Assets,
    ) -> Result<()> {
        assets.cleanup_with(|id, _, _| {
            let mesh = match self.meshes.remove(&id) {
                Some(v) => v,
                None => return,
            };

            cleaner.add_buffer(mesh.vbuf);
            cleaner.add_buffer(mesh.ibuf);
        });

        for (handle, mesh) in assets.iter_mut::<Mesh>() {
            let data = match mesh.data.take() {
                Some(v) => v,
                None => continue,
            };

            let usage = BufferUsage::VERTEX;
            let bytes = inspect_slice_as_bytes(&data.vertices);
            let mut vbuf = self
                .buffer_uploader
                .upload(gpu, cleaner, cbuf, usage, bytes)?;
            vbuf.set_name(gpu, &format!("mesh-{:?}-vbuf", handle.id().inner));

            let usage = BufferUsage::INDEX;
            let bytes = inspect_slice_as_bytes(&data.indices);
            let mut ibuf = self
                .buffer_uploader
                .upload(gpu, cleaner, cbuf, usage, bytes)?;
            ibuf.set_name(gpu, &format!("mesh-{:?}-ibuf", handle.id().inner));

            self.meshes.insert(
                handle.id(),
                MeshEntry {
                    vbuf,
                    ibuf,
                    is_visible: Cell::new(false),
                },
            );
        }

        Ok(())
    }

    fn maintain_textures(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        assets: &mut Assets,
    ) -> Result<()> {
        assets.cleanup_with(|id, _, _| {
            self.atlas.deallocate(&id);
        });

        for (handle, texture) in assets.iter_mut::<Texture>() {
            let data = match texture.data.take() {
                Some(v) => v,
                None => continue,
            };

            let rgba = data.flipv().into_rgba();
            let (w, h) = rgba.dimensions();
            let samples = rgba.as_flat_samples();
            let bytes = samples.as_slice();
            self.atlas
                .add(gpu, cleaner, frame, handle.id(), (w, h), |out| {
                    out.copy_from_slice(bytes)
                })?;
        }

        Ok(())
    }

    /// Destroy the pipeline
    ///
    /// # Safety
    /// Underlying data should not be in use
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        self.atlas.destroy(gpu);
        self.buffer_uploader.destroy(gpu);
        for (_, mesh) in self.meshes {
            mesh.vbuf.destroy(gpu);
            mesh.ibuf.destroy(gpu);
        }
    }
}
