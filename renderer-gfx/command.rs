use std::collections::VecDeque;

use anyhow::{Context, Result};
use gfx_hal::command::Level;
use gfx_hal::device::Device;
use gfx_hal::pool::{CommandPool, CommandPoolCreateFlags};
use gfx_hal::Backend;

use crate::gpu::Gpu;
use crate::util::Relevant;

/// Command buffer cache (reset pool once per frame, reuse old buffers)
pub struct CommandBufferCache<B: Backend> {
    pool: B::CommandPool,
    primary_buffers: VecDeque<B::CommandBuffer>,
    secondary_buffers: VecDeque<B::CommandBuffer>,
    _relevant: Relevant,
}

impl<B: Backend> CommandBufferCache<B> {
    /// Create a new command buffer cache
    pub fn new(gpu: &Gpu<B>) -> Result<CommandBufferCache<B>> {
        let pool = unsafe {
            gpu.device()
                .create_command_pool(gpu.queue_family_id(), CommandPoolCreateFlags::empty())
                .context("Cannot create command pool")?
        };

        Ok(CommandBufferCache {
            pool,
            primary_buffers: VecDeque::new(),
            secondary_buffers: VecDeque::new(),
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Create a new primary command buffer or reuse an old one
    pub fn get_primary(&mut self) -> B::CommandBuffer {
        self.primary_buffers
            .pop_back()
            .unwrap_or_else(|| unsafe { self.pool.allocate_one(Level::Primary) })
    }

    /// Return a previously allocated primary command buffer
    pub fn return_primary(&mut self, buf: B::CommandBuffer) {
        self.primary_buffers.push_front(buf);
    }

    /// Create a new secondary command buffer or reuse an old one
    pub fn get_secondary(&mut self) -> B::CommandBuffer {
        self.secondary_buffers
            .pop_back()
            .unwrap_or_else(|| unsafe { self.pool.allocate_one(Level::Secondary) })
    }

    /// Return a previously allocated secondary command buffer
    pub fn return_secondary(&mut self, buf: B::CommandBuffer) {
        self.secondary_buffers.push_front(buf);
    }

    /// Reset all the buffers
    ///
    /// # Safety
    /// The command buffers must have completed execution
    pub unsafe fn reset(&mut self) {
        self.pool.reset(false);
    }

    /// Destroy the command buffer cache
    ///
    /// # Safety
    /// The command buffers must have completed execution
    pub unsafe fn destroy(mut self, gpu: &Gpu<B>) {
        self.reset();
        self.pool.free(self.primary_buffers);
        self.pool.free(self.secondary_buffers);
        gpu.device().destroy_command_pool(self.pool);
        self._relevant.destroy();
    }
}
