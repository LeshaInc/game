/// Marker for trivially copyable types without invalid bit patterns
///
/// It is safe to implement this trait for integers, floats (but not booleans), and any combination
/// of them. Padding bytes inside structs are considered unimportant, however you should prefer to avoid
/// them and always use `#[repr(C)]`
pub unsafe trait Pod: Copy {}
