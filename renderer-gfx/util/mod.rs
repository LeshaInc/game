//! Various utilities

mod pod;
mod relevant;

pub use self::pod::Pod;
pub use self::relevant::Relevant;

use nalgebra::Matrix4;

/// Inspect arbitrary slice as a byte slice
pub fn inspect_slice_as_bytes<T>(slice: &[T]) -> &[u8] {
    // inspecting bytes is always safe
    unsafe {
        std::slice::from_raw_parts(
            slice.as_ptr() as *const u8,
            slice.len() * std::mem::size_of::<T>(),
        )
    }
}

/// Convert OpenGL NDC transformation matrix to Vulkan NDC space (flip y, normalize z to 0..1)
pub fn gl_ndc_to_vk_ndc(mat: &Matrix4<f32>) -> Matrix4<f32> {
    #[rustfmt::skip]
    let fixup = Matrix4::new(
        1.0,  0.0, 0.0, 0.0,
        0.0, -1.0, 0.0, 0.0,
        0.0,  0.0, 0.5, 0.5,
        0.0,  0.0, 0.0, 1.0,
    );
    fixup * mat
}
