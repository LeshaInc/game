/// Marker for values that cannot be dropped. Works only with `debug_assertions` on
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Relevant {
    #[cfg(debug_assertions)]
    type_name: &'static str,
}

impl Relevant {
    /// Create a new relevant marker
    #[cfg(debug_assertions)]
    pub fn new<T: 'static>() -> Relevant {
        Relevant {
            type_name: std::any::type_name::<T>(),
        }
    }

    /// Create a new relevant marker
    #[cfg(not(debug_assertions))]
    pub fn new<T: 'static>() -> Relevant {
        Relevant {}
    }

    /// Destroy the relevant
    pub fn destroy(self) {
        std::mem::forget(self);
    }
}

#[cfg(debug_assertions)]
impl std::ops::Drop for Relevant {
    fn drop(&mut self) {
        log::error!("Values of type {} cannot be dropped", self.type_name);
    }
}
