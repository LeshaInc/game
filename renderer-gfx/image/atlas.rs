use std::hash::Hash;
use std::mem;

use anyhow::Result;
use fxhash::FxHashMap;
use gfx_hal::buffer::Usage as BufferUsage;
use gfx_hal::command::{BufferImageCopy, CommandBuffer, ImageCopy};
use gfx_hal::format::{Aspects, Format};
use gfx_hal::image::{
    Access, Extent, Layout, Offset, SubresourceLayers, SubresourceRange, Usage, ViewKind,
};
use gfx_hal::memory::{Barrier, Dependencies};
use gfx_hal::pso::PipelineStage;
use gfx_hal::Backend;
use guillotiere::{AllocId, AtlasAllocator, Rectangle, Size};

use super::{Image, ImageInfo, ImageView};
use crate::buffer::DynamicBuffer;
use crate::frame::{FrameCleaner, FrameIndex, FrameRingbuffer};
use crate::gpu::Gpu;

#[derive(Debug)]
struct AllocatedEntry {
    id: AllocId,
    rect: Rectangle,
    is_live: bool,
}

#[derive(Debug)]
struct NewEntry<K> {
    key: K,
    size: (u32, u32),
    data_offset: u64,
}

/// Dynamic texture atlas
pub struct ImageAtlas<B: Backend, K> {
    image: Image<B>,
    image_view: ImageView<B>,
    format: Format,
    usage: Usage,
    allocator: AtlasAllocator,
    allocated: FxHashMap<K, AllocatedEntry>,
    new_entries: Vec<NewEntry<K>>,
    staging: FrameRingbuffer<DynamicBuffer<B>>,
    name: Option<Box<str>>,
}

const SUBRS_RANGE: SubresourceRange = SubresourceRange {
    aspects: Aspects::COLOR,
    levels: 0..1,
    layers: 0..1,
};

impl<B: Backend, K: Hash + Eq> ImageAtlas<B, K> {
    /// Create a new image atlas
    pub fn new(
        gpu: &Gpu<B>,
        frames_in_flight: usize,
        format: Format,
        mut usage: Usage,
    ) -> Result<ImageAtlas<B, K>> {
        usage |= Usage::TRANSFER_SRC | Usage::TRANSFER_DST;

        let image = Image::new(
            gpu,
            &ImageInfo::new_2d(256, 256)
                .with_format(format)
                .with_usage(usage),
        )?;

        let image_view = image.create_view(gpu, ViewKind::D2, format, SUBRS_RANGE.clone())?;

        Ok(ImageAtlas {
            image,
            image_view,
            format,
            usage,
            allocator: AtlasAllocator::new(Size::new(256, 256)),
            allocated: FxHashMap::default(),
            new_entries: Vec::new(),
            staging: FrameRingbuffer::new(frames_in_flight, |_| {
                DynamicBuffer::new(gpu, BufferUsage::TRANSFER_SRC)
            })?,
            name: None,
        })
    }

    /// Set image name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        self.image.set_name(gpu, name);
        for (i, staging) in self.staging.iter_mut().enumerate() {
            staging.set_name(gpu, &format!("{}-staging-{}", name, i));
        }
        self.name = Some(String::from(name).into());
    }

    /// Get the underlying image
    pub fn image(&self) -> &Image<B> {
        &self.image
    }

    /// Get the underlying image view
    pub fn image_view(&self) -> &ImageView<B> {
        &self.image_view
    }

    /// Get rect of the entry, or None if it doesn't exist
    pub fn get_rect(&self, key: &K) -> Option<Rectangle> {
        self.allocated.get(key).map(|e| e.rect)
    }

    /// Get rect of the entry in normalized floating coordinates, or None if it doesn't exist
    pub fn get_rect_f32(&self, key: &K) -> Option<[f32; 4]> {
        let rect = self.get_rect(key)?;
        let size = self.allocator.size();
        let size = (size.width as f32, size.height as f32);
        Some([
            (rect.min.x as f32) / size.0,
            (rect.min.y as f32) / size.1,
            (rect.width() as f32) / size.0,
            (rect.height() as f32) / size.1,
        ])
    }

    /// Add an entry to the image atlas. If it is already uploaded, mark it as live, otherwise
    /// schedule it for uploading. The data is obtained by calling the filler closure
    pub fn add(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        key: K,
        size: (u32, u32),
        filler: impl FnOnce(&mut [u8]),
    ) -> Result<()> {
        match self.allocated.get_mut(&key) {
            Some(e) if e.rect.size() == Size::new(size.0 as _, size.1 as _) => {
                e.is_live = true;
                return Ok(());
            }
            Some(e) => {
                self.allocator.deallocate(e.id);
                self.allocated.remove(&key);
            }
            _ => {}
        }

        let len = (size.0 as u64) * (size.1 as u64) * (self.format.surface_desc().bits / 8) as u64;

        self.staging[frame].align(gpu, cleaner, &[0; 4])?; // offset should be a multiple of 4
        let data_offset = self.staging[frame].size();
        self.staging[frame].extend_raw(gpu, cleaner, len, filler)?;

        self.new_entries.push(NewEntry {
            key,
            size,
            data_offset,
        });

        Ok(())
    }

    /// Deallocate one allocated entry
    pub fn deallocate(&mut self, key: &K) {
        let id = match self.allocated.get(key) {
            Some(entry) => entry.id,
            _ => return,
        };
        self.allocator.deallocate(id);
    }

    /// Deallocate dead entries
    pub fn deallocate_dead(&mut self) {
        let allocator = &mut self.allocator;
        self.allocated.retain(|_, entry| {
            if !entry.is_live {
                allocator.deallocate(entry.id);
            }
            entry.is_live
        });

        // mark all as dead
        for entry in self.allocated.values_mut() {
            entry.is_live = false;
        }
    }

    /// True if the `flush` call is necessary to make updates visible
    pub fn flush_needed(&self) -> bool {
        !self.new_entries.is_empty()
    }

    /// Pack all new entries and upload them to the texture atlas, growing it if necessary
    ///
    /// # Synchronization
    ///
    /// The inner image should be synchronized manually
    ///  - Src state: `PipelineStage::TRANSFER, Access::TRANSFER_WRITE, Layout::TransferDstOptimal`
    ///  - Dst state: `PipelineStage::TRANSFER, Access::TRANSFER_WRITE, Layout::TransferDstOptimal`
    pub fn flush(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        cbuf: &mut B::CommandBuffer,
    ) -> Result<FlushStatus> {
        let mut copy_cmds = Vec::with_capacity(self.new_entries.len());

        let subrs_layers = SubresourceLayers {
            aspects: Aspects::COLOR,
            level: 0,
            layers: 0..1,
        };

        let old_size = self.allocator.size();

        while let Some(entry) = self.new_entries.last_mut() {
            let alloc_res = self
                .allocator
                .allocate(Size::new(entry.size.0 as _, entry.size.1 as _));
            let alloc = match alloc_res {
                Some(alloc) => alloc,
                None => {
                    let new_size = self.allocator.size() * 2;
                    self.allocator.grow(new_size);
                    continue;
                }
            };

            let entry = self.new_entries.pop().unwrap();
            self.allocated.insert(
                entry.key,
                AllocatedEntry {
                    id: alloc.id,
                    rect: alloc.rectangle,
                    is_live: false,
                },
            );

            let cmd = BufferImageCopy {
                buffer_offset: entry.data_offset,
                buffer_width: entry.size.0,
                buffer_height: entry.size.1,
                image_layers: subrs_layers.clone(),
                image_offset: Offset {
                    x: alloc.rectangle.min.x as _,
                    y: alloc.rectangle.min.y as _,
                    z: 0,
                },
                image_extent: Extent {
                    width: alloc.rectangle.width() as _,
                    height: alloc.rectangle.height() as _,
                    depth: 1,
                },
            };

            copy_cmds.push(cmd);
        }

        let new_size = self.allocator.size();
        let status = if old_size != new_size {
            let mut new_image = Image::new(
                gpu,
                &ImageInfo::new_2d(new_size.width as _, new_size.height as _)
                    .with_format(self.format)
                    .with_usage(self.usage),
            )?;

            if let Some(name) = &self.name {
                new_image.set_name(gpu, name);
            }

            let old_image = &self.image;

            let stages = PipelineStage::TRANSFER..PipelineStage::TRANSFER;
            let barriers = &[
                Barrier::Image {
                    states: (Access::TRANSFER_WRITE, Layout::TransferDstOptimal)
                        ..(Access::TRANSFER_READ, Layout::TransferSrcOptimal),
                    target: old_image.raw(),
                    range: SUBRS_RANGE.clone(),
                    families: None,
                },
                Barrier::Image {
                    states: (Access::TRANSFER_WRITE, Layout::Undefined)
                        ..(Access::TRANSFER_WRITE, Layout::TransferDstOptimal),
                    target: new_image.raw(),
                    range: SUBRS_RANGE,
                    families: None,
                },
            ];

            unsafe {
                cbuf.pipeline_barrier(stages, Dependencies::empty(), barriers);
                cbuf.copy_image(
                    old_image.raw(),
                    Layout::TransferSrcOptimal,
                    new_image.raw(),
                    Layout::TransferDstOptimal,
                    &[ImageCopy {
                        src_subresource: subrs_layers.clone(),
                        src_offset: Offset { x: 0, y: 0, z: 0 },
                        dst_subresource: subrs_layers,
                        dst_offset: Offset { x: 0, y: 0, z: 0 },
                        extent: Extent {
                            width: old_size.width as _,
                            height: old_size.height as _,
                            depth: 1,
                        },
                    }],
                );
            }

            let new_image_view =
                new_image.create_view(gpu, ViewKind::D2, self.format, SUBRS_RANGE.clone())?;

            let old_image = mem::replace(&mut self.image, new_image);
            let old_image_view = mem::replace(&mut self.image_view, new_image_view);
            cleaner.add_image(old_image);
            cleaner.add_image_view(old_image_view);

            FlushStatus::Recreated
        } else {
            FlushStatus::Reused
        };

        unsafe {
            cbuf.copy_buffer_to_image(
                self.staging[frame].inner().raw(),
                self.image.raw(),
                Layout::TransferDstOptimal,
                &copy_cmds,
            );
        }

        self.staging[frame].clear();

        Ok(status)
    }

    /// Destroy the pipeline
    ///
    /// # Safety
    /// The atlas should not be in use
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        self.staging.destroy(|buf| buf.destroy(gpu));
        self.image.destroy(gpu);
        self.image_view.destroy(gpu);
    }
}

/// Atlas image status after the `flush call`
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum FlushStatus {
    /// The image was recreated
    Recreated,
    /// The image was reused
    Reused,
}
