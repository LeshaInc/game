//! Image wrappers

mod atlas;

pub use self::atlas::{FlushStatus, ImageAtlas};

use anyhow::{Context, Result};
use gfx_hal::device::Device;
use gfx_hal::format::{Format, Swizzle};
use gfx_hal::image::{Kind, SubresourceRange, Tiling, Usage, ViewCapabilities, ViewKind};
use gfx_hal::Backend;
use gfx_memory::{Block, Kind as MemKind, MemoryBlock, MemoryUsage};

use crate::gpu::Gpu;
use crate::util::Relevant;

/// Image info used for construction
#[derive(Clone, Copy, Debug)]
pub struct ImageInfo {
    /// Image kind (dimensions)
    pub kind: Kind,
    /// How many MIP levels to allocate
    pub mip_levels: u8,
    /// Image format
    pub format: Format,
    /// Image tiling (optimal or linear)
    pub tiling: Tiling,
    /// Usage flags
    pub usage: Usage,
    /// View capabilities
    pub view_caps: ViewCapabilities,
    /// Memory usage flags
    pub mem_usage: MemoryUsage,
}

impl ImageInfo {
    /// Create a 2D image info with 1 mip level, `Rgba8Unorm` format, optimal tiling, empty usage,
    /// empty view capabilities, and `Private` memory usage.
    pub fn new_2d(width: u32, height: u32) -> ImageInfo {
        ImageInfo {
            kind: Kind::D2(width, height, 1, 1),
            mip_levels: 1,
            format: Format::Rgba8Unorm,
            tiling: Tiling::Optimal,
            usage: Usage::empty(),
            view_caps: ViewCapabilities::empty(),
            mem_usage: MemoryUsage::Private,
        }
    }

    /// Change format
    pub fn with_format(mut self, format: Format) -> ImageInfo {
        self.format = format;
        self
    }

    /// Change usage
    pub fn with_usage(mut self, usage: Usage) -> ImageInfo {
        self.usage = usage;
        self
    }
}

/// Image wrapper
#[derive(Debug)]
pub struct Image<B: Backend> {
    /// Raw image object
    raw: B::Image,
    /// Memory block
    memory: MemoryBlock<B>,
    /// Marker to disallow dropping
    _relevant: Relevant,
}

impl<B: Backend> Image<B> {
    /// Create a new image
    pub fn new(gpu: &Gpu<B>, info: &ImageInfo) -> Result<Image<B>> {
        let device = gpu.device();
        let mut heaps = gpu.heaps();

        let mut raw = unsafe {
            device.create_image(
                info.kind,
                info.mip_levels,
                info.format,
                info.tiling,
                info.usage,
                info.view_caps,
            )
        }?;

        let req = unsafe { device.get_image_requirements(&raw) };

        let memory = heaps
            .allocate(
                device,
                req.type_mask as u32,
                info.mem_usage,
                MemKind::General,
                req.size,
                req.alignment,
            )
            .context("Cannot allocate memory for image")?;

        unsafe { device.bind_image_memory(memory.memory(), memory.segment().offset, &mut raw) }?;

        Ok(Image {
            raw,
            memory,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Set image name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        unsafe { gpu.device().set_image_name(&mut self.raw, name) }
    }

    /// Destroy the image
    ///
    /// # Safety
    /// All commands referring to the image must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_image(self.raw);
        gpu.heaps().free(gpu.device(), self.memory);
        self._relevant.destroy();
    }

    /// Get raw image object
    pub fn raw(&self) -> &B::Image {
        &self.raw
    }

    /// Get memory block
    pub fn memory(&self) -> &MemoryBlock<B> {
        &self.memory
    }

    /// Create an image view. No swizzling is performed
    pub fn create_view(
        &self,
        gpu: &Gpu<B>,
        kind: ViewKind,
        format: Format,
        range: SubresourceRange,
    ) -> Result<ImageView<B>> {
        let raw = unsafe {
            gpu.device()
                .create_image_view(&self.raw, kind, format, Swizzle::NO, range)
                .context("Cannot create image view")?
        };

        Ok(ImageView {
            raw,
            _relevant: Relevant::new::<ImageView<B>>(),
        })
    }
}

/// Image view wrapper
#[derive(Debug)]
pub struct ImageView<B: Backend> {
    raw: B::ImageView,
    _relevant: Relevant,
}

impl<B: Backend> ImageView<B> {
    /// Destroy the image view
    ///
    /// # Safety
    /// All commands referring to the image view must have completed execution
    /// No live framebuffers must refer to the view
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_image_view(self.raw);
        self._relevant.destroy();
    }

    /// Get raw image object
    pub fn raw(&self) -> &B::ImageView {
        &self.raw
    }
}
