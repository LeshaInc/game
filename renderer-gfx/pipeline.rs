//! Pipeline wrappers

use std::borrow::Borrow;
use std::fs::File;
use std::io::{Read, Seek, SeekFrom, Write};
use std::ops::Range;
use std::path::Path;

use anyhow::{Context, Result};
use gfx_hal::device::Device;
use gfx_hal::pso::{GraphicsPipelineDesc, ShaderStageFlags};
use gfx_hal::Backend;

use crate::util::Relevant;
use crate::Gpu;

/// Graphics pipeline wrapper
pub struct GraphicsPipeline<B: Backend> {
    raw: B::GraphicsPipeline,
    _relevant: Relevant,
}

impl<B: Backend> GraphicsPipeline<B> {
    /// Create a new graphics pipeline
    pub fn new(gpu: &Gpu<B>, desc: &GraphicsPipelineDesc<'_, B>) -> Result<GraphicsPipeline<B>> {
        let raw = unsafe { gpu.device().create_graphics_pipeline(desc, None) }
            .context("Cannot create graphics pipeline")?;
        Ok(GraphicsPipeline {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Create a new graphics pipeline using the given pipeline cache
    pub fn with_cache(
        gpu: &Gpu<B>,
        desc: &GraphicsPipelineDesc<'_, B>,
        cache: &PipelineCache<B>,
    ) -> Result<GraphicsPipeline<B>> {
        let raw = unsafe {
            gpu.device()
                .create_graphics_pipeline(desc, Some(cache.raw()))
                .context("Cannot create graphics pipeline")?
        };
        Ok(GraphicsPipeline {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Get raw pipeline object
    pub fn raw(&self) -> &B::GraphicsPipeline {
        &self.raw
    }

    /// Destroy the pipeline
    ///
    /// # Safety
    /// All commands using the pipeline must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_graphics_pipeline(self.raw);
        self._relevant.destroy();
    }
}

/// Pipeline cache wrapper
pub struct PipelineCache<B: Backend> {
    raw: B::PipelineCache,
    _relevant: Relevant,
}

impl<B: Backend> PipelineCache<B> {
    /// Load cache data from the specified path. If the file doesn't exist, an empty cache will be
    /// returned
    pub fn load(gpu: &Gpu<B>, path: &Path) -> Result<PipelineCache<B>> {
        let data = if path.exists() {
            let mut file = File::open(path)?;
            let size = file.seek(SeekFrom::End(0))?;
            let mut data = Vec::with_capacity(size as usize);
            file.read_to_end(&mut data)?;
            Some(data)
        } else {
            None
        };

        let raw = unsafe { gpu.device().create_pipeline_cache(data.as_deref()) }
            .context("Cannot create pipeline cache")?;

        Ok(PipelineCache {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Save cache data
    pub fn save(&self, gpu: &Gpu<B>, path: &Path) -> Result<()> {
        let data = unsafe { gpu.device().get_pipeline_cache_data(&self.raw()) }
            .context("Cannot retrieve pipeline cache data")?;
        let mut file = File::create(path)?;
        file.write_all(&data)?;
        Ok(())
    }

    /// Get raw pipeline cache
    pub fn raw(&self) -> &B::PipelineCache {
        &self.raw
    }

    /// Destroy the pipeline cache
    pub fn destroy(self, gpu: &Gpu<B>) {
        unsafe { gpu.device().destroy_pipeline_cache(self.raw) };
        self._relevant.destroy();
    }
}

/// Pipeline layout wrapper
pub struct PipelineLayout<B: Backend> {
    raw: B::PipelineLayout,
    _relevant: Relevant,
}

impl<B: Backend> PipelineLayout<B> {
    /// Create a new pipeline layout object
    pub fn new<IS, IR>(
        gpu: &Gpu<B>,
        set_layouts: IS,
        push_constants: IR,
    ) -> Result<PipelineLayout<B>>
    where
        IS: IntoIterator,
        IS::Item: Borrow<B::DescriptorSetLayout>,
        IR: IntoIterator,
        IR::Item: Borrow<(ShaderStageFlags, Range<u32>)>,
    {
        let raw = unsafe {
            gpu.device()
                .create_pipeline_layout(set_layouts, push_constants)
        }
        .context("Cannot create pipeline layout")?;
        Ok(PipelineLayout {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Get raw pipeline layout object
    pub fn raw(&self) -> &B::PipelineLayout {
        &self.raw
    }

    /// Destroy the pipeline layout
    ///
    /// # Safety
    /// The pipeline layout cannot be destroyed while any of the pipelines created using it are
    /// still alive
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_pipeline_layout(self.raw);
        self._relevant.destroy();
    }
}
