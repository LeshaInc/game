use std::iter;

use anyhow::Result;
use gfx_hal::buffer::Usage;
use gfx_hal::command::{BufferCopy, CommandBuffer};
use gfx_hal::Backend;
use gfx_memory::MemoryUsage;

use super::{Buffer, DynamicBuffer};
use crate::frame::FrameCleaner;
use crate::Gpu;

/// Uploads buffers in GPU memory
pub struct BufferUploader<B: Backend> {
    staging: DynamicBuffer<B>,
}

impl<B: Backend> BufferUploader<B> {
    /// Create a new buffer uploader
    pub fn new(gpu: &Gpu<B>) -> Result<BufferUploader<B>> {
        Ok(BufferUploader {
            staging: DynamicBuffer::new(gpu, Usage::TRANSFER_SRC | Usage::TRANSFER_DST)?,
        })
    }

    /// Destroy the uploader
    ///
    /// # Safety
    /// All commands encoded by the uploader must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        self.staging.destroy(gpu);
    }

    /// Upload a buffer. Further access should be synchronized externally
    pub fn upload(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        cbuf: &mut B::CommandBuffer,
        usage: Usage,
        data: &[u8],
    ) -> Result<Buffer<B>> {
        let size = data.len() as u64;
        let buffer = Buffer::new(gpu, size, usage | Usage::TRANSFER_DST, MemoryUsage::Private)?;

        self.staging.extend_nopreserve(gpu, cleaner, data)?;
        let end = self.staging.size();

        unsafe {
            cbuf.copy_buffer(
                self.staging.inner().raw(),
                buffer.raw(),
                iter::once(BufferCopy {
                    src: end - size,
                    dst: 0,
                    size,
                }),
            );
        }

        Ok(buffer)
    }
}
