use std::iter;
use std::marker::PhantomData;

use anyhow::Result;
use gfx_hal::buffer::Usage;
use gfx_hal::command::{BufferCopy, CommandBuffer};
use gfx_hal::Backend;
use gfx_memory::MemoryUsage;

use super::{Buffer, DynamicBuffer};
use crate::frame::{FrameCleaner, FrameIndex, FrameRingbuffer};
use crate::util::{inspect_slice_as_bytes, Pod};
use crate::Gpu;

/// Typed dynamic double buffer. Used for storing instances
pub struct InstanceBuffer<B: Backend, T: Pod> {
    staging: FrameRingbuffer<DynamicBuffer<B>>,
    device: Buffer<B>,
    usage: Usage,
    marker: PhantomData<T>,
}

impl<B: Backend, T: Pod> InstanceBuffer<B, T> {
    /// Create a new instance buffer
    pub fn new(
        gpu: &Gpu<B>,
        frames_in_flight: usize,
        mut usage: Usage,
    ) -> Result<InstanceBuffer<B, T>> {
        usage |= Usage::TRANSFER_DST;

        Ok(InstanceBuffer {
            staging: FrameRingbuffer::new(frames_in_flight, |_| {
                DynamicBuffer::new(gpu, Usage::TRANSFER_SRC)
            })?,
            device: Buffer::new(gpu, 256, usage, MemoryUsage::Private)?,
            usage,
            marker: PhantomData,
        })
    }

    /// Get the underlying device buffer
    pub fn inner(&self) -> &Buffer<B> {
        &self.device
    }

    /// Get the underlying host buffer
    pub fn dynamic(&mut self, frame: FrameIndex) -> &mut DynamicBuffer<B> {
        &mut self.staging[frame]
    }

    /// Set buffer name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        self.device.set_name(gpu, &format!("{}-device", name));
        for (i, staging) in self.staging.iter_mut().enumerate() {
            staging.set_name(gpu, &format!("{}-staging-{}", name, i));
        }
    }

    /// # Safety
    /// All commands referring to the buffer must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        self.staging.destroy(|buf| buf.destroy(gpu));
        self.device.destroy(gpu);
    }

    /// Reserve some space
    pub fn reserve(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        size: u64,
    ) -> Result<()> {
        self.staging[frame].reserve(gpu, cleaner, size * std::mem::size_of::<T>() as u64)
    }

    /// Extend the buffer with some values
    pub fn extend(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        values: &[T],
    ) -> Result<()> {
        self.staging[frame].extend(gpu, cleaner, inspect_slice_as_bytes(values))?;
        Ok(())
    }

    /// Clear the buffer
    pub fn clear(&mut self, frame: FrameIndex) {
        self.staging[frame].clear();
    }

    /// Change size of the buffer witout checking capacity
    pub unsafe fn set_size(&mut self, frame: FrameIndex, size: u64) {
        self.staging[frame].set_size(size * std::mem::size_of::<T>() as u64);
    }

    /// Copy data to the device buffer and clear the staging buffer
    ///
    /// # Synchronization
    ///
    /// The device buffer must be synchronized externally
    ///  - Src state: `PipelineStage::TRANSFER, Access::TRANSFER_WRITE`
    ///  - Dst state: `PipelineStage::TRANSFER, Access::TRANSFER_WRITE`
    pub fn flush(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        frame: FrameIndex,
        cbuf: &mut B::CommandBuffer,
    ) -> Result<()> {
        let staging = &mut self.staging[frame];

        if self.device.size() < staging.size() {
            let new_size = staging.capacity();
            let mut buf = Buffer::new(gpu, new_size, self.usage, MemoryUsage::Private)?;
            if let Some(name) = staging.get_name() {
                buf.set_name(
                    gpu,
                    &format!("{}-device-{}", &name[..name.len() - 8], frame.0),
                );
            }

            cleaner.add_buffer(std::mem::replace(&mut self.device, buf));
        }

        unsafe {
            cbuf.copy_buffer(
                staging.inner().raw(),
                self.device.raw(),
                iter::once(BufferCopy {
                    src: 0,
                    dst: 0,
                    size: staging.size(),
                }),
            );
        }

        Ok(())
    }
}
