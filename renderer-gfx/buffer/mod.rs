//! Buffer wrappers

mod dynamic;
mod instance;
mod uploader;

pub use self::dynamic::DynamicBuffer;
pub use self::instance::InstanceBuffer;
pub use self::uploader::BufferUploader;

use anyhow::{Context, Result};
use gfx_hal::buffer::Usage;
use gfx_hal::device::Device;
use gfx_hal::memory::Segment;
use gfx_hal::Backend;
use gfx_memory::{Block, Kind, MappedRange, MemoryBlock, MemoryUsage};

use crate::util::Relevant;
use crate::Gpu;

/// Buffer wrapper
#[derive(Debug)]
pub struct Buffer<B: Backend> {
    size: u64,
    /// Raw buffer object
    raw: B::Buffer,
    /// Memory block
    memory: MemoryBlock<B>,
    /// Marker to disallow dropping
    _relevant: Relevant,
}

impl<B: Backend> Buffer<B> {
    /// Create a new buffer
    pub fn new(gpu: &Gpu<B>, size: u64, usage: Usage, mem_usage: MemoryUsage) -> Result<Buffer<B>> {
        let device = gpu.device();
        let mut heaps = gpu.heaps();

        let mut raw = unsafe { device.create_buffer(size, usage) }?;
        let req = unsafe { device.get_buffer_requirements(&raw) };
        let memory = heaps
            .allocate(
                device,
                req.type_mask as u32,
                mem_usage,
                Kind::General,
                req.size,
                req.alignment,
            )
            .context("Cannot allocate memory for buffer")?;

        unsafe {
            device.bind_buffer_memory(memory.memory(), memory.segment().offset, &mut raw)?;
        }

        Ok(Buffer {
            size,
            raw,
            memory,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Create a new buffer and fill it from slice. `mem_usage` should specify host-visible memory
    pub fn from_slice<T: Copy>(
        gpu: &Gpu<B>,
        usage: Usage,
        mem_usage: MemoryUsage,
        data: &[T],
    ) -> Result<Buffer<B>> {
        let size = (data.len() as u64) * (std::mem::size_of::<T>() as u64);
        let mut buffer = Buffer::new(gpu, size, usage, mem_usage)?;
        {
            let mut map = buffer.map(gpu)?;
            let writer = unsafe { map.write::<T>(gpu.device(), Segment::ALL)? };
            writer.slice[..data.len()].copy_from_slice(data);
        }
        Ok(buffer)
    }

    /// Set buffer name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        unsafe { gpu.device().set_buffer_name(&mut self.raw, name) }
    }

    /// Destroy the buffer
    ///
    /// # Safety
    /// All commands referring to the buffer must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_buffer(self.raw);
        gpu.heaps().free(gpu.device(), self.memory);
        self._relevant.destroy();
    }

    /// Get raw buffer object
    pub fn raw(&self) -> &B::Buffer {
        &self.raw
    }

    /// Get memory block
    pub fn memory(&self) -> &MemoryBlock<B> {
        &self.memory
    }

    /// Map buffer memory
    pub fn map<'a>(&'a mut self, gpu: &Gpu<B>) -> Result<MappedRange<'a, B>> {
        Ok(self.memory.map(gpu.device(), Segment::ALL)?)
    }

    /// Get length of the buffer in bytes
    pub fn size(&self) -> u64 {
        self.size
    }
}
