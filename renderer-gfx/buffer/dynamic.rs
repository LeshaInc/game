use std::mem;

use anyhow::{Context, Result};
use gfx_hal::buffer::Usage;
use gfx_hal::memory::Segment;
use gfx_hal::Backend;
use gfx_memory::MemoryUsage;

use super::Buffer;
use crate::frame::FrameCleaner;
use crate::Gpu;

/// Resizable host memory buffer. Can be used as a staging buffer
#[derive(Debug)]
pub struct DynamicBuffer<B: Backend> {
    size: u64,
    usage: Usage,
    inner: Buffer<B>,
    name: Option<Box<str>>,
}

impl<B: Backend> DynamicBuffer<B> {
    /// Create a new dynamic buffer with default capacity (256 bytes)
    pub fn new(gpu: &Gpu<B>, usage: Usage) -> Result<DynamicBuffer<B>> {
        Ok(DynamicBuffer {
            size: 256,
            usage,
            inner: Self::_alloc(gpu, 256, usage)?,
            name: None,
        })
    }

    fn _alloc(gpu: &Gpu<B>, size: u64, usage: Usage) -> Result<Buffer<B>> {
        Buffer::new(
            gpu,
            size,
            Usage::TRANSFER_SRC | Usage::TRANSFER_DST | usage,
            MemoryUsage::Staging { read_back: false },
        )
    }

    fn alloc(&self, gpu: &Gpu<B>, size: u64) -> Result<Buffer<B>> {
        let mut buf = Self::_alloc(gpu, size, self.usage)?;
        if let Some(name) = &self.name {
            buf.set_name(gpu, name);
        }

        Ok(buf)
    }

    /// Set buffer name for easier debugging
    pub fn set_name(&mut self, gpu: &Gpu<B>, name: &str) {
        self.name = Some(String::from(name).into());
        self.inner.set_name(gpu, name);
    }

    /// Get buffer name
    pub fn get_name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    /// Get the underlying buffer
    pub fn inner(&self) -> &Buffer<B> {
        &self.inner
    }

    /// Get the underlying buffer
    pub fn inner_mut(&mut self) -> &mut Buffer<B> {
        &mut self.inner
    }

    /// Get number of used bytes
    pub fn size(&self) -> u64 {
        self.size
    }

    /// Get current capacity (the number of bytes the buffer can hold without reallocating)
    pub fn capacity(&self) -> u64 {
        self.inner.size()
    }

    /// Reserve capacity for at least `additional` more bytes to be inserted in the buffer
    pub fn reserve(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        additional: u64,
    ) -> Result<()> {
        let req_cap = self.size() + additional;
        let new_capacity = req_cap + (req_cap.wrapping_neg() & 255);
        if new_capacity > self.capacity() {
            self.realloc(gpu, cleaner, new_capacity, true)
                .context("Cannot resize dynamic buffer")?;
        }

        Ok(())
    }

    /// Reallocate the underlying buffer, copying the data from the old one if the `copy` flag is
    /// set to true
    pub fn realloc(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        new_capacity: u64,
        copy: bool,
    ) -> Result<()> {
        let segment = Segment {
            offset: 0,
            size: Some(self.size),
        };

        let mut new_buf = self.alloc(gpu, new_capacity)?;
        let old_buf = &mut self.inner;

        if copy {
            let mut new_map = new_buf.map(gpu)?;
            let new_range = unsafe { new_map.write::<u8>(gpu.device(), segment.clone()).unwrap() };

            let mut old_map = old_buf.map(gpu)?;
            let old_range = unsafe { old_map.read::<u8>(gpu.device(), segment).unwrap() };

            new_range.slice.copy_from_slice(old_range);

            drop((new_range, old_range));
        }

        cleaner.add_buffer(mem::replace(&mut self.inner, new_buf));

        Ok(())
    }

    /// Allocate space for the requested amount of bytes filling it by calling a closure
    pub fn extend_raw(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        size: u64,
        fill: impl FnOnce(&mut [u8]),
    ) -> Result<()> {
        let segment = Segment {
            offset: self.size,
            size: Some(size),
        };

        self.reserve(gpu, cleaner, size)?;

        let mut map = self.inner.map(gpu)?;
        let mut range = unsafe { map.write::<u8>(gpu.device(), segment).unwrap() };
        fill(&mut range.slice);

        self.size += size;

        Ok(())
    }

    /// Allocate space for the requested amount of bytes filling it by calling a closure
    ///
    /// If there is not enough capacity, the old data will not be copied, but will be still
    /// accessable by previously submitted commands
    pub fn extend_nopreserve_raw(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        size: u64,
        fill: impl FnOnce(&mut [u8]),
    ) -> Result<()> {
        if self.capacity() < self.size + size {
            self.size = 0;
            self.realloc(gpu, cleaner, size.next_power_of_two(), false)?;
        }

        let segment = Segment {
            offset: self.size,
            size: Some(size),
        };

        let mut map = self.inner.map(gpu)?;
        let mut range = unsafe { map.write::<u8>(gpu.device(), segment)? };
        fill(&mut range.slice);

        self.size += size;

        Ok(())
    }

    /// Copies all bytes from the specified slice and appends them to the buffer, resizing it if
    /// necessary
    pub fn extend(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        slice: &[u8],
    ) -> Result<()> {
        self.extend_raw(gpu, cleaner, slice.len() as u64, |out| {
            out.copy_from_slice(slice)
        })
    }

    /// Copies all bytes from the specified slice and appends them to the buffer
    ///
    /// If there is not enough capacity, the old data will not be copied, but will be still
    /// accessable by previously submitted commands
    pub fn extend_nopreserve(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        slice: &[u8],
    ) -> Result<()> {
        self.extend_nopreserve_raw(gpu, cleaner, slice.len() as u64, |out| {
            out.copy_from_slice(slice)
        })
    }

    /// Clear the buffer, but keep the allocated memory
    pub fn clear(&mut self) {
        self.size = 0;
    }

    /// Change size of the buffer without checking capacity
    pub unsafe fn set_size(&mut self, size: u64) {
        self.size = size;
    }

    /// Add padding bytes to the buffer so its size is divisible by `padding.len()`. The
    /// specified slice will be truncated if needed
    pub fn align(
        &mut self,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
        padding: &[u8],
    ) -> Result<()> {
        let len = padding.len() - self.size() as usize % padding.len();
        self.extend(gpu, cleaner, &padding[..len])
    }

    /// Destroy the dynamic buffer
    ///
    /// # Safety
    /// All commands referring to the buffer must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        self.inner.destroy(gpu);
    }
}
