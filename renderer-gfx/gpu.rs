use std::mem::ManuallyDrop;
use std::ops::{DerefMut, Drop};

use anyhow::{anyhow, Result};
use gfx_hal::adapter::{Adapter, DeviceType, PhysicalDevice};
use gfx_hal::device::Device;
use gfx_hal::queue::{QueueFamily, QueueFamilyId};
use gfx_hal::window::Surface;
use gfx_hal::{Backend, Features, Instance};
use gfx_memory::{GeneralConfig, Heaps, LinearConfig};
use parking_lot::Mutex;

/// Gpu wrapper
pub struct Gpu<B: Backend> {
    instance: ManuallyDrop<B::Instance>,
    physical_device: ManuallyDrop<B::PhysicalDevice>,
    device: ManuallyDrop<B::Device>,
    heaps: ManuallyDrop<Mutex<Heaps<B>>>,
    queue: ManuallyDrop<Mutex<B::CommandQueue>>,
    family_id: QueueFamilyId,
}

impl<B: Backend> Gpu<B> {
    /// Initialize a new GPU instance
    pub fn new(instance: B::Instance, surface: &B::Surface) -> Result<Gpu<B>> {
        let adapter = pick_adapter(instance.enumerate_adapters())
            .ok_or_else(|| anyhow!("No GPUs detected"))?;
        let family = pick_family::<B>(&adapter.queue_families, surface)
            .ok_or_else(|| anyhow!("Couldn't pick a supported  queue family"))?;
        let mut gpu = unsafe {
            adapter
                .physical_device
                .open(&[(family, &[1.0])], Features::empty())?
        };
        let family_id = gpu.queue_groups[0].family;
        let queue = gpu.queue_groups.remove(0).queues.remove(0);
        let limits = adapter.physical_device.limits();
        let mem_props = adapter.physical_device.memory_properties();
        let heaps = unsafe {
            Heaps::new(
                &mem_props,
                // some arbitrary values
                GeneralConfig {
                    block_size_granularity: 256,
                    min_device_allocation: 2 * 1024 * 1024,
                    max_chunk_size: 32 * 1024 * 1024,
                },
                LinearConfig {
                    linear_size: 32 * 1024 * 1024,
                },
                limits.non_coherent_atom_size as _,
            )
        };

        Ok(Gpu {
            instance: ManuallyDrop::new(instance),
            physical_device: ManuallyDrop::new(adapter.physical_device),
            device: ManuallyDrop::new(gpu.device),
            heaps: ManuallyDrop::new(Mutex::new(heaps)),
            queue: ManuallyDrop::new(Mutex::new(queue)),
            family_id,
        })
    }

    /// Get the underlying instance
    pub fn instance(&self) -> &B::Instance {
        &self.instance
    }

    /// Get the underlying logical device
    pub fn device(&self) -> &B::Device {
        &self.device
    }

    /// Get the underlying physical device
    pub fn physical_device(&self) -> &B::PhysicalDevice {
        &self.physical_device
    }

    /// Lock the memory allocator
    pub fn heaps(&self) -> impl DerefMut<Target = Heaps<B>> + '_ {
        self.heaps.lock()
    }

    /// Lock the command queue
    pub fn queue(&self) -> impl DerefMut<Target = B::CommandQueue> + '_ {
        self.queue.lock()
    }

    /// Get the queue family ID
    pub fn queue_family_id(&self) -> QueueFamilyId {
        self.family_id
    }
}

impl<B: Backend> Drop for Gpu<B> {
    fn drop(&mut self) {
        unsafe {
            self.device.wait_idle().unwrap();
            self.heaps.lock().clear(&self.device);
            ManuallyDrop::drop(&mut self.queue);
            ManuallyDrop::drop(&mut self.heaps);
            ManuallyDrop::drop(&mut self.device);
            ManuallyDrop::drop(&mut self.physical_device);
            ManuallyDrop::drop(&mut self.instance);
        }
    }
}

/// Pick the best available adapter
pub fn pick_adapter<B: Backend>(mut adapters: Vec<Adapter<B>>) -> Option<Adapter<B>> {
    log::trace!("Available adapters:");
    let (idx, _) = adapters
        .iter()
        .enumerate()
        .filter(|(i, adapter)| {
            log::trace!(
                " {}. {}; type: {:?}; id: {}; vendor: {}",
                i,
                adapter.info.name,
                adapter.info.device_type,
                adapter.info.device,
                adapter.info.vendor
            );

            adapter
                .queue_families
                .iter()
                .any(|f| f.queue_type().supports_graphics())
        })
        .max_by_key(|(_, adapter)| match adapter.info.device_type {
            DeviceType::DiscreteGpu => 4,
            DeviceType::VirtualGpu => 3,
            DeviceType::IntegratedGpu => 2,
            DeviceType::Cpu => 1,
            DeviceType::Other => 0,
        })?;

    log::trace!("Picked adapter #{}", idx);

    Some(adapters.remove(idx))
}

/// Pick a graphics queue family supporting the given surface
pub fn pick_family<'a, B: Backend>(
    families: &'a [B::QueueFamily],
    surface: &B::Surface,
) -> Option<&'a B::QueueFamily> {
    log::trace!("Available queue families: ");
    for (i, family) in families.iter().enumerate() {
        log::trace!(
            " {}. type: {:?}, max-queues: {}",
            i,
            family.queue_type(),
            family.max_queues()
        );
    }

    let (id, family) = families.iter().enumerate().find(|(_, family)| {
        surface.supports_queue_family(family) && family.queue_type().supports_graphics()
    })?;

    log::trace!("Picked family #{}", id);
    Some(family)
}
