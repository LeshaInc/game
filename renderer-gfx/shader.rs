use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::path::Path;

use anyhow::{bail, Context, Result};
use gfx_hal::device::Device;
use gfx_hal::Backend;

use crate::util::Relevant;
use crate::Gpu;

/// Shader module wrapper
pub struct ShaderModule<B: Backend> {
    raw: B::ShaderModule,
    _relevant: Relevant,
}

impl<B: Backend> ShaderModule<B> {
    /// Load SPIR-V shader from the specified path
    pub fn new(gpu: &Gpu<B>, path: &Path) -> Result<ShaderModule<B>> {
        let mut file =
            File::open(path).with_context(|| format!("Cannot open {}", path.display()))?;

        let size = file.seek(SeekFrom::End(0))?;
        if size % 4 != 0 {
            bail!("SPIR-V data is not divisible by 4");
        }

        let words = (size / 4) as usize;
        let mut data = Vec::<u32>::with_capacity(words);

        file.seek(SeekFrom::Start(0))?;

        unsafe {
            file.read_exact(std::slice::from_raw_parts_mut(
                data.as_mut_ptr() as *mut u8,
                words * 4,
            ))?;
            data.set_len(words);
        }

        const MAGIC_NUMBER: u32 = 0x07230203;

        if !data.is_empty() && data[0] == MAGIC_NUMBER.swap_bytes() {
            for word in &mut data {
                *word = word.swap_bytes();
            }
        }

        if data.is_empty() || data[0] != MAGIC_NUMBER {
            bail!("Shader missing SPIR-V magic number");
        }

        let raw = unsafe { gpu.device().create_shader_module(&data)? };

        Ok(ShaderModule {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Get raw shader module
    pub fn raw(&self) -> &B::ShaderModule {
        &self.raw
    }

    /// Destroy the shader module. It can be done even if the pipeline using the shader is still in
    /// use.
    pub fn destroy(self, gpu: &Gpu<B>) {
        unsafe { gpu.device().destroy_shader_module(self.raw) };
        self._relevant.destroy();
    }
}
