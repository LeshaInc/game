use std::iter;
use std::ops::Range;

use anyhow::{Context, Result};
use gfx_hal::buffer::{Access as BufferAccess, SubRange, Usage as BufferUsage};
use gfx_hal::command::CommandBuffer;
use gfx_hal::command::{BufferCopy, SubpassContents};
use gfx_hal::device::Device;
use gfx_hal::format::{Aspects, Format};
use gfx_hal::image::{
    Access as ImageAccess, Filter, Layout, SamplerDesc, SubresourceRange, Usage as ImageUsage,
    WrapMode,
};
use gfx_hal::memory::{Barrier, Dependencies};
use gfx_hal::pass::{Attachment, AttachmentOps, Subpass, SubpassDependency, SubpassDesc};
use gfx_hal::pso::{
    AttributeDesc, BakedStates, BasePipeline, BlendDesc, BlendState, ColorBlendDesc, ColorMask,
    DepthStencilDesc, Descriptor, DescriptorSetLayoutBinding, DescriptorSetWrite, DescriptorType,
    Element, EntryPoint, GraphicsPipelineDesc, GraphicsShaderSet, ImageDescriptorType,
    InputAssemblerDesc, PipelineCreationFlags, PipelineStage, Primitive, Rasterizer, Rect,
    ShaderStageFlags, Specialization, VertexBufferDesc, VertexInputRate,
};
use gfx_hal::Backend;
use gfx_memory::MemoryUsage;
use nalgebra::{Matrix3, Point2, Vector2};
use palette::{Pixel as _, Srgba};
use rusttype::{Font, GlyphId, PositionedGlyph};
use winit::dpi::LogicalSize;

use crate::buffer::{Buffer, InstanceBuffer};
use crate::descriptor::{DescriptorSet, DescriptorSetLayout};
use crate::frame::{FrameRingbuffer, Framebuffer};
use crate::image::ImageAtlas;
use crate::pipeline::{GraphicsPipeline, PipelineLayout};
use crate::util::Pod;
use crate::{InitializationCtx, RenderingCtx, ShaderModule, TeardownCtx};
use game_core::assets::{Assets, Id};
use game_core::graphics::drawlist2d::{DrawCommand2D, DrawList2D, Rectangle, Text};
use game_core::util::{Aabr, AabrTree};

/// Convert sRGBA to linear RGBA as an array suitable for shaders
fn to_rgba(srgba: Srgba) -> [f32; 4] {
    srgba.into_linear().into_format().into_raw()
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct Vertex {
    pos: [f32; 2],
}

unsafe impl Pod for Vertex {}

impl Vertex {
    const VBUF_DESC: VertexBufferDesc = VertexBufferDesc {
        binding: 0,
        stride: 8,
        rate: VertexInputRate::Vertex,
    };

    const ATTRIBUTES: &'static [AttributeDesc] = &[AttributeDesc {
        location: 0,
        binding: 0,
        element: Element {
            format: Format::Rg32Sfloat,
            offset: 0,
        },
    }];

    const QUAD: [Vertex; 6] = [
        Vertex { pos: [0.0, 0.0] },
        Vertex { pos: [1.0, 0.0] },
        Vertex { pos: [0.0, 1.0] },
        Vertex { pos: [1.0, 0.0] },
        Vertex { pos: [1.0, 1.0] },
        Vertex { pos: [0.0, 1.0] },
    ];
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct RectInstance {
    transform_col0_1: [f32; 4],
    transform_col2_shadow_offset: [f32; 4],
    color: [f32; 4],
    shadow_color: [f32; 4],
    border_color: [f32; 4],
    corner_radii: [f32; 4],
    size_radius_thickness: [f32; 4],
}

unsafe impl Pod for RectInstance {}

impl RectInstance {
    const VBUF_DESC: VertexBufferDesc = VertexBufferDesc {
        binding: 1,
        stride: 112,
        rate: VertexInputRate::Instance(1),
    };

    const ATTRIBUTES: &'static [AttributeDesc] = &[
        AttributeDesc {
            location: 1,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 0,
            },
        },
        AttributeDesc {
            location: 2,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 16,
            },
        },
        AttributeDesc {
            location: 3,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 32,
            },
        },
        AttributeDesc {
            location: 4,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 48,
            },
        },
        AttributeDesc {
            location: 5,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 64,
            },
        },
        AttributeDesc {
            location: 6,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 80,
            },
        },
        AttributeDesc {
            location: 7,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 96,
            },
        },
    ];

    fn new(rect: &Rectangle, dpi: f32, view_proj: &Matrix3<f32>) -> RectInstance {
        let aabr = rect.aabr();
        let margin = Vector2::from_element(1.0 / dpi);
        let min = aabr.min.coords - margin;
        let size = aabr.extent() + margin * 2.0;
        let model = Matrix3::new_nonuniform_scaling(&size).append_translation(&min);
        let mvp = view_proj * model;

        let col0 = mvp.column(0).xy();
        let col1 = mvp.column(1).xy();
        let col2 = mvp.column(2).xy();

        RectInstance {
            transform_col0_1: [col0.x, col0.y, col1.x, col1.y],
            transform_col2_shadow_offset: [
                col2.x,
                col2.y,
                rect.shadow.offset.x,
                rect.shadow.offset.y,
            ],
            color: to_rgba(rect.color),
            shadow_color: to_rgba(rect.shadow.color),
            border_color: to_rgba(rect.border.color),
            corner_radii: rect.corner_radii.into_array(),
            size_radius_thickness: [
                rect.size.x + margin.x * 2.0,
                rect.size.y + margin.x * 2.0,
                rect.shadow.radius,
                rect.border.thickness,
            ],
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct GlyphInstance {
    transform_col0_1: [f32; 4],
    transform_col2: [f32; 2],
    color: [f32; 4],
    uv_rect: [f32; 4],
}

unsafe impl Pod for GlyphInstance {}

impl GlyphInstance {
    const VBUF_DESC: VertexBufferDesc = VertexBufferDesc {
        binding: 1,
        stride: 56,
        rate: VertexInputRate::Instance(1),
    };

    const ATTRIBUTES: &'static [AttributeDesc] = &[
        AttributeDesc {
            location: 1,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 0,
            },
        },
        AttributeDesc {
            location: 2,
            binding: 1,
            element: Element {
                format: Format::Rg32Sfloat,
                offset: 16,
            },
        },
        AttributeDesc {
            location: 3,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 24,
            },
        },
        AttributeDesc {
            location: 4,
            binding: 1,
            element: Element {
                format: Format::Rgba32Sfloat,
                offset: 40,
            },
        },
    ];

    fn new(
        bb: rusttype::Rect<i32>,
        color: Srgba,
        dpi: f32,
        view_proj: &Matrix3<f32>,
    ) -> GlyphInstance {
        let size = Vector2::new(bb.width() as f32 / dpi, bb.height() as f32 / dpi);
        let pos = Vector2::new(bb.min.x as f32 / dpi, bb.min.y as f32 / dpi);
        let model = Matrix3::new_nonuniform_scaling(&size).append_translation(&pos);
        let mvp = view_proj * model;

        let col0 = mvp.column(0).xy();
        let col1 = mvp.column(1).xy();
        let col2 = mvp.column(2).xy();

        GlyphInstance {
            uv_rect: [0.0; 4],
            color: to_rgba(color),
            transform_col0_1: [col0.x, col0.y, col1.x, col1.y],
            transform_col2: col2.into(),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct GlyphKey {
    font_id: Id<Font<'static>>,
    glyph_id: GlyphId,
    subpixel_offset: (u32, u32),
    size: u32,
}

impl GlyphKey {
    fn new(font_id: Id<Font<'static>>, glyph: &PositionedGlyph<'_>) -> GlyphKey {
        GlyphKey {
            font_id,
            glyph_id: glyph.id(),
            subpixel_offset: (
                glyph.position().x.fract().to_bits(),
                glyph.position().y.fract().to_bits(),
            ),
            size: glyph.scale().y.to_bits(),
        }
    }
}

#[derive(Debug)]
enum DrawCall {
    Rect(usize, Rect),
    Text(usize, Rect),
}

/// 2D rendering pass
pub struct Render2DPass<B: Backend> {
    pipeline_layout: PipelineLayout<B>,
    rpass: B::RenderPass,

    rect_pipeline: GraphicsPipeline<B>,
    rect_ibuf: InstanceBuffer<B, RectInstance>,
    rect_instances: Vec<RectInstance>,
    rect_batch_ranges: Vec<Range<u32>>,

    text_pipeline: GraphicsPipeline<B>,
    text_ibuf: InstanceBuffer<B, GlyphInstance>,
    text_atlas: ImageAtlas<B, GlyphKey>,
    not_flushed_text: AabrTree,
    glyph_instances: Vec<GlyphInstance>,
    glyph_keys: Vec<GlyphKey>,
    text_batch_ranges: Vec<Range<u32>>,

    frame_desc_sets: FrameRingbuffer<DescriptorSet<B>>,
    desc_set_layout: DescriptorSetLayout<B>,
    drawcalls: Vec<DrawCall>,
    first_frame: bool,

    quad_vertices: Buffer<B>,
}

impl<B: Backend> Render2DPass<B> {
    /// Create a new 2D rendering pass
    pub fn new(ctx: &mut InitializationCtx<'_, B>) -> Result<Render2DPass<B>> {
        let gpu = ctx.gpu;
        let device = gpu.device();

        let bindings = vec![DescriptorSetLayoutBinding {
            binding: 0,
            ty: DescriptorType::Image {
                ty: ImageDescriptorType::Sampled { with_sampler: true },
            },
            count: 1,
            stage_flags: ShaderStageFlags::FRAGMENT,
            immutable_samplers: false, // TODO: use immutable samplers
        }];

        let desc_set_layout = DescriptorSetLayout::new(gpu, bindings)?;
        let frame_desc_sets = FrameRingbuffer::new(ctx.frames_in_flight, |i| {
            let mut set = ctx.desc_pool.allocate(&desc_set_layout)?;
            set.set_name(ctx.gpu, &format!("render2d-desc-{}", i));
            Ok(set)
        })?;

        let attachments = &[Attachment {
            format: Some(Format::Bgra8Srgb),
            samples: 1,
            ops: AttachmentOps::PRESERVE,
            stencil_ops: AttachmentOps::DONT_CARE,
            layouts: Layout::ColorAttachmentOptimal..Layout::Present,
        }];
        let subpasses = &[SubpassDesc {
            colors: &[(0, Layout::ColorAttachmentOptimal)],
            depth_stencil: None,
            inputs: &[],
            resolves: &[],
            preserves: &[],
        }];
        let dependencies = &[SubpassDependency {
            passes: None..Some(0),
            stages: PipelineStage::COLOR_ATTACHMENT_OUTPUT..PipelineStage::COLOR_ATTACHMENT_OUTPUT,
            accesses: ImageAccess::COLOR_ATTACHMENT_WRITE..ImageAccess::COLOR_ATTACHMENT_WRITE,
            flags: Dependencies::BY_REGION,
        }];
        let rpass = unsafe {
            device
                .create_render_pass(attachments, subpasses, dependencies)
                .context("Cannot create render pass")?
        };

        let push_constants = &[(ShaderStageFlags::FRAGMENT, 0..4)];
        let pipeline_layout =
            PipelineLayout::new(gpu, iter::once(desc_set_layout.raw()), push_constants)?;

        let rect_vert_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/rects.vert.spv"))?;
        let rect_frag_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/rects.frag.spv"))?;
        let mut attributes =
            Vec::with_capacity(Vertex::ATTRIBUTES.len() + RectInstance::ATTRIBUTES.len());
        attributes.extend_from_slice(Vertex::ATTRIBUTES);
        attributes.extend_from_slice(RectInstance::ATTRIBUTES);
        let rect_pso_desc = GraphicsPipelineDesc {
            shaders: GraphicsShaderSet {
                vertex: EntryPoint {
                    entry: "main",
                    module: rect_vert_shader.raw(),
                    specialization: Specialization::EMPTY,
                },
                hull: None,
                domain: None,
                geometry: None,
                fragment: Some(EntryPoint {
                    entry: "main",
                    module: rect_frag_shader.raw(),
                    specialization: Specialization::EMPTY,
                }),
            },
            rasterizer: Rasterizer::FILL,
            vertex_buffers: vec![Vertex::VBUF_DESC, RectInstance::VBUF_DESC],
            attributes,
            input_assembler: InputAssemblerDesc::new(Primitive::TriangleList),
            blender: BlendDesc {
                logic_op: None,
                targets: vec![ColorBlendDesc {
                    mask: ColorMask::ALL,
                    blend: Some(BlendState::ALPHA),
                }],
            },
            depth_stencil: DepthStencilDesc {
                depth: None,
                depth_bounds: false,
                stencil: None,
            },
            multisampling: None,
            baked_states: BakedStates::default(),
            layout: pipeline_layout.raw(),
            subpass: Subpass {
                index: 0,
                main_pass: &rpass,
            },
            flags: PipelineCreationFlags::empty(),
            parent: BasePipeline::None,
        };
        let rect_pipeline = GraphicsPipeline::new(gpu, &rect_pso_desc)?;

        let text_vert_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/text.vert.spv"))?;
        let text_frag_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/text.frag.spv"))?;
        let mut attributes =
            Vec::with_capacity(Vertex::ATTRIBUTES.len() + GlyphInstance::ATTRIBUTES.len());
        attributes.extend_from_slice(Vertex::ATTRIBUTES);
        attributes.extend_from_slice(GlyphInstance::ATTRIBUTES);
        let text_pso_desc = GraphicsPipelineDesc {
            shaders: GraphicsShaderSet {
                vertex: EntryPoint {
                    entry: "main",
                    module: text_vert_shader.raw(),
                    specialization: Specialization::EMPTY,
                },
                hull: None,
                domain: None,
                geometry: None,
                fragment: Some(EntryPoint {
                    entry: "main",
                    module: text_frag_shader.raw(),
                    specialization: Specialization::EMPTY,
                }),
            },
            vertex_buffers: vec![Vertex::VBUF_DESC, GlyphInstance::VBUF_DESC],
            attributes,
            ..rect_pso_desc
        };
        let text_pipeline = GraphicsPipeline::new(gpu, &text_pso_desc)?;

        let mut rect_ibuf = InstanceBuffer::new(gpu, ctx.frames_in_flight, BufferUsage::VERTEX)?;
        let mut text_ibuf = InstanceBuffer::new(gpu, ctx.frames_in_flight, BufferUsage::VERTEX)?;

        rect_ibuf.set_name(gpu, "rect-ibuf");
        text_ibuf.set_name(gpu, "text-ibuf");

        let mut text_atlas = ImageAtlas::new(
            gpu,
            ctx.frames_in_flight,
            Format::R8Unorm,
            ImageUsage::SAMPLED,
        )?;
        text_atlas.set_name(gpu, "text-atlas");

        let mut quad_vertices_staging = Buffer::from_slice(
            gpu,
            BufferUsage::VERTEX | BufferUsage::TRANSFER_SRC,
            MemoryUsage::Staging { read_back: false },
            &Vertex::QUAD,
        )?;
        quad_vertices_staging.set_name(gpu, "quad-vertices-staging");

        let mut quad_vertices = Buffer::new(
            gpu,
            quad_vertices_staging.size(),
            BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            MemoryUsage::Private,
        )?;
        quad_vertices.set_name(gpu, "quad-vertices");

        unsafe {
            ctx.cbuf.copy_buffer(
                quad_vertices_staging.raw(),
                quad_vertices.raw(),
                &[BufferCopy {
                    src: 0,
                    dst: 0,
                    size: quad_vertices_staging.size(),
                }],
            );
        }

        ctx.cleaner.add_buffer(quad_vertices_staging);
        rect_frag_shader.destroy(gpu);
        rect_vert_shader.destroy(gpu);
        text_frag_shader.destroy(gpu);
        text_vert_shader.destroy(gpu);

        Ok(Render2DPass {
            pipeline_layout,
            rpass,

            rect_pipeline,
            rect_ibuf,
            rect_instances: Vec::new(),
            rect_batch_ranges: Vec::new(),

            text_pipeline,
            text_ibuf,
            text_atlas,
            not_flushed_text: AabrTree::new(),
            glyph_instances: Vec::new(),
            glyph_keys: Vec::new(),
            text_batch_ranges: Vec::new(),

            frame_desc_sets,
            desc_set_layout,
            drawcalls: Vec::new(),
            first_frame: true,

            quad_vertices,
        })
    }

    fn add_rect(&mut self, ctx: &RenderingCtx<'_, B>, rect: &Rectangle, view_proj: &Matrix3<f32>) {
        self.rect_instances
            .push(RectInstance::new(rect, ctx.dpi_factor, view_proj));
    }

    fn end_rect_batch(&mut self, scissor: Rect) {
        let start = self
            .rect_batch_ranges
            .last()
            .map(|last| last.end)
            .unwrap_or(0);
        let end = self.rect_instances.len() as u32;
        self.rect_batch_ranges.push(start..end);
        let idx = self.rect_batch_ranges.len() - 1;
        self.drawcalls.push(DrawCall::Rect(idx, scissor));
    }

    fn upload_rects(&mut self, ctx: &mut RenderingCtx<'_, B>) -> Result<()> {
        self.rect_ibuf.clear(ctx.frame);
        self.rect_ibuf
            .extend(ctx.gpu, ctx.frame_cleaner, ctx.frame, &self.rect_instances)?;
        self.rect_ibuf
            .flush(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf)?;

        Ok(())
    }

    fn add_text(
        &mut self,
        ctx: &mut RenderingCtx<'_, B>,
        text: &Text,
        view_proj: &Matrix3<f32>,
    ) -> Result<()> {
        use rusttype::{Point, Scale};

        let assets = ctx.resources.get::<Assets>().unwrap();

        let dpi = ctx.dpi_factor;
        let font = assets.get_by_id(text.font_id).unwrap();
        let pos = Point {
            x: text.origin.x * dpi,
            y: text.origin.y * dpi,
        };

        let glyphs = font.layout(&text.text, Scale::uniform(text.size * dpi), pos);
        for glyph in glyphs {
            let bb = match glyph.pixel_bounding_box() {
                Some(v) => v,
                None => continue,
            };

            let key = GlyphKey::new(text.font_id, &glyph);
            let instance = GlyphInstance::new(bb, text.color, dpi, &view_proj);
            self.glyph_instances.push(instance);
            self.glyph_keys.push(key);

            self.text_atlas.add(
                ctx.gpu,
                ctx.frame_cleaner,
                ctx.frame,
                key,
                (bb.width() as u32, bb.height() as u32),
                |data| {
                    glyph.draw(|x, y, coverage| {
                        let value = (coverage * 255.0) as u8;
                        let idx = (y as usize) * (bb.width() as usize) + (x as usize);
                        data[idx] = value;
                    });
                },
            )?;
        }

        Ok(())
    }

    fn end_text_batch(&mut self, scissor: Rect) {
        let start = self
            .text_batch_ranges
            .last()
            .map(|last| last.end)
            .unwrap_or(0);
        let end = self.glyph_instances.len() as u32;
        self.text_batch_ranges.push(start..end);
        let idx = self.text_batch_ranges.len() - 1;
        self.drawcalls.push(DrawCall::Text(idx, scissor));
        self.not_flushed_text.clear();
    }

    fn upload_texts(&mut self, ctx: &mut RenderingCtx<'_, B>) -> Result<()> {
        if self.text_atlas.flush_needed() {
            self.text_atlas.deallocate_dead();
            self.text_atlas
                .flush(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf)?;
        }

        if self.glyph_instances.is_empty() {
            return Ok(());
        }

        for (glyph, key) in self.glyph_instances.iter_mut().zip(&self.glyph_keys) {
            let rect = self
                .text_atlas
                .get_rect_f32(key)
                .expect("no glyph in atlas");
            glyph.uv_rect = rect;
        }

        self.text_ibuf.clear(ctx.frame);
        self.text_ibuf
            .extend(ctx.gpu, ctx.frame_cleaner, ctx.frame, &self.glyph_instances)?;
        self.text_ibuf
            .flush(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf)?;

        Ok(())
    }

    fn reset(&mut self) {
        self.rect_instances.clear();
        self.rect_batch_ranges.clear();
        self.glyph_instances.clear();
        self.text_batch_ranges.clear();
        self.glyph_keys.clear();
    }

    fn build_proj_matrix(res: LogicalSize<f32>) -> Matrix3<f32> {
        Matrix3::new_nonuniform_scaling(&Vector2::new(2.0 / res.width, 2.0 / res.height))
            .append_translation(&Vector2::new(-1.0, -1.0))
    }

    fn evaluate_transforms(
        commands: &[DrawCommand2D],
        proj: Matrix3<f32>,
    ) -> impl Iterator<Item = (&'_ DrawCommand2D, Matrix3<f32>, Matrix3<f32>)> + Clone + '_ {
        let mut stack = Vec::new();
        let mut view = Matrix3::identity();
        let mut view_proj = proj;
        commands.iter().flat_map(move |command| match command {
            DrawCommand2D::PushMatrix => {
                stack.push(view);
                None
            }
            DrawCommand2D::Transform(mat) => {
                view = mat * view;
                view_proj = proj * view;
                None
            }
            DrawCommand2D::PopMatrix => {
                view = stack.pop().expect("nothing to pop");
                view_proj = proj * view;
                None
            }
            _ => Some((command, view_proj, view)),
        })
    }

    fn build_scissor(ctx: &RenderingCtx<'_, B>, scissor: &Aabr<f32>) -> Rect {
        let scale = ctx.dpi_factor;
        let size = scissor.extent();
        Rect {
            x: (scissor.min.x * scale) as _,
            y: (scissor.min.y * scale) as _,
            w: (size.x * scale) as _,
            h: (size.y * scale) as _,
        }
    }

    fn end_all_batches(&mut self, scissor: Rect) {
        self.end_rect_batch(scissor);
        self.end_text_batch(scissor);
    }

    /// Render the specified draw list
    pub fn render(
        &mut self,
        ctx: &mut RenderingCtx<'_, B>,
        color: &B::ImageView,
        list: &DrawList2D,
    ) -> Result<()> {
        let rect = Aabr {
            min: Point2::new(0.0, 0.0),
            max: Point2::new(ctx.logical_resolution.width, ctx.logical_resolution.height),
        };
        let fullscreen_scissor = Self::build_scissor(ctx, &rect);
        let mut scissor = fullscreen_scissor;

        let proj = Self::build_proj_matrix(ctx.logical_resolution);
        let transformed = Self::evaluate_transforms(&list.commands, proj);

        for (cmd, view_proj, view) in transformed {
            match *cmd {
                DrawCommand2D::Rectangle(rect_id) => {
                    let rect = &list.rectangles[rect_id];
                    let aabr = rect.aabr().transform(&view);
                    if self.not_flushed_text.any_overlap(aabr) {
                        self.end_all_batches(scissor);
                    }

                    self.add_rect(ctx, rect, &view_proj);
                }
                DrawCommand2D::Text(text_id) => {
                    let text = &list.texts[text_id];
                    let assets = ctx.resources.get::<Assets>().unwrap(); // TODO: move out of context
                    let aabr = text.aabr(&assets).map(|v| v.transform(&view));
                    drop(assets);
                    if let Some(aabr) = aabr {
                        self.not_flushed_text.insert(aabr);
                        self.add_text(ctx, text, &view_proj)?;
                    }
                }
                DrawCommand2D::SetScissor(ref rect) => {
                    self.end_all_batches(scissor);
                    scissor = Self::build_scissor(ctx, rect);
                }
                DrawCommand2D::ClearScissor => {
                    self.end_all_batches(scissor);
                    scissor = fullscreen_scissor;
                }
                _ => {}
            }
        }

        self.end_all_batches(scissor);

        let device = ctx.gpu.device();

        let attachments = iter::once(color);
        let framebuffer = Framebuffer::new(ctx.gpu, &self.rpass, attachments, ctx.extent())?;

        let atlas_src = if self.first_frame {
            (ImageAccess::empty(), Layout::Undefined)
        } else {
            (ImageAccess::SHADER_READ, Layout::ShaderReadOnlyOptimal)
        };

        let subrs_range = SubresourceRange {
            aspects: Aspects::COLOR,
            levels: 0..1,
            layers: 0..1,
        };

        let src_barriers = &[
            Barrier::Buffer {
                target: self.rect_ibuf.inner().raw(),
                states: BufferAccess::VERTEX_BUFFER_READ..BufferAccess::TRANSFER_WRITE,
                range: SubRange::WHOLE,
                families: None,
            },
            Barrier::Buffer {
                target: self.text_ibuf.inner().raw(),
                states: BufferAccess::VERTEX_BUFFER_READ..BufferAccess::TRANSFER_WRITE,
                range: SubRange::WHOLE,
                families: None,
            },
            Barrier::Image {
                target: self.text_atlas.image().raw(),
                states: atlas_src..(ImageAccess::TRANSFER_WRITE, Layout::TransferDstOptimal),
                range: subrs_range.clone(),
                families: None,
            },
        ];

        unsafe {
            ctx.cbuf.pipeline_barrier(
                PipelineStage::FRAGMENT_SHADER | PipelineStage::VERTEX_INPUT
                    ..PipelineStage::TRANSFER,
                Dependencies::empty(),
                src_barriers,
            );
        }

        self.upload_rects(ctx)?;
        self.upload_texts(ctx)?;

        let dst_barriers = &[
            Barrier::Buffer {
                target: self.rect_ibuf.inner().raw(),
                states: BufferAccess::TRANSFER_WRITE..BufferAccess::VERTEX_BUFFER_READ,
                range: SubRange::WHOLE,
                families: None,
            },
            Barrier::Buffer {
                target: self.text_ibuf.inner().raw(),
                states: BufferAccess::TRANSFER_WRITE..BufferAccess::VERTEX_BUFFER_READ,
                range: SubRange::WHOLE,
                families: None,
            },
            Barrier::Image {
                target: self.text_atlas.image().raw(),
                states: (ImageAccess::TRANSFER_WRITE, Layout::TransferDstOptimal)
                    ..(ImageAccess::SHADER_READ, Layout::ShaderReadOnlyOptimal),
                range: subrs_range.clone(),
                families: None,
            },
        ];

        unsafe {
            ctx.cbuf.pipeline_barrier(
                PipelineStage::TRANSFER
                    ..PipelineStage::FRAGMENT_SHADER | PipelineStage::VERTEX_INPUT,
                Dependencies::empty(),
                dst_barriers,
            );
        }

        let desc_set = self.frame_desc_sets[ctx.frame].raw();

        unsafe {
            let sampler = ctx
                .sampler_cache
                .get(ctx.gpu, &SamplerDesc::new(Filter::Linear, WrapMode::Clamp))?;
            device.write_descriptor_sets(iter::once(DescriptorSetWrite {
                set: desc_set,
                binding: 0,
                array_offset: 0,
                descriptors: &[Descriptor::CombinedImageSampler(
                    self.text_atlas.image_view().raw(),
                    Layout::ShaderReadOnlyOptimal,
                    sampler.raw(),
                )],
            }));
        }

        unsafe {
            ctx.cbuf.set_viewports(0, &[ctx.viewport()]);

            ctx.cbuf.begin_render_pass(
                &self.rpass,
                framebuffer.raw(),
                ctx.rect(),
                &[],
                SubpassContents::Inline,
            );

            ctx.cbuf.push_graphics_constants(
                self.pipeline_layout.raw(),
                ShaderStageFlags::FRAGMENT,
                0,
                &[ctx.dpi_factor.to_bits()],
            );

            ctx.cbuf.bind_graphics_descriptor_sets(
                self.pipeline_layout.raw(),
                0,
                iter::once(desc_set),
                &[],
            );

            ctx.cbuf
                .bind_vertex_buffers(0, iter::once((self.quad_vertices.raw(), SubRange::WHOLE)));

            for call in self.drawcalls.drain(..) {
                let pipeline = match call {
                    DrawCall::Rect(..) => self.rect_pipeline.raw(),
                    DrawCall::Text(..) => self.text_pipeline.raw(),
                };

                ctx.cbuf.bind_graphics_pipeline(pipeline);

                let vbuf = match call {
                    DrawCall::Rect(..) => self.rect_ibuf.inner().raw(),
                    DrawCall::Text(..) => self.text_ibuf.inner().raw(),
                };

                ctx.cbuf
                    .bind_vertex_buffers(1, iter::once((vbuf, SubRange::WHOLE)));

                let (range, scissor) = match call {
                    DrawCall::Rect(batch_id, s) => (&self.rect_batch_ranges[batch_id], s),
                    DrawCall::Text(batch_id, s) => (&self.text_batch_ranges[batch_id], s),
                };

                ctx.cbuf.set_scissors(0, &[scissor]);
                ctx.cbuf.draw(0..6, (range.start as _)..(range.end as _));
            }

            ctx.cbuf.end_render_pass();
        }

        ctx.frame_cleaner.add_framebuffer(framebuffer);
        self.reset();

        self.first_frame = false;

        Ok(())
    }

    /// Destroy the pipeline
    ///
    /// # Safety
    /// The pipeline should not be in use
    pub unsafe fn destroy(self, ctx: &mut TeardownCtx<'_, B>) {
        let gpu = ctx.gpu;

        self.rect_pipeline.destroy(gpu);
        self.text_pipeline.destroy(gpu);
        self.pipeline_layout.destroy(gpu);
        gpu.device().destroy_render_pass(self.rpass);
        self.desc_set_layout.destroy(gpu);
        self.text_atlas.destroy(gpu);
        self.text_ibuf.destroy(gpu);
        self.rect_ibuf.destroy(gpu);
        self.quad_vertices.destroy(gpu);
    }
}
