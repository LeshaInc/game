//! Rendering passes

mod render2d;
mod render3d;

pub use self::render2d::Render2DPass;
pub use self::render3d::Render3DPass;
