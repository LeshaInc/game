use std::{iter, mem};

use anyhow::{Context, Result};
use gfx_hal::buffer::{Access as BufferAccess, IndexBufferView, SubRange, Usage as BufferUsage};
use gfx_hal::command::{ClearColor, ClearDepthStencil, ClearValue, CommandBuffer, SubpassContents};
use gfx_hal::device::Device;
use gfx_hal::format::{Aspects, Format};
use gfx_hal::image::{
    Access as ImageAccess, Extent, Filter, Layout, SamplerDesc, SubresourceRange,
    Usage as ImageUsage, ViewKind, WrapMode,
};
use gfx_hal::memory::{Barrier, Dependencies, Segment};
use gfx_hal::pass::{Attachment, AttachmentOps, Subpass, SubpassDependency, SubpassDesc};
use gfx_hal::pso::{
    AttributeDesc, BakedStates, BasePipeline, BlendDesc, BufferDescriptorFormat,
    BufferDescriptorType, ColorBlendDesc, Comparison, DepthBias, DepthStencilDesc, DepthTest,
    Descriptor, DescriptorSetLayoutBinding, DescriptorSetWrite, DescriptorType, Element,
    EntryPoint, GraphicsPipelineDesc, GraphicsShaderSet, ImageDescriptorType, InputAssemblerDesc,
    PipelineCreationFlags, PipelineStage, Primitive, Rasterizer, Rect, ShaderStageFlags,
    Specialization, State, VertexBufferDesc, VertexInputRate, Viewport,
};
use gfx_hal::{Backend, IndexType};
use gfx_memory::MemoryUsage;
use nalgebra::{Unit, Vector3};

use crate::buffer::{Buffer, InstanceBuffer};
use crate::descriptor::{DescriptorSet, DescriptorSetLayout};
use crate::frame::{FrameCleaner, FrameRingbuffer, Framebuffer};
use crate::gpu::Gpu;
use crate::image::{Image, ImageInfo, ImageView};
use crate::pipeline::{GraphicsPipeline, PipelineLayout};
use crate::util::{gl_ndc_to_vk_ndc, Pod};
use crate::{InitializationCtx, RenderingCtx, ShaderModule, TeardownCtx};
use game_core::graphics::drawlist3d::MeshTransform;
use game_core::graphics::{DrawList3D, GraphicsSettings};
use game_core::Camera;

const VERTEX_BUFFERS: &[VertexBufferDesc] = &[
    VertexBufferDesc {
        binding: 0,
        stride: 32,
        rate: VertexInputRate::Vertex,
    },
    VertexBufferDesc {
        binding: 1,
        stride: 80,
        rate: VertexInputRate::Instance(1),
    },
];

const ATTRIBUTES: &[AttributeDesc] = &[
    // layout(location = 0) in vec3 in_pos;
    AttributeDesc {
        location: 0,
        binding: 0,
        element: Element {
            format: Format::Rgb32Sfloat,
            offset: 0,
        },
    },
    // layout(location = 1) in vec3 in_normal;
    AttributeDesc {
        location: 1,
        binding: 0,
        element: Element {
            format: Format::Rgb32Sfloat,
            offset: 12,
        },
    },
    // layout(location = 2) in vec2 in_tex;
    AttributeDesc {
        location: 2,
        binding: 0,
        element: Element {
            format: Format::Rg32Sfloat,
            offset: 24,
        },
    },
    // layout(location = 3) in vec4 in_model_row0;
    AttributeDesc {
        location: 3,
        binding: 1,
        element: Element {
            format: Format::Rgba32Sfloat,
            offset: 0,
        },
    },
    // layout(location = 4) in vec4 in_model_row1;
    AttributeDesc {
        location: 4,
        binding: 1,
        element: Element {
            format: Format::Rgba32Sfloat,
            offset: 16,
        },
    },
    // layout(location = 5) in vec4 in_model_row2;
    AttributeDesc {
        location: 5,
        binding: 1,
        element: Element {
            format: Format::Rgba32Sfloat,
            offset: 32,
        },
    },
    // layout(location = 6) in vec4 in_rot;
    AttributeDesc {
        location: 6,
        binding: 1,
        element: Element {
            format: Format::Rgba32Sfloat,
            offset: 48,
        },
    },
    // layout(location = 7) in vec4 in_uv_rect;
    AttributeDesc {
        location: 7,
        binding: 1,
        element: Element {
            format: Format::Rgba32Sfloat,
            offset: 64,
        },
    },
];

#[derive(Copy, Clone)]
#[repr(C)]
struct Uniforms {
    view_projection: [[f32; 4]; 4],
    shadow_matrix: [[f32; 4]; 4],
    shadowmap_size: [f32; 2],
}

#[derive(Copy, Clone)]
#[allow(dead_code)]
#[repr(C)]
struct MeshInstance {
    model_row0: [f32; 4],
    model_row1: [f32; 4],
    model_row2: [f32; 4],
    rot: [f32; 4],
    uv_rect: [f32; 4],
}

unsafe impl Pod for MeshInstance {}

impl MeshInstance {
    fn new(model: &MeshTransform) -> MeshInstance {
        MeshInstance {
            model_row0: model.matrix.row(0).transpose().into(),
            model_row1: model.matrix.row(1).transpose().into(),
            model_row2: model.matrix.row(2).transpose().into(),
            rot: model.rotation.into_inner().coords.into(),
            uv_rect: [0.0; 4],
        }
    }
}

/// 3D rendering pass
pub struct Render3DPass<B: Backend> {
    shadow_rpass: B::RenderPass,
    shadow_image: Image<B>,
    shadow_image_view: ImageView<B>,
    shadow_framebuffer: Framebuffer<B>,
    shadow_pipeline_layout: PipelineLayout<B>,
    shadow_pipeline: GraphicsPipeline<B>,
    shadow_res: (u32, u32),

    forward_rpass: B::RenderPass,
    forward_pipeline_layout: PipelineLayout<B>,
    forward_pipeline: GraphicsPipeline<B>,

    frame_desc_sets: FrameRingbuffer<DescriptorSet<B>>,
    frame_desc_set_layout: DescriptorSetLayout<B>,
    shadow_ibuf: InstanceBuffer<B, MeshInstance>,
    forward_ibuf: InstanceBuffer<B, MeshInstance>,
    uniform_buffers: FrameRingbuffer<Buffer<B>>,

    first_frame: bool,
}

fn create_shadow_views<B: Backend>(
    gpu: &Gpu<B>,
    settings: &GraphicsSettings,
    rpass: &B::RenderPass,
) -> Result<(Image<B>, ImageView<B>, Framebuffer<B>)> {
    let res = settings.shadow_resolution;
    let mut shadow_image = Image::new(
        gpu,
        &ImageInfo::new_2d(res, res)
            .with_format(Format::D16Unorm)
            .with_usage(ImageUsage::DEPTH_STENCIL_ATTACHMENT | ImageUsage::SAMPLED),
    )?;

    shadow_image.set_name(gpu, "shadow-depth");

    let shadow_image_view = shadow_image.create_view(
        gpu,
        ViewKind::D2,
        Format::D16Unorm,
        SubresourceRange {
            aspects: Aspects::DEPTH,
            levels: 0..1,
            layers: 0..1,
        },
    )?;

    let extent = Extent {
        width: res,
        height: res,
        depth: 1,
    };

    let shadow_framebuffer =
        Framebuffer::new(gpu, rpass, iter::once(shadow_image_view.raw()), extent)?;

    Ok((shadow_image, shadow_image_view, shadow_framebuffer))
}

impl<B: Backend> Render3DPass<B> {
    /// Create a new 3D rendering pass
    pub fn new(ctx: &mut InitializationCtx<'_, B>) -> Result<Render3DPass<B>> {
        let gpu = ctx.gpu;
        let device = gpu.device();

        let attachments = &[Attachment {
            format: Some(Format::D16Unorm),
            samples: 1,
            ops: AttachmentOps::INIT,
            stencil_ops: AttachmentOps::DONT_CARE,
            layouts: Layout::Undefined..Layout::ShaderReadOnlyOptimal,
        }];
        let subpasses = &[SubpassDesc {
            colors: &[],
            depth_stencil: Some(&(0, Layout::DepthStencilAttachmentOptimal)),
            inputs: &[],
            resolves: &[],
            preserves: &[],
        }];
        let dependencies = &[SubpassDependency {
            passes: Some(0)..None,
            stages: PipelineStage::EARLY_FRAGMENT_TESTS..PipelineStage::FRAGMENT_SHADER,
            accesses: ImageAccess::DEPTH_STENCIL_ATTACHMENT_WRITE..ImageAccess::SHADER_READ,
            flags: Dependencies::BY_REGION,
        }];
        let shadow_rpass = unsafe {
            device
                .create_render_pass(attachments, subpasses, dependencies)
                .context("Cannot create render pass")?
        };

        let (shadow_image, shadow_image_view, shadow_framebuffer) =
            create_shadow_views(gpu, ctx.settings, &shadow_rpass)?;

        let push_constants = &[(ShaderStageFlags::VERTEX, 0..64)];
        let shadow_pipeline_layout = PipelineLayout::new(gpu, &[], push_constants)?;

        let shadow_vert_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/shadows.vert.spv"))?;

        let shadow_pso_desc = GraphicsPipelineDesc {
            shaders: GraphicsShaderSet {
                vertex: EntryPoint {
                    entry: "main",
                    module: shadow_vert_shader.raw(),
                    specialization: Specialization::EMPTY,
                },
                hull: None,
                domain: None,
                geometry: None,
                fragment: None,
            },
            rasterizer: Rasterizer {
                depth_bias: Some(State::Static(DepthBias {
                    const_factor: 1.25,
                    clamp: 0.0,
                    slope_factor: 1.75,
                })),
                ..Rasterizer::FILL
            },
            vertex_buffers: VERTEX_BUFFERS.into(),
            attributes: ATTRIBUTES.into(),
            input_assembler: InputAssemblerDesc::new(Primitive::TriangleList),
            blender: BlendDesc {
                logic_op: None,
                targets: vec![],
            },
            depth_stencil: DepthStencilDesc {
                depth: Some(DepthTest {
                    fun: Comparison::LessEqual,
                    write: true,
                }),
                depth_bounds: false,
                stencil: None,
            },
            multisampling: None,
            baked_states: BakedStates::default(),
            layout: shadow_pipeline_layout.raw(),
            subpass: Subpass {
                index: 0,
                main_pass: &shadow_rpass,
            },
            flags: PipelineCreationFlags::empty(),
            parent: BasePipeline::None,
        };
        let shadow_pipeline = GraphicsPipeline::new(gpu, &shadow_pso_desc)?;

        let bindings = vec![
            DescriptorSetLayoutBinding {
                binding: 0,
                ty: DescriptorType::Buffer {
                    ty: BufferDescriptorType::Uniform,
                    format: BufferDescriptorFormat::Structured {
                        dynamic_offset: false,
                    },
                },
                count: 1,
                stage_flags: ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                immutable_samplers: false,
            },
            DescriptorSetLayoutBinding {
                binding: 1,
                ty: DescriptorType::Image {
                    ty: ImageDescriptorType::Sampled { with_sampler: true },
                },
                count: 1,
                stage_flags: ShaderStageFlags::FRAGMENT,
                immutable_samplers: false, // TODO: use immutable samplers
            },
            DescriptorSetLayoutBinding {
                binding: 2,
                ty: DescriptorType::Image {
                    ty: ImageDescriptorType::Sampled { with_sampler: true },
                },
                count: 1,
                stage_flags: ShaderStageFlags::FRAGMENT,
                immutable_samplers: false, // TODO: use immutable samplers
            },
        ];

        let frame_desc_set_layout = DescriptorSetLayout::new(gpu, bindings)?;
        let frame_desc_sets = FrameRingbuffer::new(ctx.frames_in_flight, |i| {
            let mut set = ctx.desc_pool.allocate(&frame_desc_set_layout)?;
            set.set_name(ctx.gpu, &format!("forward-desc-{}", i));
            Ok(set)
        })?;

        let attachments = &[
            Attachment {
                format: Some(Format::Bgra8Srgb),
                samples: 1,
                ops: AttachmentOps::INIT,
                stencil_ops: AttachmentOps::DONT_CARE,
                layouts: Layout::Undefined..Layout::ColorAttachmentOptimal,
            },
            Attachment {
                format: Some(Format::D16Unorm),
                samples: 1,
                ops: AttachmentOps::INIT,
                stencil_ops: AttachmentOps::DONT_CARE,
                layouts: Layout::Undefined..Layout::DepthStencilAttachmentOptimal,
            },
        ];
        let subpasses = &[SubpassDesc {
            colors: &[(0, Layout::ColorAttachmentOptimal)],
            depth_stencil: Some(&(1, Layout::DepthStencilAttachmentOptimal)),
            inputs: &[],
            resolves: &[],
            preserves: &[],
        }];
        let dependencies = &[
            SubpassDependency {
                passes: None..Some(0),
                stages: PipelineStage::FRAGMENT_SHADER..PipelineStage::EARLY_FRAGMENT_TESTS,
                accesses: ImageAccess::SHADER_READ..ImageAccess::DEPTH_STENCIL_ATTACHMENT_WRITE,
                flags: Dependencies::BY_REGION,
            },
            SubpassDependency {
                passes: Some(0)..None,
                stages: PipelineStage::COLOR_ATTACHMENT_OUTPUT
                    ..PipelineStage::COLOR_ATTACHMENT_OUTPUT,
                accesses: ImageAccess::COLOR_ATTACHMENT_WRITE..ImageAccess::COLOR_ATTACHMENT_WRITE,
                flags: Dependencies::BY_REGION,
            },
        ];
        let forward_rpass = unsafe {
            device
                .create_render_pass(attachments, subpasses, dependencies)
                .context("Cannot create render pass")?
        };

        let push_constants = &[(ShaderStageFlags::FRAGMENT, 0..4)];
        let forward_pipeline_layout =
            PipelineLayout::new(gpu, iter::once(frame_desc_set_layout.raw()), push_constants)?;

        let forward_vert_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/forward.vert.spv"))?;
        let forward_frag_shader =
            ShaderModule::new(gpu, &ctx.assets_path.join("shaders-vk/forward.frag.spv"))?;

        let forward_pso_desc = GraphicsPipelineDesc {
            shaders: GraphicsShaderSet {
                vertex: EntryPoint {
                    entry: "main",
                    module: forward_vert_shader.raw(),
                    specialization: Specialization::EMPTY,
                },
                hull: None,
                domain: None,
                geometry: None,
                fragment: Some(EntryPoint {
                    entry: "main",
                    module: forward_frag_shader.raw(),
                    specialization: Specialization::EMPTY,
                }),
            },
            rasterizer: Rasterizer::FILL,
            blender: BlendDesc {
                logic_op: None,
                targets: vec![ColorBlendDesc::EMPTY],
            },
            layout: forward_pipeline_layout.raw(),
            subpass: Subpass {
                index: 0,
                main_pass: &forward_rpass,
            },
            ..shadow_pso_desc
        };
        let forward_pipeline = GraphicsPipeline::new(gpu, &forward_pso_desc)?;

        let uniform_buffers = FrameRingbuffer::new(ctx.frames_in_flight, |i| {
            let mut buffer = Buffer::new(
                ctx.gpu,
                mem::size_of::<Uniforms>() as u64,
                BufferUsage::UNIFORM,
                MemoryUsage::Dynamic {
                    sparse_updates: false,
                },
            )?;
            buffer.set_name(ctx.gpu, &format!("forward-ubo-{}", i));
            Ok(buffer)
        })?;

        shadow_vert_shader.destroy(gpu);
        forward_vert_shader.destroy(gpu);
        forward_frag_shader.destroy(gpu);

        let mut shadow_ibuf = InstanceBuffer::new(gpu, ctx.frames_in_flight, BufferUsage::VERTEX)?;
        let mut forward_ibuf = InstanceBuffer::new(gpu, ctx.frames_in_flight, BufferUsage::VERTEX)?;

        shadow_ibuf.set_name(gpu, "shadow-ibuf");
        forward_ibuf.set_name(gpu, "forward-ibuf");

        Ok(Render3DPass {
            shadow_rpass,
            shadow_image,
            shadow_image_view,
            shadow_framebuffer,
            shadow_pipeline_layout,
            shadow_pipeline,
            forward_rpass,
            forward_pipeline_layout,
            forward_pipeline,
            shadow_res: (
                ctx.settings.shadow_resolution,
                ctx.settings.shadow_resolution,
            ),

            frame_desc_sets,
            frame_desc_set_layout,
            shadow_ibuf,
            forward_ibuf,
            uniform_buffers,
            first_frame: true,
        })
    }

    fn add_instances(
        ctx: &mut RenderingCtx<'_, B>,
        list: &DrawList3D,
        target: &mut InstanceBuffer<B, MeshInstance>,
        textured: bool,
    ) -> Result<()> {
        let size = list.len() as u64;
        target.clear(ctx.frame);
        target.reserve(ctx.gpu, ctx.frame_cleaner, ctx.frame, size)?;
        unsafe { target.set_size(ctx.frame, size) };

        let mut map = target.dynamic(ctx.frame).inner_mut().map(ctx.gpu)?;
        let mut writer = unsafe { map.write::<MeshInstance>(ctx.gpu.device(), Segment::ALL)? };
        let slice = &mut writer.slice;

        if textured {
            for (i, (_, trans, tex)) in list.iter().enumerate() {
                let mut instance = MeshInstance::new(trans);
                if let Some(rect) = tex.and_then(|t| ctx.storage.atlas.get_rect_f32(&t)) {
                    instance.uv_rect = rect;
                }
                slice[i] = instance;
            }
        } else {
            for (i, (_, trans, _)) in list.iter().enumerate() {
                let instance = MeshInstance::new(trans);
                slice[i] = instance;
            }
        }

        Ok(())
    }

    fn prepare_list(ctx: &mut RenderingCtx<'_, B>, list: &DrawList3D) {
        let mut barriers = Vec::new();
        for (mesh_id, _, _) in list.iter() {
            let mesh = &ctx.storage.meshes[&mesh_id];
            if mesh.is_visible.get() {
                continue;
            }

            mesh.is_visible.set(true);

            barriers.push(Barrier::Buffer {
                target: mesh.vbuf.raw(),
                states: BufferAccess::TRANSFER_WRITE..BufferAccess::VERTEX_BUFFER_READ,
                range: SubRange::WHOLE,
                families: None,
            });

            barriers.push(Barrier::Buffer {
                target: mesh.ibuf.raw(),
                states: BufferAccess::TRANSFER_WRITE..BufferAccess::INDEX_BUFFER_READ,
                range: SubRange::WHOLE,
                families: None,
            });
        }

        if !barriers.is_empty() {
            unsafe {
                ctx.cbuf.pipeline_barrier(
                    PipelineStage::TRANSFER..PipelineStage::VERTEX_INPUT,
                    Dependencies::empty(),
                    barriers,
                );
            }
        }
    }

    fn draw_list(
        ctx: &mut RenderingCtx<'_, B>,
        list: &DrawList3D,
        layout: &PipelineLayout<B>,
        textured: bool,
    ) {
        let mut objects = list.iter().peekable();

        let tex_const = &[1.0f32.to_bits()];
        let notex_const = &[0.0f32.to_bits()];

        let mut offset = 0;
        let mut batch_len = 0;
        let mut prev_tex = true;

        let fragment = ShaderStageFlags::FRAGMENT;

        if textured {
            unsafe {
                ctx.cbuf
                    .push_graphics_constants(layout.raw(), fragment, 0, tex_const);
            }
        }

        while let Some((mesh_id, _, texture_id)) = objects.next() {
            batch_len += 1;

            let next = objects.peek().copied();
            if let Some((next_mesh, _, next_tex)) = next {
                if mesh_id == next_mesh && texture_id.is_some() == next_tex.is_some() {
                    continue;
                }
            }

            let mesh = &ctx.storage.meshes[&mesh_id];

            unsafe {
                ctx.cbuf.bind_index_buffer(IndexBufferView {
                    buffer: mesh.ibuf.raw(),
                    range: SubRange::WHOLE,
                    index_type: IndexType::U32,
                });

                ctx.cbuf
                    .bind_vertex_buffers(0, iter::once((mesh.vbuf.raw(), SubRange::WHOLE)));

                let has_tex = texture_id.is_some();
                if textured && prev_tex != has_tex {
                    if has_tex {
                        ctx.cbuf
                            .push_graphics_constants(layout.raw(), fragment, 0, tex_const);
                    } else {
                        ctx.cbuf
                            .push_graphics_constants(layout.raw(), fragment, 0, notex_const);
                    }

                    prev_tex = has_tex;
                }

                ctx.cbuf.draw_indexed(
                    0..(mesh.ibuf.size() / 4) as u32,
                    0,
                    offset..offset + batch_len,
                )
            }

            offset += batch_len;
            batch_len = 0;
        }
    }

    /// Render the specified draw list
    pub fn render(
        &mut self,
        ctx: &mut RenderingCtx<'_, B>,
        color: &B::ImageView,
        depth: &ImageView<B>,
        objects: DrawList3D,
        casters: DrawList3D,
    ) -> Result<()> {
        let mut camera = ctx.resources.get_mut::<Camera>().unwrap();
        let light_dir = Unit::new_normalize(Vector3::new(1.0, 1.0, -2.0));
        let (shadow_matrix, _planes) = camera.shadow_matrix(light_dir, 4.0);
        camera.fit_near_far(4.0);
        camera.aspect = (ctx.resolution.width as f32) / (ctx.resolution.height as f32);
        camera.update_projection();

        let proj = camera.projection.to_homogeneous();
        let view = camera.view.to_homogeneous();
        let view_proj = gl_ndc_to_vk_ndc(&(proj * view));
        let shadow_matrix = gl_ndc_to_vk_ndc(&shadow_matrix);

        drop(camera);

        let device = ctx.gpu.device();
        let desc_set = self.frame_desc_sets[ctx.frame].raw();
        let uniform_buffer = &mut self.uniform_buffers[ctx.frame];

        let uniforms = Uniforms {
            view_projection: *view_proj.as_ref(),
            shadow_matrix: *shadow_matrix.as_ref(),
            shadowmap_size: [self.shadow_res.0 as f32, self.shadow_res.1 as f32],
        };

        {
            let mut map = uniform_buffer.map(ctx.gpu)?;
            let segment = Segment {
                offset: 0,
                size: Some(mem::size_of::<Uniforms>() as _),
            };
            let range = unsafe { map.write::<Uniforms>(ctx.gpu.device(), segment)? };
            range.slice[0] = uniforms;
        }

        let attachments = iter::once(color).chain(iter::once(depth.raw()));
        let framebuffer =
            Framebuffer::new(ctx.gpu, &self.forward_rpass, attachments, ctx.extent())?;

        Self::add_instances(ctx, &casters, &mut self.shadow_ibuf, false)?;

        unsafe {
            device.write_descriptor_sets(iter::once(DescriptorSetWrite {
                set: desc_set,
                binding: 0,
                array_offset: 0,
                descriptors: &[Descriptor::Buffer(uniform_buffer.raw(), SubRange::WHOLE)],
            }));

            let sampler = ctx
                .sampler_cache
                .get(ctx.gpu, &SamplerDesc::new(Filter::Linear, WrapMode::Clamp))?;
            device.write_descriptor_sets(iter::once(DescriptorSetWrite {
                set: desc_set,
                binding: 1,
                array_offset: 0,
                descriptors: &[Descriptor::CombinedImageSampler(
                    self.shadow_image_view.raw(),
                    Layout::ShaderReadOnlyOptimal,
                    sampler.raw(),
                )],
            }));
        }

        unsafe {
            let barrier = Barrier::Buffer {
                target: self.shadow_ibuf.inner().raw(),
                states: BufferAccess::VERTEX_BUFFER_READ..BufferAccess::TRANSFER_WRITE,
                range: SubRange::WHOLE,
                families: None,
            };

            ctx.cbuf.pipeline_barrier(
                PipelineStage::VERTEX_INPUT..PipelineStage::TRANSFER,
                Dependencies::empty(),
                &[barrier],
            );

            self.shadow_ibuf
                .flush(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf)?;

            let barrier = Barrier::Buffer {
                target: self.shadow_ibuf.inner().raw(),
                states: BufferAccess::TRANSFER_WRITE..BufferAccess::VERTEX_BUFFER_READ,
                range: SubRange::WHOLE,
                families: None,
            };

            ctx.cbuf.pipeline_barrier(
                PipelineStage::TRANSFER..PipelineStage::VERTEX_INPUT,
                Dependencies::empty(),
                &[barrier],
            );

            Self::prepare_list(ctx, &casters);

            let rect = Rect {
                x: 0,
                y: 0,
                w: self.shadow_res.0 as _,
                h: self.shadow_res.1 as _,
            };
            ctx.cbuf.set_viewports(
                0,
                &[Viewport {
                    rect,
                    depth: 0.0..1.0,
                }],
            );
            ctx.cbuf.set_scissors(0, &[rect]);

            ctx.cbuf.begin_render_pass(
                &self.shadow_rpass,
                self.shadow_framebuffer.raw(),
                rect,
                iter::once(ClearValue {
                    depth_stencil: ClearDepthStencil {
                        depth: 1.0,
                        stencil: 0,
                    },
                }),
                SubpassContents::Inline,
            );

            ctx.cbuf.bind_graphics_pipeline(self.shadow_pipeline.raw());
            ctx.cbuf.bind_vertex_buffers(
                1,
                iter::once((self.shadow_ibuf.inner().raw(), SubRange::WHOLE)),
            );

            let constants = &[shadow_matrix];
            let constants = std::slice::from_raw_parts(constants.as_ptr() as *const u32, 16);
            ctx.cbuf.push_graphics_constants(
                self.shadow_pipeline_layout.raw(),
                ShaderStageFlags::VERTEX,
                0,
                constants,
            );

            Self::draw_list(ctx, &casters, &self.shadow_pipeline_layout, false);

            ctx.cbuf.end_render_pass();

            let subrs_range = SubresourceRange {
                aspects: Aspects::COLOR,
                levels: 0..1,
                layers: 0..1,
            };

            let flush = ctx.storage.atlas.flush_needed();

            if self.first_frame || flush {
                let src = if self.first_frame {
                    (ImageAccess::empty(), Layout::Undefined)
                } else {
                    (ImageAccess::SHADER_READ, Layout::ShaderReadOnlyOptimal)
                };
                let barrier = Barrier::Image {
                    target: ctx.storage.atlas.image().raw(),
                    states: src..(ImageAccess::TRANSFER_WRITE, Layout::TransferDstOptimal),
                    range: subrs_range.clone(),
                    families: None,
                };
                ctx.cbuf.pipeline_barrier(
                    PipelineStage::FRAGMENT_SHADER..PipelineStage::TRANSFER,
                    Dependencies::empty(),
                    &[barrier],
                );

                ctx.storage
                    .atlas
                    .flush(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf)?;

                let barrier = Barrier::Image {
                    target: ctx.storage.atlas.image().raw(),
                    states: (ImageAccess::TRANSFER_WRITE, Layout::TransferDstOptimal)
                        ..(ImageAccess::SHADER_READ, Layout::ShaderReadOnlyOptimal),
                    range: subrs_range,
                    families: None,
                };
                ctx.cbuf.pipeline_barrier(
                    PipelineStage::TRANSFER..PipelineStage::FRAGMENT_SHADER,
                    Dependencies::empty(),
                    &[barrier],
                );
            }

            {
                let sampler = ctx
                    .sampler_cache
                    .get(ctx.gpu, &SamplerDesc::new(Filter::Linear, WrapMode::Clamp))?;
                device.write_descriptor_sets(iter::once(DescriptorSetWrite {
                    set: desc_set,
                    binding: 2,
                    array_offset: 0,
                    descriptors: &[Descriptor::CombinedImageSampler(
                        ctx.storage.atlas.image_view().raw(),
                        Layout::ShaderReadOnlyOptimal,
                        sampler.raw(),
                    )],
                }));
            }

            Self::add_instances(ctx, &objects, &mut self.forward_ibuf, true)?;

            let barrier = Barrier::Buffer {
                target: self.forward_ibuf.inner().raw(),
                states: BufferAccess::VERTEX_BUFFER_READ..BufferAccess::TRANSFER_WRITE,
                range: SubRange::WHOLE,
                families: None,
            };

            ctx.cbuf.pipeline_barrier(
                PipelineStage::VERTEX_INPUT..PipelineStage::TRANSFER,
                Dependencies::empty(),
                &[barrier],
            );

            self.forward_ibuf
                .flush(ctx.gpu, ctx.frame_cleaner, ctx.frame, ctx.cbuf)?;

            let barrier = Barrier::Buffer {
                target: self.forward_ibuf.inner().raw(),
                states: BufferAccess::TRANSFER_WRITE..BufferAccess::VERTEX_BUFFER_READ,
                range: SubRange::WHOLE,
                families: None,
            };

            ctx.cbuf.pipeline_barrier(
                PipelineStage::TRANSFER..PipelineStage::VERTEX_INPUT,
                Dependencies::empty(),
                &[barrier],
            );

            Self::prepare_list(ctx, &objects);

            ctx.cbuf.set_viewports(0, &[ctx.viewport()]);
            ctx.cbuf.set_scissors(0, &[ctx.rect()]);

            ctx.cbuf.begin_render_pass(
                &self.forward_rpass,
                framebuffer.raw(),
                ctx.rect(),
                &[
                    ClearValue {
                        color: ClearColor { float32: [1.0; 4] },
                    },
                    ClearValue {
                        depth_stencil: ClearDepthStencil {
                            depth: 1.0,
                            stencil: 0,
                        },
                    },
                ],
                SubpassContents::Inline,
            );

            ctx.cbuf.bind_graphics_descriptor_sets(
                self.forward_pipeline_layout.raw(),
                0,
                iter::once(desc_set),
                &[],
            );
            ctx.cbuf.bind_graphics_pipeline(self.forward_pipeline.raw());
            ctx.cbuf.bind_vertex_buffers(
                1,
                iter::once((self.forward_ibuf.inner().raw(), SubRange::WHOLE)),
            );

            Self::draw_list(ctx, &objects, &self.forward_pipeline_layout, true);

            ctx.cbuf.end_render_pass();
        }

        ctx.frame_cleaner.add_framebuffer(framebuffer);

        self.first_frame = false;

        Ok(())
    }

    /// Update settings
    pub fn update_settings(
        &mut self,
        old: &GraphicsSettings,
        new: &GraphicsSettings,
        gpu: &Gpu<B>,
        cleaner: &mut FrameCleaner<B>,
    ) -> Result<()> {
        if old.shadow_resolution != new.shadow_resolution {
            let (shadow_image, shadow_image_view, shadow_framebuffer) =
                create_shadow_views(gpu, new, &self.shadow_rpass)?;
            cleaner.add_image(std::mem::replace(&mut self.shadow_image, shadow_image));
            cleaner.add_image_view(std::mem::replace(
                &mut self.shadow_image_view,
                shadow_image_view,
            ));
            cleaner.add_framebuffer(std::mem::replace(
                &mut self.shadow_framebuffer,
                shadow_framebuffer,
            ));
            self.shadow_res = (new.shadow_resolution, new.shadow_resolution);
        }

        Ok(())
    }

    /// Destroy the pipeline
    ///
    /// # Safety
    /// The pipeline should not be in use
    pub unsafe fn destroy(self, ctx: &mut TeardownCtx<'_, B>) {
        let gpu = ctx.gpu;

        self.forward_pipeline.destroy(gpu);
        self.forward_pipeline_layout.destroy(gpu);
        gpu.device().destroy_render_pass(self.forward_rpass);

        self.shadow_pipeline.destroy(gpu);
        self.shadow_pipeline_layout.destroy(gpu);
        self.shadow_framebuffer.destroy(gpu);
        self.shadow_image_view.destroy(gpu);
        self.shadow_image.destroy(gpu);
        gpu.device().destroy_render_pass(self.shadow_rpass);

        self.frame_desc_set_layout.destroy(gpu);

        self.shadow_ibuf.destroy(gpu);
        self.forward_ibuf.destroy(gpu);
        self.uniform_buffers.destroy(|buf| buf.destroy(gpu));
    }
}
