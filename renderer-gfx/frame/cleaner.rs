use gfx_hal::Backend;

use crate::buffer::Buffer;
use crate::frame::Framebuffer;
use crate::gpu::Gpu;
use crate::image::{Image, ImageView};

/// Collects resources to destroy after the current frame has finished execution
#[derive(Debug)]
pub struct FrameCleaner<B: Backend> {
    buffers: Vec<Buffer<B>>,
    images: Vec<Image<B>>,
    image_views: Vec<ImageView<B>>,
    framebuffers: Vec<Framebuffer<B>>,
}

impl<B: Backend> Default for FrameCleaner<B> {
    fn default() -> FrameCleaner<B> {
        FrameCleaner::new()
    }
}

impl<B: Backend> FrameCleaner<B> {
    /// Create a new `FrameCleaner`
    pub fn new() -> FrameCleaner<B> {
        FrameCleaner {
            buffers: vec![],
            images: vec![],
            image_views: vec![],
            framebuffers: vec![],
        }
    }

    /// Add a buffer to destroy
    pub fn add_buffer(&mut self, buffer: Buffer<B>) {
        self.buffers.push(buffer);
    }

    /// Add an image to destroy
    pub fn add_image(&mut self, image: Image<B>) {
        self.images.push(image);
    }

    /// Add an image view to destroy
    pub fn add_image_view(&mut self, view: ImageView<B>) {
        self.image_views.push(view);
    }

    /// Add an framebuffer to destroy
    pub fn add_framebuffer(&mut self, fb: Framebuffer<B>) {
        self.framebuffers.push(fb);
    }

    /// Destroy the resources associated with the current frame
    ///
    /// # Safety
    /// The frame must have completed execution. No image views should be still alive if if
    /// destroying an image
    pub unsafe fn cleanup(&mut self, gpu: &Gpu<B>) {
        for buffer in self.buffers.drain(..) {
            buffer.destroy(gpu);
        }

        for image_view in self.image_views.drain(..) {
            image_view.destroy(gpu);
        }

        for image in self.images.drain(..) {
            image.destroy(gpu);
        }

        for framebuffer in self.framebuffers.drain(..) {
            framebuffer.destroy(gpu);
        }
    }
}
