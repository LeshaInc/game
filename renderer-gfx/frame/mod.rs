//! Frame related stuff

mod cleaner;
mod framebuffer;

pub use self::cleaner::FrameCleaner;
pub use self::framebuffer::Framebuffer;

use std::ops::{Index, IndexMut};

use anyhow::Result;

/// Current frame index mod frames-in-flight
#[derive(Clone, Copy, Default, Eq, Hash, PartialEq)]
pub struct FrameIndex(pub u8);

impl FrameIndex {
    /// Get the next frame
    pub fn next(self, frames_in_flight: usize) -> FrameIndex {
        FrameIndex((self.0 + 1) % (frames_in_flight as u8))
    }
}

/// Ring buffer for storing per frame data
#[derive(Debug)]
pub struct FrameRingbuffer<T> {
    per_frame: Vec<T>,
}

impl<T> FrameRingbuffer<T> {
    /// Create a new ring buffer
    pub fn new(
        frames_in_flight: usize,
        factory: impl FnMut(usize) -> Result<T>,
    ) -> Result<FrameRingbuffer<T>> {
        Ok(FrameRingbuffer {
            per_frame: (0..frames_in_flight).map(factory).collect::<Result<_>>()?,
        })
    }

    /// Create a new ring buffer. Infallible version
    pub fn new_infallible(
        frames_in_flight: usize,
        factory: impl FnMut(usize) -> T,
    ) -> FrameRingbuffer<T> {
        FrameRingbuffer {
            per_frame: (0..frames_in_flight).map(factory).collect(),
        }
    }

    /// Iterate through per frame data
    pub fn iter(&self) -> impl Iterator<Item = &T> + '_ {
        self.per_frame.iter()
    }

    /// Iterate through per frame data
    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> + '_ {
        self.per_frame.iter_mut()
    }

    /// Destroy all underlying resources using the specified destructor closure
    pub fn destroy(self, mut destructor: impl FnMut(T)) {
        for value in self.per_frame {
            destructor(value);
        }
    }
}

impl<T> Index<FrameIndex> for FrameRingbuffer<T> {
    type Output = T;

    fn index(&self, idx: FrameIndex) -> &T {
        &self.per_frame[idx.0 as usize]
    }
}

impl<T> IndexMut<FrameIndex> for FrameRingbuffer<T> {
    fn index_mut(&mut self, idx: FrameIndex) -> &mut T {
        &mut self.per_frame[idx.0 as usize]
    }
}
