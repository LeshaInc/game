use std::borrow::Borrow;

use anyhow::{Context, Result};
use gfx_hal::device::Device;
use gfx_hal::image::Extent;
use gfx_hal::Backend;

use crate::util::Relevant;
use crate::Gpu;

/// Framebuffer wrapper
#[derive(Debug)]
pub struct Framebuffer<B: Backend> {
    raw: B::Framebuffer,
    _relevant: Relevant,
}

impl<B: Backend> Framebuffer<B> {
    /// Create a new framebuffer
    pub fn new<I>(
        gpu: &Gpu<B>,
        pass: &B::RenderPass,
        attachments: I,
        extent: Extent,
    ) -> Result<Framebuffer<B>>
    where
        I: IntoIterator,
        I::Item: Borrow<B::ImageView>,
    {
        let raw = unsafe { gpu.device().create_framebuffer(pass, attachments, extent) }
            .context("Cannot create framebuffer")?;
        Ok(Framebuffer {
            raw,
            _relevant: Relevant::new::<Self>(),
        })
    }

    /// Destroy the framebuffer
    ///
    /// # Safety
    /// All commands referring to the framebuffer must have completed execution
    pub unsafe fn destroy(self, gpu: &Gpu<B>) {
        gpu.device().destroy_framebuffer(self.raw);
        self._relevant.destroy();
    }

    /// Get raw framebuffer object
    pub fn raw(&self) -> &B::Framebuffer {
        &self.raw
    }
}
